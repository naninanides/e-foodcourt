<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3"><i class="fa fa-money"></i> Transactions</h1>
</div>
<div class="wrapper-md">
	<div class="row m-b-md">
		<div class="col-xs-12 col-md-6 col-lg-4 text-center">
			<div class="r bg-info dker item hbox no-border">
				<div class="col w-xs v-middle hidden-md">
					<i class="fa fa-bar-chart-o fa-4x"></i>
				</div>
				<div class="col dk padder-v r-r">
					<div class="font-thin h1">
						<span>Rp. <?php echo format_number($acc_info->balance) ?></span>
					</div>
					<span class="text-muted text-md">Balance</span>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6 col-lg-4 text-center">
			<div class="r bg-light dker item hbox no-border">
				<div class="col w-xs v-middle hidden-md">
					<i class="fa fa-calendar fa-4x"></i> 
				</div>
				<div class="col dk padder-v r-r">
					<span class="text-muted text-md">Last Transaction</span>
					<div class="font-thin h2">
						<span><?php echo $acc_info->last_transaction_diff ?></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6 col-lg-4 text-center">
			<div class="r bg-light dker item hbox no-border">
				<div class="col w-xs v-middle hidden-md">
					<i class="fa fa-arrow-up fa-4x"></i> 
				</div>
				<div class="col dk padder-v r-r">
					<span class="text-muted text-md">Last Topup</span>
					<div class="font-thin h2">
						<span><?php echo $acc_info->last_topup_diff ?></span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="table-responsive">
					<table ui-jq="dataTable" ui-options="{
						sAjaxSource: '<?php echo site_url('api/transactions') ?>',
						columns: [
							{ data: 'acc_number' },
							{ 
								data: 'trans_date',
								render: dt_date,
								className: 'text-center'
							},
							{ data: 'description' },
							{ 
								data: 'debit',
								render: dt_currency,
								className: 'text-right' 
							},
							{ 
								data: 'credit',
								render: dt_currency,
								className: 'text-right'
							}
						],
						pageLength: 50,
						order: [[1, 'desc']]
						}" class="table table-striped b-t b-b">
						<thead>
							<tr>
								<th  style="width:15%">Account Number</th>
								<th  style="width:15%">Date</th>
								<th  style="width:40%">Description</th>
								<th  style="width:15%">Debit</th>
								<th  style="width:15%">Credit</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>