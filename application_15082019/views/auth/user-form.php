<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3"><i class="fa fa-user"></i> <?php echo lang('user') ?></h1>
</div>
<div class="wrapper-md">
	<form class="form-horizontal box" method="post">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 class="panel-title"><?php echo lang('user') ?></h2>
			</div>
			<div class="panel-body">
				<fieldset>
					<legend><?php echo lang('account'); ?></legend>
					<?php echo $form->fields(); ?>
				</fieldset>
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a href="<?php echo site_url('auth/user') ?>" class="btn btn-default"><i class="fa fa-repeat"></i> Batal</a>
			</div>
		</div>
	</form>
</div>