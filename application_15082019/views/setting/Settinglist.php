<div class="table-responsive">
	<table id="tbl2" class="table table-striped b-t b-light" ui-jq="dataTable" ui-options="{
		   ajax: '<?php echo site_url('api/Settingmenu/index') ?>',
		   columns: [			
		   		{ data: 'foto_menu' },			   
			   	{ data: 'id_menu' },
			   	{ data: 'nama_menu' },					   
			   	{ data: 'desk_menu' },			   
			   	{ data: 'status_menu' },					   
			   	{ data: 'harga_menu' },
				{ data: 'action_edit', render: render_edit },
				{ data: 'action_delete', render: render_action, orderable: false }
		   ],
		   order: [[0, 'desc']]
		   }" class="table table-striped b-t b-b">
			<thead>
				<tr>
					<th class="col-md-3">Foto</th>
					<th class="col-md-1">Id</th>														
					<th class="col-md-2">Nama</th>							
					<th class="col-md-3">Deskripsi</th>							
					<th class="col-md-1">Status</th>							
					<th class="col-md-1">Harga</th>
					<th style="width: 3px;"></th>
					<th style="width: 3px;"></th>									
				</tr>
			</thead>
			<tbody>
			</tbody>
	</table>
</div>
<?php $this->load->view('delete-modal'); ?>
<script>

	function render_edit(data, type, row, meta) {
	return '<a href="<?php echo site_url('tenant/setting/edit') ?>/' + row.id_menu + '" title="Edit" "><i class="fa fa-pencil"></i></a>';
	}

	function render_action(data, type, row, meta) {
		return '<a href="<?php echo site_url('tenant/setting/hapus') ?>/' + row.id_menu + '" title="Delete" data-button="delete"><i class="fa fa-trash"></i></a>';
	}	
</script>