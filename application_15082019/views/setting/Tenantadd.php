<div id="tenantAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-left">Tambah Tenant</h4>
      </div>
      <form class="form-horizontal" role="form" action="<?php echo site_url('setting/Settingtenant/tambah') ?>" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="form-group">
            <label class="col-sm-3 control-label">Pemilik</label>
            <div class="col-sm-9">
              <select name="data_pemilik" id="data_pemilik" class="form-control" style="width: 100%">
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-9">
              <input id="nama_tenant" class="form-control" type="text" placeholder="" name="nama_tenant">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Kategori</label>
            <div class="col-sm-9">
              <input id="kat_tenant" class="form-control" type="text" placeholder="" name="kat_tenant">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Deskripsi</label>
            <div class="col-sm-9">
              <textarea id="desk_tenant" class="form-control" rows="2" name="desk_tenant"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">No Telepon 1</label>
            <div class="col-sm-9">
              <input id="notel1_tenant" class="form-control" type="text" placeholder="" name="notel1_tenant">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">No Telepon 2</label>
            <div class="col-sm-9">
              <input id="notel2_tenant" class="form-control" type="text" placeholder="" name="notel2_tenant">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
              <input id="email_tenant" class="form-control" type="email" placeholder="" name="email_tenant">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Foto</label>
            <div class="col-sm-9">
              <input id="foto_tenant" class="form-control" type="file" name="foto_tenant">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Status</label>
            <div class="col-sm-9">
              <label class="checkbox-inline i-checks">
                <input type="radio" name="status_tenant" value="1" checked>
                <i></i>
                Aktif
              </label>
              <label class="checkbox-inline i-checks">
                <input type="radio" name="status_tenant" value="0">
                <i></i>
                Tidak Aktif
              </label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Simpan</button>
          <a href="#" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-repeat"></i> Batal </a>
        </div>
      </form>
    </div>
  </div>
</div>