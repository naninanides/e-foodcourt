<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">
    <a href data-toggle="modal" class="btn btn-primary open-Modal pull-right"><i class="fa fa-plus"></i> Tambah</a>
    <i class="fa fa-building"></i> Tenant
  </h1>
</div>
<div class="col">
  <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="table-responsive">
            <table id="dataTable_tenants" ui-jq="dataTable" ui-options="{
              processing: true,
              serverSide: true,
              ajax: {
                url: '<?php echo site_url('api/settingtenant/') ?>',
                type: 'POST'
              },
              columns: [
              { data: 'nama_tenant', render: render_nama_tenant },
              { data: 'pemilik_tenant'},
              { data: 'kat_tenant' },
              { data: 'notel1_tenant' },
              { data: 'email_tenant'},
              { data: 'status_tenant'},
              { data: 'action_delete', render: render_action, orderable: false }
              ],
              order: [[0, 'asc']],
              pageLength: 25
			}" class="table table-striped b-t b-b">
              <thead>
                <tr>
                  <th>Nama Tenant</th>
                  <th>Pemilik</th>
                  <th>Kategori Tenant</th>
                  <th>No HP Tenant</th>
                  <th>Email</th>
                  <th>Status</th>
                  <th style="width: 15px;"></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <?php include('Tenantadd.php'); ?>

          <div id="tenantEdit" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title text-left">Edit Tenant</h4>
                </div>
                <form class="form-horizontal" role="form" action="<?php echo site_url('setting/Settingtenant/ubah_tenant') ?>" method="POST" enctype="multipart/form-data">
                  <div class="modal-body">
                    Loading...
                  </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Simpan</button>
                    <a href="#" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-repeat"></i> Batal </a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('delete-modal'); ?>

<script>
 
  function render_nama_tenant(data, type, row, meta) {
    return '<a class="edit-tenant" href="#" data-id="' + row.id_tenant + '" title="Edit">' + data + '</a>';
  }

  function render_action(data, type, row, meta) {
    return '<a href="<?php echo site_url('setting/Settingtenant/hapus') ?>/' + row.id_tenant + '" title="Delete" data-button="delete"><i class="fa fa-trash"></i></a>';
  }
  $('.open-Modal').click(function(e) {
    select = document.getElementById('data_pemilik');
    e.preventDefault();
    $.ajax({
      type: 'ajax',
      method: 'get',
      url: '<?php echo site_url('setting/settingtenant/get_data_pemilik') ?>',
      dataType: 'json',
      success: function(data) {
        for (index = 0; index < data.length; ++index) {
          var opt = document.createElement('option');
          opt.value = data[index]['id'];
          opt.innerHTML = data[index]['first_name'] + " " + data[index]['last_name'];
          select.appendChild(opt);
        }
      }
    });
    $('#tenantAdd').modal('show');
  });
  $('#dataTable_tenants').on('click', '.edit-tenant', function(e) {
    e.preventDefault();
    var tenant_id = $(this).data('id');
    $('#tenantEdit .modal-body').html('Loading...');
    $('#tenantEdit').modal('show');
    $('#tenantEdit .modal-body').load('<?php echo site_url('setting/Settingtenant/edit/') ?>' + tenant_id, function() {
      $('select:not(.manual-select2)').select2({
        minimumResultsForSearch: 20
      });
    });
  });
</script>