<!DOCTYPE html>
<html lang="<?php echo $this->session->userdata('lang') ?>">
	<head>
		<meta charset="utf-8" />
		<title><?php echo $template['title']; ?></title>
		<meta name="description" content="app, web app, responsive, responsive layout, admin, admin panel, admin dashboard, flat, flat ui, ui kit, AngularJS, ui route, charts, widgets, components" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<?php echo $template['metas']; ?>
		
		<link rel="stylesheet" href="<?php echo assets_url('libs/assets/animate.css/animate.css') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo assets_url('libs/assets/font-awesome/css/font-awesome.min.css') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo assets_url('libs/assets/simple-line-icons/css/simple-line-icons.css') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo assets_url('libs/jquery/bootstrap/dist/css/bootstrap.css') ?>" type="text/css" />

		<link rel="stylesheet" href="<?php echo css_url('font') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo css_url('app') ?>" type="text/css" />
		
		<?php echo $template['css']; ?>

		<link rel="stylesheet" href="<?php echo css_url('custom') ?>" type="text/css" />
		
		<script src="<?php echo assets_url('libs/jquery/jquery/dist/jquery.js') ?>"></script>
		<?php echo $template['js_header']; ?>
		<script>
			var base_url = "<?php echo base_url() ?>";
		</script>
	</head>
	<body class="clean-background-grey">
		
		<?php echo $template['content']; ?>

		<script src="<?php echo base_url('assets/libs/jquery/bootstrap/dist/js/bootstrap.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-load.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-jp.config.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-jp.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-toggle.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-client.js') ?>"></script>

	</body>
</html>
