<!DOCTYPE html>
<html lang="<?php echo $lang ?>" class="">
	<head>
		<meta charset="utf-8" />
		<title>Food Court</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<?php echo $template['metas']; ?>

		<link rel="stylesheet" href="<?php echo assets_url('libs/assets/animate.css/animate.css') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo assets_url('libs/assets/font-awesome/css/font-awesome.min.css') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo assets_url('libs/assets/simple-line-icons/css/simple-line-icons.css') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo assets_url('libs/jquery/bootstrap/dist/css/bootstrap.css') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo assets_url('libs/jquery/select2/css/select2.css') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo assets_url('libs/assets/amstock3/amcharts/style.css') ?>" type="text/css" />

		<link rel="stylesheet" href="<?php echo css_url('font') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo css_url('app') ?>" type="text/css" />
		<link rel="stylesheet" href="<?php echo css_url('bootstrap-datepicker') ?>" type="text/css" />

		<link rel="stylesheet" href="<?php echo assets_url('easy/easy-autocomplete.min.css') ?>" type="text/css"/>

		<?php echo $template['css']; ?>
		<link rel="stylesheet" href="<?php echo css_url('custom') ?>" type="text/css" />
		<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?language=id&key=AIzaSyBBc3Q8eLfP2T3Fe5lPcM7Q0tOYEZoFzc0&libraries=places&v=3"></script>
		 --><!-- javascript amcharts charts library -->
		<script src="<?php echo assets_url('libs/assets/amstock3/amcharts/amcharts.js') ?>"></script>
		<script src="<?php echo assets_url('libs/assets/amstock3/amcharts/serial.js') ?>"></script>
		<script src="<?php echo assets_url('libs/assets/amstock3/amcharts/amstock.js') ?>"></script>
		<script src="<?php echo assets_url('libs/assets/amstock3/amcharts/plugins/dataloader/dataloader.min.js') ?>"></script>
		
		<script src="https://www.amcharts.com/lib/3/pie.js"></script>
		<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
		<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
		<script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 

		<script src="<?php echo assets_url('libs/jquery/jquery/dist/jquery.js') ?>"></script>
		<script src="<?php echo assets_url('easy/jquery.easy-autocomplete.min.js') ?>"></script>
		<?php echo $template['js_header']; ?>
		<script>
			var base_url = "<?php echo base_url() ?>";
		</script>
	</head>
	<body>
		<div class="app app-header-fixed ">


			<!-- header -->
			<header id="header" class="app-header navbar" role="menu">
				<!-- navbar header -->
				<div class="navbar-header bg-white">
					<button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
						<i class="glyphicon glyphicon-cog"></i>
					</button>
					<button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
						<i class="glyphicon glyphicon-align-justify"></i>
					</button>
					<!-- brand -->
					<a href="#/" class="navbar-brand text-lt">
						<span class="hidden-folded m-l-xs">Food Court</span>
					</a>
					<!-- / brand -->
				</div>
				<!-- / navbar header -->

				<!-- navbar collapse -->
				<div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
					<!-- buttons -->
					<div class="nav navbar-nav hidden-xs">
						<a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="app-aside-folded" target=".app">
							<i class="fa fa-dedent fa-fw text"></i>
							<i class="fa fa-indent fa-fw text-active"></i>
						</a>
						<!--<a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="show" target="#aside-user">
							<i class="icon-user fa-fw"></i>
						</a>-->
					</div>
					<!-- / buttons -->

					<!-- link and dropdown -->
					<!-- / link and dropdown -->

					<!-- search form -->
					<!-- / search form -->

					<!-- nabar right -->

              <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">
             <!-- <i class="icon-bell fa-fw"></i>
              <span class="visible-xs-inline">Notifications</span>
              <span class="badge badge-sm up bg-danger pull-right-xs">2</span>
            </a>
            <!-- dropdown --> 
            <div class="dropdown-menu w-xl animated fadeInUp">
              <div class="panel bg-white">
                <div class="panel-heading b-light bg-light">
                 <strong>You have <span>2</span> notifications</strong>
                </div>
                <div class="list-group">
                  <a href class="list-group-item">

                    <span class="clear block m-b-none">
                      Request New Event<br>
                      <small class="text-muted">10 minutes ago</small>
                    </span>
                  </a>
                  <a href class="list-group-item">
                    <span class="clear block m-b-none">
                      Need Approval<br>
                      <small class="text-muted">1 hour ago</small>
                    </span>
                  </a>
                </div>

						<li class="dropdown">
							<a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
								<span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
									<img src="<?php echo base_url('assets/img/a0.jpg') ?>" alt="...">
									<i class="on md b-white bottom"></i>
								</span>
								<span class="hidden-sm hidden-md"><?php echo $auth_user['username'] ?></span> <b class="caret"></b>
							</a>
							<!-- dropdown -->
							<ul class="dropdown-menu animated fadeInRight w">
							<!--	<li>
									<a href>
										<span class="badge bg-danger pull-right">30%</span>
										<span>Settings</span>
									</a>
								</li>
								<li>
									<a ui-sref="app.page.profile">Profile</a>
								</li>
								<li>
									<a ui-sref="app.docs">
										<span class="label bg-info pull-right">new</span>
										Help
									</a>
								</li> -->
								<!--<li class="divider"></li>-->
								<li>
									<a href="<?php echo site_url('auth/logout') ?>" ui-sref="access.signin">Logout</a>
								</li>
							</ul>
							<!-- / dropdown -->
						</li>
					</ul>
					<!-- / navbar right -->
				</div>
				<!-- / navbar collapse -->
			</header>
			<!-- / header -->


			<!-- aside -->
			<aside id="aside" class="app-aside hidden-xs bg-black">
				<div class="aside-wrap">
					<div class="navi-wrap">
						<!-- user -->
						<div class="clearfix hidden-xs text-center hide" id="aside-user">
							<div class="dropdown wrapper">
								<a href="app.page.profile">
									<span class="thumb-lg w-auto-folded avatar m-t-sm">
										<img src="<?php echo image_url('a0.jpg') ?>" class="img-full" alt="...">
									</span>
								</a>
						<!--		<a href="#" data-toggle="dropdown" class="dropdown-toggle hidden-folded">
									<span class="clear">
										<span class="block m-t-sm">
											<strong class="font-bold text-lt">John.Smith</strong>
											<b class="caret"></b>
										</span>
										<span class="text-muted text-xs block">Art Director</span>
									</span>
								</a>
								<!-- dropdown -->
								<ul class="dropdown-menu animated fadeInRight w hidden-folded">
									<li>
										<a href>Settings</a>
									</li>
									<li>
										<a href="page_profile.html">Profile</a>
									</li>
									<li>
										<a href>
											<span class="badge bg-danger pull-right">3</span>
											Notifications
										</a>
									</li>
									<li class="divider"></li>
									<li>
										<a href="page_signin.html">Logout</a>
									</li>
								</ul>
								<!-- / dropdown -->
							</div>
							<div class="line dk hidden-folded"></div>
						</div>
						<!-- / user -->

						<!-- nav -->
						<nav ui-nav class="navi clearfix">
							<ul class="nav">
								<li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
									<span>Navigasi</span>
								</li>
								<?php
								$this->load->config('navigation');
								$navigation = $this->config->item('navigation');

								function isMenuActive($url)
								{
									return (substr(uri_string(), 0, strlen($url)) == $url);
								}

								function traverseMenu(&$menus, $acl)
								{
									$active = FALSE;
									foreach ($menus as $key => $menu)
									{
										if (isset($menu['uri']) && !$acl->is_allowed($menu['uri']))
										{
											unset($menus[$key]);
											continue;
										}
										$children_active = FALSE;
										if (isset($menu['children']))
										{
											$children_active = traverseMenu($menu['children'], $acl);
											if (empty($menu['children']))
											{
												unset($menus[$key]);
												continue;
											}
											else
												$menus[$key]['children'] = $menu['children'];
										}
										if ($children_active || (isset($menu['uri']) && isMenuActive($menu['uri'])))
										{
											$menus[$key]['active'] = TRUE;
											$active = TRUE;
										}
									}
									return $active;
								}
								traverseMenu($navigation, $this->acl);

								foreach ($navigation as $nav_lvl_1):
									?>
									<li>
										<?php $has_children = isset($nav_lvl_1['children']) && is_array($nav_lvl_1['children']); ?>
										<a href="<?php echo (isset($nav_lvl_1['uri']) ? site_url($nav_lvl_1['uri']) : '#') ?>">
											<?php if ($has_children): ?>
												<span class="pull-right text-muted">
													<i class="fa fa-fw fa-angle-right text"></i>
													<i class="fa fa-fw fa-angle-down text-active"></i>
												</span>
											<?php endif; ?>
											<i class="<?php echo $nav_lvl_1['icon'] ?>"></i>
											<span class="font-bold"><?php echo $nav_lvl_1['title'] ?></span>
										</a>

										<?php if ($has_children): ?>
											<ul class="nav nav-sub dk">
												<?php foreach ($nav_lvl_1['children'] as $nav_lvl_2): ?>
													<li>
														<?php $has_children_2 = isset($nav_lvl_2['children']) && is_array($nav_lvl_2['children']); ?>
														<a href="<?php echo (isset($nav_lvl_2['uri']) ? site_url($nav_lvl_2['uri']) : '#') ?>">
															<?php if ($has_children_2): ?>
																<span class="pull-right text-muted">
																	<i class="fa fa-fw fa-angle-right text"></i>
																	<i class="fa fa-fw fa-angle-down text-active"></i>
																</span>
															<?php endif; ?>
															<?php echo $nav_lvl_2['title'] ?>
														</a>

														<?php if ($has_children_2): ?>
															<ul class="nav nav-sub dk">
																<?php foreach ($nav_lvl_2['children'] as $nav_lvl_3): ?>
																	<li>
																		<a href="<?php echo (isset($nav_lvl_3['uri']) ? site_url($nav_lvl_3['uri']) : '#') ?>">
																			<?php echo $nav_lvl_3['title'] ?>
																		</a>
																	</li>
																<?php endforeach; ?>
															</ul>
															<!-- /.nav-third-level -->
														<?php endif; ?>
													</li>
												<?php endforeach; ?>
											</ul>
											<!-- /.nav-second-level -->
										<?php endif; ?>
									</li>
								<?php endforeach; ?>
							</ul>
						</nav>
						<!-- nav -->

						<!-- aside footer -->
						<!-- / aside footer -->
					</div>
				</div>
			</aside>
			<!-- / aside -->


			<!-- content -->
			<div id="content" class="app-content" role="main">
				<div class="app-content-body ">
					<?php echo $template['content'] ?>
				</div>
			</div>
			<!-- /content -->

			<!-- footer -->
			<div class="app-footer wrapper b-t bg-light">
				<span class="pull-right">1.0.0 <?php echo ENVIRONMENT ?> <a href ui-scroll-to="app" class="m-l-sm text-muted"><i class="fa fa-long-arrow-up"></i></a></span>
				Copyright &copy; 2017. All Rights Reserved.
			</div>
			<!-- / footer -->
		</div>

		<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="deleteModalLabel">Delete Confirmation</h4>
					</div>
					<div class="modal-body">
						Apa anda yakin akan menghapus data ?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
						<button type="button" class="btn btn-primary" id="delete-confirmation"><i class="fa fa-save"></i> Ya</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="photoModal" tabindex="-1" role="dialog" aria-labelledby="photoModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="photoModalLabel"></h4>
					</div>
					<div class="modal-body" id="photoModalBody" style="text-align:center">

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="otherModal" tabindex="-1" role="dialog" aria-labelledby="otherModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="otherModalLabel">Confirmation</h4>
					</div>
					<div class="modal-body" id="otherModalMsg">
						Apa anda yakin ?
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" id="other-cancel" data-dismiss="modal">Tidak</button>
						<button type="button" class="btn btn-primary" id="other-confirmation"><i class="fa fa-save"></i> Ya</button>
					</div>
				</div>
			</div>
		</div>

		<script src="<?php echo base_url('assets/js/jshashtable-3.0.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/jquery.numberformatter-1.2.4.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/libs/jquery/bootstrap/dist/js/bootstrap.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-load.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-jp.config.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-jp.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-nav.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-toggle.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/ui-client.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/bootstrap-datepicker.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/vue.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/libs/jquery/select2/js/select2.full.js') ?>"></script>
		<script src="<?php echo base_url('assets/js/locales/bootstrap-datepicker.id.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/libs/jquery/numeral/min/numeral.min.js') ?>"></script>
		<script src="<?php echo base_url('assets/libs/jquery/moment/moment.js') ?>"></script>
		<script>
			$(document).ready(function () {
				// load a language for numeral
				numeral.language('id', {
					delimiters: {
						thousands: '.',
						decimal: ','
					},
					abbreviations: {
						thousand: 'ribu',
						million: 'juta',
						billion: 'milyar',
						trillion: 'trilyun'
					},
					currency: {
						symbol: 'Rp.'
					}
				});
				numeral.language('<?php echo $lang ?>');

				$('select:not(.manual-select2)').select2({
					minimumResultsForSearch: 20
				});

				moment.locale('<?php echo $lang ?>');
			});
		</script>
		<script src="<?php echo base_url('assets/js/main.js') ?>"></script>

		<?php echo $template['js_footer']; ?>
	</body>
</html>
