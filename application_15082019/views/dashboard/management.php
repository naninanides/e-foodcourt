<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3"><i class="fa fa-dashboard"></i> Dashboard</h1>
</div>
<div class="col">
    <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
        <div class="row">
            <div class="col-md-12">
              <div class="row row-sm">
                  <!--script for chart-->
                  <script>
                      var chart = AmCharts.makeChart( "chart_main", {
                      "type": "serial",
                      "dataProvider": [{
                        "tenant": "bakso semar",
                        "transaksi": 500
                      }, {
                        "tenant": "ayam criscy",
                        "transaksi": 475
                      }, {
                        "tenant": "oseng mercon",
                        "transaksi": 450
                      }, {
                        "tenant": "D'juice",
                        "transaksi": 425
                      }, {
                        "tenant": "Bubur ayam naik haji",
                        "transaksi": 400
                      }],
                      "valueAxes": [ {
                        "gridColor": "#FFFFFF",
                        "gridAlpha": 0.2,
                        "dashLength": 0
                      },
                     ],
                      "gridAboveGraphs": true,
                      "startDuration": 1,
                      "graphs": [ {
                        "balloonText": "[[tenant]]: <b>[[transaksi]]</b>",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "transaksi"
                      } ],
                      "chartCursor": {
                        "categoryBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "zoomable": false
                      },

                      "categoryField": "menu",
                      "categoryAxis": {
                        "labelsEnabled": false,
                      }
                      } );
                  </script>
                <div class="col-xs-12">
                  <div class="panel padder-v item">
                    <div class="panel header" style="padding-left:3%">
                      <h3>Grafik Transaksi Penjualan PerTenant</h3>
                    </div>
                      <div id="chart_main" class="chart-main"></div>
                  </div>
                </div>
                  <div class="col-xs-6">
                  <div class="col-xs-6 text-center">
                    <div class="block panel padder-v bg-danger item">
                      <div class="h4">Transaksi</div>
                      <div class="h1 text-white font-bold">655</div>
                    </div>
                  </div>
                  <div class="col-xs-6 text-center">
                    <div class="block panel padder-v bg-primary item">
                      <div class="h4">Total Penjualan</div>
                      <span class="text-white font-bold h1 block">Rp. 765.000 </span>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <div class="block panel padder-v item">
                      <div class="table-responsive" style="padding-left:2%;padding-right:2%">
                        <table  class="table table-striped b-t b-light">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Nama Menu</th>
                              <th>Transaksi</th>
                              <th>Nama Tenant</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th>1</th>
                              <th>D'juice Mangga</th>
                              <th>230</th>
                              <th>D'juice</th>
                            </tr>
                            <tr>
                              <th>2</th>
                              <th>Bakso Urat Semar</th>
                              <th>200</th>
                              <th>Bakso Semar</th>
                            </tr>
                            <tr>
                              <th>3</th>
                              <th>Paket Big</th>
                              <th>172</th>
                              <th>Ayam Criscy</th>
                            </tr>
                            <tr>
                              <th>4</th>
                              <th>Ayam bakar nuklir</th>
                              <th>146</th>
                              <th>Oseng mercon</th>
                            </tr>
                            <tr>
                              <th>5</th>
                              <th>Bubur ayam spesial</th>
                              <th>112</th>
                              <th>Bubur Ayam Naik Haji</th>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="col-md-12">
                    <div class="block panel padder-v item">
                      <div class="h4 text-center">Grafik Transaksi</div>
                        <!-- javascript for menu chart-->
                        <script>

                      			var chartData1 = [];
                      			var chartData2 = [];
                      			var chartData3 = [];
                      			var chartData4 = [];

                      			generateChartData();

                      			function generateChartData() {
                      				var firstDate = new Date();
                      				firstDate.setDate(firstDate.getDate() - 500);
                      				firstDate.setHours(0, 0, 0, 0);

                      				for (var i = 0; i < 500; i++) {
                      					var newDate = new Date(firstDate);
                      					newDate.setDate(newDate.getDate() + i);

                      					var a1 = Math.round(Math.random() * (40 + i)) + 100 + i;
                      					var b1 = Math.round(Math.random() * (1000 + i)) + 500 + i * 2;

                      					var a2 = Math.round(Math.random() * (100 + i)) + 200 + i;
                      					var b2 = Math.round(Math.random() * (1000 + i)) + 600 + i * 2;

                      					chartData1.push({
                      						date: newDate,
                      						value: a1,
                      						volume: b1
                      					});
                      					chartData2.push({
                      						date: newDate,
                      						value: a2,
                      						volume: b2
                      					});
                      				}
                      			}

                      			AmCharts.makeChart("chart_mini", {
                      				type: "stock",
                              categoryAxesSetting: {
                                maxSeries:1,
                                minPeriod:"DD"
                              },
                      				dataSets: [{
                      					title: "Transaksi",
                      					fieldMappings: [{
                      						fromField: "value",
                      						toField: "value"
                      					}, {
                      						fromField: "volume",
                      						toField: "volume"
                      					}],
                      					dataProvider: chartData1,
                      					categoryField: "date"
                      				},

                      				{
                      					title: "Transaksi sebelumnya",
                      					compared:true,
                      					fieldMappings: [{
                      						fromField: "value",
                      						toField: "value"
                      					}, {
                      						fromField: "volume",
                      						toField: "volume"
                      					}],
                      					dataProvider: chartData2,
                      					categoryField: "date"
                      				}],

                      				panels: [{
                      					title: "Value",
                      					percentHeight: 70,
                      					stockGraphs: [{
                      						id: "g1",
                                  type: "smoothedLine",
                      						valueField: "value",
                      						comparable: true,
                      						compareField: "value",
                      						bullet: "round",
                      						bulletBorderColor: "#FFFFFF",
                      						bulletBorderAlpha: 1,
                      						balloonText: "[[title]]:<b>[[value]]</b>",
                      						compareGraphBalloonText: "[[title]]:<b>[[value]]</b>",
                      						compareGraphBullet: "round",
                      						compareGraphBulletBorderColor: "#FFFFFF",
                      						compareGraphBulletBorderAlpha: 1
                      					}],

                      					stockLegend: {
                      						periodValueTextComparing: "[[percents.value.close]]%",
                      						periodValueTextRegular: "[[value.close]]"
                      					}
                      				}],

                      				chartScrollbarSettings: {
                      					graph: "g1",
                      					updateOnReleaseOnly:false,
                                enabled: false
                      				},

                      				chartCursorSettings: {
                      					valueBalloonsEnabled: true,
                      					valueLineEnabled:true,
                      					valueLineBalloonEnabled:true,
                                zoomable:true
                      				},
                              periodSelector: {
                      					position: "bot",
                                inputFieldsEnabled: false,
                      					periods: [{
                      						period: "DD",
                                  selected: true,
                      						count: 7,
                      						label: "1 minggu terakhir"
                      					}, {
                      						period: "MM",
                      						count: 1,
                      						label: "1 bulan terakhir"
                      					}, {
                      						period: "YYYY",
                      						count: 1,
                      						label: "1 tahun terakhir"
                      					}]
                      				}
                      			});
                      		</script>
                      <div class="row">
                        <div class="col-xs-12 stock-mini" id="chart_mini"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
