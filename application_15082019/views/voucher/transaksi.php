<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">
    <a href data-toggle="modal" class="btn btn-primary open-Modal pull-right"><i class="fa fa-plus"></i> Tambah</a>
    <i class="fa fa-building"></i> Transaksi 
  </h1>
</div>
<div class="col">
  <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="table-responsive">
            <table id="dataTable_Transaksis" ui-jq="dataTable" ui-options="{
              processing: true,
              serverSide: true,
              ajax: {
                url: '<?php echo site_url('api/VoucherTrans/').$this->uri->segment(3); ; ?>',
                type: 'GET'
              },
              columns: [
                
              { data: 'nama_depan'},
              { data: 'nama_tenant'},
              { data: 'nama_menu'},

              { data: 'waktu_transaksi' },
              { data: 'harga_menu'},         
              
             
            
              ],



              order: [[0, 'asc'],[3, 'asc'],[1, 'asc']],
              rowGroup: {
                  dataSrc: ['nama_depan','waktu_transaksi','nama_tenant']
              },
              columnDefs: [ {
                  targets: [0,3,1],
                  visible: false
              } ],
              pageLength: 25
			}" class="table table-striped b-t b-b">
              <thead>
                <tr>
                  <th>Nama</th>
                  <th>Tenant</th>
                  <th>Produk</th>
                  <th>Tanggal</th>
                  <th>Harga</th>
                
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
         
        </div>
      </div>
    </div>
  </div>
</div>

<script>
 
  function render_noAGP(data, type, row, meta) {
    return '<a class="edit-tenant" href="#" data-id="' + row.id_tenant + '" title="Edit">' + data + '</a>';
  }

  
</script>