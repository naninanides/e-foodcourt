<style>
body {
    font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 12px;
}
table {
    width: 100%;
    border: 1px solid #edf1f2;
    border-spacing: 0;
    border-collapse: collapse;
}
td, th {
    padding: 8px 15px;
    border-top: 1px solid #eaeff0;
    line-height: 1.42857143;
    vertical-align: top;
    box-sizing: content-box;
}
th {
    text-align: left;
}
.odd td {
    background-color: #f9f9f9;
}
</style>
<table cellpadding="0">
    <thead>
        <tr>
            <th style="text-align: right">No AGP</td>
            <th>Nama</td>
            <th>Nominal</td>
            <th>Tanggal</td>
            <th>Tipe Topup</td>
            
            <th>Operator</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($rows as $index => $row): ?>
        <tr class="<?php echo ($index % 2 == 0 ? '' : 'odd') ?>">
            <td style="text-align: right"><?php echo $row->noagp ?></td>
            <td><?php echo $row->nama_lengkap ?></td>
            <td style="text-align: right"><?php echo number_format($row->harga, 0, ',', '.') ?></td>
            
            <td><?php echo $row->tgl_tu ?></td>
            <td><?php echo $row->kdJnstu ?></td>
            <td><?php echo $row->operator ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>