<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3"><i class="fa fa-cutlery"></i> Order Menu</h1>
</div>
<div class="col">
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="row">
			<div class="col-lg-7">
				<div class="tab-container">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation"  class="nav-item active">
							<a class="nav-link active" href="#menulist" role="tab" data-toggle="tab">
								<i class="fa fa-list"></i>&nbsp;&nbsp;Menu List
							</a>
						</li>
						<li role="presentation" class="nav-item">
							<a class="nav-link" id="menuB" href="#MenuMakanan" role="tab" data-toggle="tab">
								<i class="fa fa-cutlery"></i>&nbsp;&nbsp;Makanan
							</a>
						</li>
						<li role="presentation" class="nav-item">
							<a class="nav-link" href="#MenuMinuman" role="tab" data-toggle="tab">
								<i class="fa fa-glass"></i>&nbsp;&nbsp;Minuman
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel"  id="menulist" class="tab-pane active">
							<div  class="table-responsive">					
								<div id="tabelMenu" class="text-center">
									<?php $this->load->view('tenant/Menulist'); ?>
								</div>
							</div>
						</div>
						<div role="tabpanel"  id="MenuMakanan" class="tab-pane">
							<div  class="table-responsive">					
								<div id="tabelMenu" class="text-center">
									<?php $this->load->view('tenant/Menulistmak'); ?>
								</div>
							</div>
						</div>

						<div role="tabpanel"  id="MenuMinuman" class="tab-pane"><div  class="table-responsive">					
								<div id="tabelMenu" class="text-center">
									<?php $this->load->view('tenant/Menulistmnm'); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5" ng-controller="FormDemoCtrl">
				<div class="panel panel-default">
					<div class="panel-heading">
						Order List
					</div>
					<div class="panel-heading ">
            <?php $session_data=$this->session->userdata('id'); ?>
            <input type="hidden" name="kodepesanan" id="kodepesanan" value="<?php echo $session_data.'--'.date('Y-m-d-h:i:s'); ?>">
						<span class="font-bold col-md-2">No Meja : </span><input type="text" name="nomeja" class="col-md-2" id="nomeja">
            <div class="col-md-2"></div>
            <span class="font-bold col-md-3">Nama Pemesan :</span><input type="text" name="namapemesan" class="col-md-2" id="namapemesan">
					</div>
					<div class="table-responsive col-md-12">
						<table id="tabelOrder" class="table table-striped b-t b-light">
							<thead>
							  <tr>
							  <th style="visibility: hidden;">ID</th>
								<th class="col-md-7">Nama Makanan</th>
								<th class="col-md-2 text-center">Qty</th>
								<th class="col-md-1 text-center"></th>
								<th class="col-md-2 text-center">Harga</th>
							  </tr>
							</thead>
							<tbody>
							  
							</tbody>
						</table>
						<div>
						<span id="totalPesanan" hidden="true">0</span>
							<span class="col-md-9 font-bold">Diskon</span>
							<span class="col-md-1 font-bold text-center">Rp.</span>
							<span id="diskontotal" class="col-md-2 text-right font-bold">0.000</span>
							<span class="col-md-9 font-bold">PPN 10%</span>
							<span id="taxTotal" class="col-md-3 text-right font-bold">0</span>
							<span class="col-md-9 font-bold ">Total</span>
							<span class="col-md-1 font-bold text-center">Rp.</span>
							<span id="totalHarga" class="col-md-2 text-right font-bold ">0</span>
							<div class="panel col-md-12 wrapper-md">
								<button type="button" id="btnOrder" class="btn btn-success col-md-12" data-toggle="modal" data-target="#om00"><i class="fa fa-money"></i> Bayar</button>
							</div>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<div id="modalDetail"  class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    <form class="form-horizontal" id="form_order" name="form_order" role="form" method="post" enctype="multipart/form-data">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-left"></h4>
      </div>
      <div class="modal-body">
        
          <div class="form-group">

            <input type="hidden" name="selected_id_menu" id="selected_id_menu">
            <label class="col-lg-12 control-label">
            <img src="" class="img-rounded center-block " id="image_menu_sel" widht="100px" height="100px">
            </label>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Nama Menu :</label>
            <div class="col-sm-6">
              <input class="form-control text-center" name="sel_nama_menu" type="text" readonly="true" >
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Deskripsi Menu :</label>
            <div class="col-sm-6">
              <textarea id="desk_menu_sel" class="form-control text-center text-muted" name="sel_desk_menu" readonly="true" ></textarea>  
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Diskon :</label>
            <div class="col-sm-6">
              <input class="form-control text-right" type="text" placeholder="0.00" readonly="true">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Harga :</label>
            <div class="col-sm-6">
              <input class="form-control text-right" name="sel_harga_menu" type="text" readonly="true" >
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Qty :</label>
            <div class="col-sm-2">
                <input id="totalQty" value="0" class="form-control text-right" name="totalQty" >
            </div>
            <div class="col-sm-1">
                <button id="tambah" type="button" class="btn btn-default btn-md text-center" ><i class="fa fa-plus"></i></button>
            </div>
            <div class="col-sm-1">
                <button type="button" id="kurang" class="btn btn-default btn-md text-center"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Status :</label>
            <div id="divStatus" class="col-sm-3">
          <input type="hidden" name="status_menu_sel" id="status_menu_sel">
     
             
         
            
           </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <div class="col-md-6 wrapper-md">
          <button type="button" class="btn btn-danger col-md-12" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
        </div>
        <div class="col-md-6 wrapper-md">
          <input id="btnSimpan" value="Simpan" type="submit" class="btn btn-success btn-md col-md-12"/>
        </div>
      </div>
    </div>
    </form>
  </div>
</div>
<script>

	$(document).on("submit",'#form_order',function(e) {
		var total =parseInt(document.getElementById('totalHarga').innerHTML.replace(/,/g, ''));
		e.preventDefault();
      $.ajax({
         type:"post",
         url:"<?php echo site_url('tenant/Order/data_form');?>",
         dataType: 'json',
         data: $(this).serialize(),
         success:function(data){
     
          var dt = data;
          alert("Order Berhasil");
           var table = document.getElementById ("tabelOrder");
            var row = table.insertRow (1);
            var cellID = row.insertCell (0);
            var cellNama = row.insertCell (1);
            var cellQty = row.insertCell (2);
            var cellRp = row.insertCell(3);
            var cellHarga = row.insertCell (4);
            cellID.innerHTML = dt[0];
            cellID.style.visibility ='hidden';
            cellNama.innerHTML = dt[1];
            cellNama.className = "col-md-7";
            cellRp.innerHTML="Rp.";
            cellRp.className="col-md-1 font-bold text-center";
            cellHarga.innerHTML=dt[2];
            cellHarga.className = "col-md-2 text-center";
            cellQty.innerHTML=dt[3];
            cellQty.className = "col-md-2 text-center";
            total=total+parseInt(dt[4]);
            var tax = total * 0.1;
            totalOrder=total+tax;
            total=numberWithCommas(total);
            document.getElementById('totalPesanan').innerHTML=total;
            tax = numberWithCommas(tax);
            document.getElementById('taxTotal').innerHTML=tax;
           totalOrder=numberWithCommas(totalOrder);
           document.getElementById('totalHarga').innerHTML=totalOrder;

         },
         error: function(XMLHttpRequest, textStatus, errorThrown) { 
        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
    }       
      });
  });

$(function() {
    $('#menuB').click(function() {
        var kategori = new Vue({
  el: '#kategori',
  data: {
    message: 'Hello Vue!'
  }
})
    });
});

	$(document).on("click",'#btnOrder',function(){
		var diskontotal=parseInt(document.getElementById('diskontotal').innerHTML.replace(/,/g, ''));
		var taxTotal=parseInt(document.getElementById('taxTotal').innerHTML.replace(/,/g, ''));
		var totalPesanan=parseInt(document.getElementById('totalPesanan').innerHTML.replace(/,/g, ''));
		var totalHarga = parseInt(document.getElementById('totalHarga').innerHTML.replace(/,/g, ''));
		var nomeja = document.getElementById('nomeja').value;
    var namapemesan = document.getElementById('namapemesan').value;
  var array = [];
    var headers = [];
    $('#tabelOrder th').each(function(index, item) {
        headers[index] = $(item).html();
    });
    $('#tabelOrder tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers[index]] = $(item).html();
        });
        array.push(arrayItem);
    });
if (nomeja == "" || namapemesan == "" || totalPesanan == null) {
  alert('pesan makanan terlebih dahulu');
  return;
}
    $.ajax({
    	type:"post",
         url:"<?php echo site_url('tenant/Order/data_order');?>",
         dataType: 'json',
         data: {'array':array, 'diskontotal':diskontotal, 'taxTotal':taxTotal, 'totalHarga':totalHarga, 'nomeja':nomeja,'totalPesanan':totalPesanan, 'namapemesan':namapemesan},
         success:function(){
         	
         }

    });
    alert('Order Berhasil');
    location.reload();
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


$('.open-Modal').click(function(e){

   e.preventDefault();
var wrapper = document.getElementById("divStatus");
$("#divStatus").empty();
   var selected_id = $(this).attr('data-id');
   $('#modalDetail #selected_id_menu').val(selected_id);
   var menu_harga = $('#menu_harga'+selected_id).val();
   var menu_foto = $('#menu_foto'+selected_id).val();
   $('#modalDetail').modal('show');

   $.ajax({
      type:'ajax',
      method:'get',
      url:'<?php echo site_url('tenant/Order/get_data_modal') ?>',
      data: {'selected_id':selected_id},
      dataType:'json',
      success:function(data){
         $('input[name=sel_nama_menu]').val(data[0].nama_menu);
         $('input[name=totalQty]').val(0);
         $('.modal-title').text(data[0].nama_menu);
         $('#desk_menu_sel').text(data[0].desk_menu)
         $('#image_menu_sel').attr("src", menu_foto);
         
         $('input[name=sel_harga_menu]').val(menu_harga);
         var statsel=data[0].status_menu;
         

             if (statsel==1){
               var btn = document.createElement("button");
               btn.setAttribute("type","button");
               btn.setAttribute("class","btn btnStatus btn-success btn-md text-center col-md-12");
             document.getElementById('btnSimpan').disabled=false;
               btn.setAttribute("id","btnStatus");
               btn.innerHTML = 'Tersedia';
               
               wrapper.appendChild(btn);
             }
             else if (statsel==0){
               var btn = document.createElement("button");
                btn.setAttribute("type","button");
               btn.setAttribute("class","btn btnStatus btn-danger btn-md text-center col-md-12");
              document.getElementById('btnSimpan').disabled=true;
               btn.setAttribute("id","btnStatus");
               btn.innerHTML = 'Habis';
               
               wrapper.appendChild(btn);
             }
      }
   });
   
});

  $(document).on("click",'.btnStatus',function() {
    var id_menu = $('input[name="selected_id_menu"]').val();
  
    $.ajax({
        url: "<?php echo site_url('tenant/Order/change_status/')?>" + id_menu,
        type: 'POST',
        data: {'id_menu': id_menu},
        dataType: 'JSON',
        success: function(data) {    
              setTimeout(function(){// wait for 5 secs(2)
           location.reload(); // then reload the page.(3)
      }, 1000); 
             
        }
    });
});

 $(document).on("click",'#tambah',function(){
      
    var i = $('input[name="totalQty"]').val();
    i=parseInt(i)+1;
    $('input[name="totalQty"]').val(i);
  });

$(document).on("click",'#kurang',function(){
    var i = $('input[name="totalQty"]').val();
    if (parseInt(i)-1 == 0){
      $('input[name="totalQty"]').val(0);
    }
    if (parseInt(i)-1 > 0){
      i=parseInt(i)-1;
      $('input[name="totalQty"]').val(i);
    }

  });


  </script>