<div id="menuAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-left">Tambah menu</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form" action="<?php echo site_url('setting/settingmenu/tambah') ?>" method="POST">
          <div class="form-group">
            <label class="col-lg-3 control-label">Id Menu :</label>
            <div class="col-sm-6">
              <input id="id_menu" class="form-control text-center" type="text" placeholder="" name="id_menu">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Poto Menu :</label>
            <div class="col-sm-6">
              <input id="poto_menu" class="form-control text-center" type="text" placeholder="" name="foto_menu">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Nama Menu :</label>
            <div class="col-sm-6">
              <input id="nama_menu" class="form-control text-center" type="text" placeholder="" name="nama_menu">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Deskipsi Menu :</label>
            <div class="col-sm-6">
              <input id="desk_menu" class="form-control text-center" type="text" placeholder="" name="desk_menu">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Status Menu :</label>
            <div class="col-sm-6">
              <input id="status_menu" class="form-control text-center" type="text" placeholder="" name="status_menu">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Harga Menu :</label>
            <div class="col-sm-6">
              <input id="harga_menu" class="form-control text-center " placeholder="" name="harga_menu" type="number" value="1000" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <div class="col-md-6 wrapper-md">
          <a href="#" type="button" class="btn btn-danger col-md-12 " data-dismiss="modal"><i class="fa fa-ban"></i> Batal </a>
        </div>
        <div class="col-md-6 wrapper-md">
          <button type="submit" class="btn btn-success btn-md col-md-12"><i class="fa fa-floppy-o"></i> Simpan</button>
          </div>
      </div>
      </form>
    </div>
  </div>
</div>