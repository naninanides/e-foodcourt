<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3"><i class="fa fa-cutlery"></i> Order</h1>
</div>
<div class="col">
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="row">
			<div class="col-lg-7">
				<div class="panel panel-default">
					<div class="panel-heading">
						Menu List	
					</div>
					<div class ="panel-body" >
						<div class="table-responsive">					
							<div class="text-center">
								<?php include('menuList.php'); ?>
   								<div class="col-xs-4" >
   									<a href class="block panel padder-v item">
   									<span class="text-muted font-bold h1 block"><img src="<?php echo base_url('images/nasiayambakar1.jpg'); ?>" class="img-rounded " widht="100px" height="100px"></span>
   									<span class="text-muted text-m font-bold block">Nasi Ayam Bakar</span>
   									<span class="text-muted text-m font-bold block">Diskon : Rp. 0</span>
   									<span class="text-muted text-m font-bold">Harga : Rp. 18.000</span>
   									</a>
   								</div>
   								<div class="col-xs-4" >
   									<a href class="block panel padder-v item">
   									<span class="text-muted font-bold h1 block"><img src="<?php echo base_url('images/nasiayampenyet.jpeg'); ?>" class="img-rounded " widht="100px" height="100px"></span>
   									<span class="text-muted text-m font-bold block">Nasi Ayam Penyet</span>
   									<span class="text-muted text-m font-bold block">Diskon : Rp. 0</span>
   									<span class="text-muted text-m font-bold">Harga : Rp. 20.000</span>
   									</a>
   								</div>
   								<div class="col-xs-4" >
   									<a href class="block panel padder-v item">
   									<span class="text-muted font-bold h1 block"><img src="<?php echo base_url('images/nasiayamgoreng.jpg'); ?>" class="img-rounded " widht="100px" height="100px"></span>
   									<span class="text-muted text-m font-bold block">Nasi Ayam Goreng</span>
   									<span class="text-muted text-m font-bold block">Diskon : Rp. 0</span>
   									<span class="text-muted text-m font-bold">Harga : Rp. 15.000</span>
   									</a>
   								</div>
   								<div class="col-xs-4" >
   									<a href class="block panel padder-v item">
   									<span class="text-muted font-bold h1 block"><img src="<?php echo base_url('images/nasiayambakar1.jpg'); ?>" class="img-rounded " widht="100px" height="100px"></span>
   									<span class="text-muted text-m font-bold block">Nasi Ayam Bakar</span>
   									<span class="text-muted text-m font-bold block">Diskon : Rp. 0</span>
   									<span class="text-muted text-m font-bold">Harga : Rp. 18.000</span>
   									</a>
   								</div>
   								<div class="col-xs-4" >
   									<a href class="block panel padder-v item">
   									<span class="text-muted font-bold h1 block"><img src="<?php echo base_url('images/nasiayampenyet.jpeg'); ?>" class="img-rounded " widht="100px" height="100px"></span>
   									<span class="text-muted text-m font-bold block">Nasi Ayam Penyet</span>
   									<span class="text-muted text-m font-bold block">Diskon : Rp. 0</span>
   									<span class="text-muted text-m font-bold">Harga : Rp. 20.000</span>
   									</a>
   								</div>
   								<div class="col-xs-4" >
   									<a href class="block panel padder-v item">
   									<span class="text-muted font-bold h1 block"><img src="<?php echo base_url('images/nasiayamgoreng.jpg'); ?>" class="img-rounded " widht="100px" height="100px"></span>
   									<span class="text-muted text-m font-bold block">Nasi Ayam Goreng</span>
   									<span class="text-muted text-m font-bold block">Diskon : Rp. 0</span>
   									<span class="text-muted text-m font-bold">Harga : Rp. 15.000</span>
   									</a>
   								</div>
   								<div class="col-xs-4" >
   									<a href class="block panel padder-v item">
   									<span class="text-muted font-bold h1 block"><img src="<?php echo base_url('images/nasiayambakar1.jpg'); ?>" class="img-rounded " widht="100px" height="100px"></span>
   									<span class="text-muted text-m font-bold block">Nasi Ayam Bakar</span>
   									<span class="text-muted text-m font-bold block">Diskon : Rp. 0</span>
   									<span class="text-muted text-m font-bold">Harga : Rp. 18.000</span>
   									</a>
   								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5" ng-controller="FormDemoCtrl">
				<div class="panel panel-default">
					<div class="panel-heading">
						Order List
					</div>
					<div class="panel-heading ">
						<span class="font-bold">No Meja : </span><input type="" name="">
					</div>
					<div class="table-responsive">
						<table class="table table-striped b-t b-light">
							<thead>
							  <tr>
								<th class="col-md-7">Nama Makanan</th>
								<th class="col-md-2 text-center">Qty</th>
								<th class="col-md-3">Harga</th>
							  </tr>
							</thead>
							<tbody>
							  <tr>
							  	<td>Nasi Ayam Bakar</td>
							  	<td class="text-center">2</td>
							  	<td class="text-right">Rp. 36.000</td>
							  </tr>
							   <tr>
							  	<td>Nasi Ayam Goreng</td>
							  	<td class="text-center">5</td>
							  	<td class="text-right">Rp. 80.000</td>
							  </tr>
							  <tr>
							  	<td>-</td>
							  	<td></td>
							  	<td></td>	
							  </tr>
							  <tr>
							  	<td>-</td>
							  	<td></td>
							  	<td></td>	
							  </tr>
							  <tr>
							  	<td>-</td>
							  	<td></td>
							  	<td></td>	
							  </tr>
							  <tr>
							  	<td>-</td>
							  	<td></td>
							  	<td></td>	
							  </tr>
							  <tr>
							  	<td>-</td>
							  	<td></td>
							  	<td></td>	
							  </tr><tr>
							</tbody>
						</table>
						<div>
							<span class="col-md-9 font-bold">Diskon</span>
							<span class="col-md-3 text-right font-bold">Rp 0.000</span>
							<span class="col-md-9 font-bold ">Total</span>
							<span class="col-md-3 text-right font-bold ">Rp 116.000</span>
							<div class="col-md-6 wrapper-md">
								<button type="button" class="btn btn-warning btn-md col-md-12"><i class="fa fa-floppy-o"></i> Simpan</button>
							</div>
							<div class="col-md-6 wrapper-md">
								<button type="button" class="btn btn-danger col-md-12"><i class="fa fa-ban"></i> Hapus</button>
							</div>
							<div class="panel col-md-12">
								<button type="button" class="btn btn-success col-md-12"><i class="fa fa-money"></i> Bayar</button>
							</div>		
							
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>	
</div>
