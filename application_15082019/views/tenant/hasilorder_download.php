<style>
body {
    font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 12px;
}
table {
    width: 100%;
    border: 1px solid #edf1f2;
    border-spacing: 0;
    border-collapse: collapse;
}
td, th {
    padding: 8px 15px;
    border-top: 1px solid #eaeff0;
    line-height: 1.42857143;
    vertical-align: top;
    box-sizing: content-box;
}
th {
    text-align: left;
}
.odd td {
    background-color: #f9f9f9;
}
</style>
<table cellpadding="0">
    <thead>
        <tr>
            <th style="text-align: right">ID Transaksi</td>
            <th>Waktu</td>
            <th>Status</td>
            <th style="text-align: right">Total Pembayaran</td>
            <th>Tenant</td>
        </tr>
    </thead>
    <tbody>
        <?php foreach($rows as $index => $row): ?>
        <tr class="<?php echo ($index % 2 == 0 ? '' : 'odd') ?>">
            <td style="text-align: right"><?php echo $row->id_transaksi ?></td>
            <td><?php echo $row->waktu_transaksi ?></td>
            <td><?php echo $row->status_bayar ?></td>
            <td style="text-align: right"><?php echo number_format($row->TotalPembayaran, 0, ',', '.') ?></td>
            <td><?php echo $row->nama_tenant ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>