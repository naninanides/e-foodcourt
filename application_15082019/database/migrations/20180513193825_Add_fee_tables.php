<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_fee_tables extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(array(
                'id' => array(
                    'type' => 'INT',
                    'auto_increment' => true
                ),
                'nama' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
                'deskripsi' => array(
                    'type' => 'TEXT',
                    'null' => true
                ),
                'created' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'null' => true
                ),
                'updated' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'null' => true
                ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('fees');

        $this->dbforge->add_field(array(
                'id' => array(
                    'type' => 'INT',
                    'auto_increment' => true
                ),
                'nama' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
                'deskripsi' => array(
                    'type' => 'TEXT',
                    'null' => true
                ),
                'created' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'null' => true
                ),
                'updated' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'null' => true
                ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('fee_accounts');

        $this->dbforge->add_field(array(
                'id' => array(
                    'type' => 'INT',
                    'auto_increment' => true
                ),
                'fee_id' => array(
                    'type' => 'INT'
                ),
                'jenis' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50'
                ),
                'kategori_id' => array(
                    'type' => 'INT',
                    'null' => true
                ),
                'jumlah' => array(
                    'type' => 'FLOAT',
                    'default' => 0
                ),
                'satuan' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '10'
                ),
                'fee_account_id' => array(
                    'type' => 'INT',
                ),
                'created' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'null' => true
                ),
                'updated' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'null' => true
                ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->add_key('fee_id');
        $this->dbforge->add_key('fee_account_id');
        $this->dbforge->create_table('fee_details');
    }

    public function down()
    {
        $this->dbforge->drop_table('fee_details');
        $this->dbforge->drop_table('fee_accounts');
        $this->dbforge->drop_table('fees');
    }
}
