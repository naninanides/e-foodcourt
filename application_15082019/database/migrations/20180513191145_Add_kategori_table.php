<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_kategori_table extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field(array(
                'id' => array(
                    'type' => 'INT',
                    'auto_increment' => true
                ),
                'nama' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255'
                ),
                'deskripsi' => array(
                    'type' => 'TEXT',
                    'null' => true
                ),
                'created' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'created_by' => array(
                    'type' => 'INT',
                    'null' => true
                ),
                'updated' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'updated_by' => array(
                    'type' => 'INT',
                    'null' => true
                ),
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('kategori');
    }

    public function down()
    {
        $this->dbforge->drop_table('kategori');
    }
}
