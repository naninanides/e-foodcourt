<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_tenant_fields extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_column('tenant', array(
			'status_tenant' => array(
				'type' => 'INT',
				'default' => 1,
				'after' => 'desk_tenant'
			),
        ));
		$this->dbforge->add_column('auth_users', array(
			'id_tenant' => array(
				'type' => 'INT',
				'null' => TRUE,
				'after' => 'role_id'
			)
		));
    }

    public function down()
    {
		$this->dbforge->drop_column('tenant', 'status_tenant');
		$this->dbforge->drop_column('auth_users', 'id_tenant');
    }
}
