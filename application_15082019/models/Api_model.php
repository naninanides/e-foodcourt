<?php


class Api_model extends CI_Model
{
    public function get_tenant($id_tenant)
    {
        $this->db->select('id_tenant,nama_tenant,pemilik_tenant,kat_tenant,
        desk_tenant,foto_tenant');
        $this->db->from('tenant');
        if($id_tenant != '')
        {
            $this->db->where('id_tenant', $id_tenant);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_menus($id_tenant)
    {
        $this->db->select('id_menu,nama_menu,desk_menu,status_menu,
        foto_menu,harga_menu,kategori.nama as type');
        $this->db->from('menu');
        $this->db->join('kategori','menu.kategori=kategori.id');
        if($id_tenant != '')
        {
            $this->db->where('id_tenant', $id_tenant);
        }
        $query = $this->db->get();
        $result = $query->result_array();
        foreach($result as $i => $row)
        {
            $result[$i]['foto_menu'] = base_url($row['foto_menu']);
        }
        return $result;
    }

    public function post_transaction($data)
    {
        $result = $this->db->insert('transaksi',$data);
        $insertId = $this->db->insert_id();
        return $insertId;
    }

    public function post_detail_transaction($data)
    {
        $result = $this->db->insert('detail_transaksi',$data);
        return $result;
        $result2=$this->db->select('kode_order AS kode_order, "OK" as statusa')
        ->from('detail_transaksi')			 
        ->where('kode_order',$result)
        ->get();
        return $result2->result_array();
        

        //return "xxxxxxxxxxx";
    }

    public function post_payment($data,$id_transaksi)
    {
        $before = $this->db->select('biaya_pesanan,discount,total_pembayaran')
                  ->from('transaksi')->where('u_id_transaksi',$id_transaksi)
                  ->get()->row()->biaya_pesanan;
        $result = false;
        if($id_transaksi != NULL && $before == 0)
            $result = $this->db->where('u_id_transaksi',$id_transaksi);
            $result = $this->db->update('transaksi',$data);
        return $result;
    }

    public function get_detail_transaction($id_transaksi)
    {
        $result = $this->db->select('jumlah_pesanan,menu.harga_menu')
                  ->from('detail_transaksi')
                  ->join('menu','menu.id_menu = detail_transaksi.id_menu')
                  ->where('u_id_transaksi',$id_transaksi)
                  ->get();
        return $result->result_array();
    }

    public function get_pesanan($id_tenant)
    {
        $result = $this->db->select('detail_transaksi.id AS ID, CONCAT(menu.nama_menu, \' (\',jumlah_pesanan,\')\') AS Name
		, CONCAT(\'No. Meja : \', detail_transaksi.no_kursi) AS NoMeja, DATE_FORMAT( detail_transaksi.create_at, \'%d/%m/%Y %H:%i\' ) AS TglPesan, status_antar AS StatusAntar, status_bayar AS StatusBayar, jumlah_pesanan AS Quantity, menu.desk_menu AS Description, total_harga_menu AS Price,cat_pesan AS CatPesan')
                  ->from('detail_transaksi')
                  ->join('menu','menu.id_menu = detail_transaksi.id_menu')
                  ->where('kode_tenant',$id_tenant)
                  ->get();
        return $result->result_array();
    }

    public function get_pesananstatus($id_tenant,$tgl,$StatusBayar,$StatusAntar,$KodeOrder,$NoPesanan)
    {

        $date = new DateTime("now");
        $curr_date = $date->format('Y-m-d ');

		if ($tgl != "-1") {
			$this->db->select('transaksi.id_transaksi,detail_transaksi.kode_order, detail_transaksi.no_kursi AS NoPesanan,detail_transaksi.id AS id_item,CONCAT(menu.nama_menu, \' (\',jumlah_pesanan,\')\') AS Name
			,  DATE_FORMAT( detail_transaksi.create_at, \'%d/%m/%Y %H:%i\' ) AS TglPesan, detail_transaksi.status_antar AS StatusAntar, detail_transaksi.status_bayar AS StatusBayar, jumlah_pesanan AS Quantity, menu.desk_menu AS Description, detail_transaksi.total_harga_menu AS Price,detail_transaksi.cat_pesan AS CatPesan')
					  ->from('detail_transaksi')
                      ->join('menu','menu.id_menu = detail_transaksi.id_menu')
                      ->join('transaksi','transaksi.kode_order = detail_transaksi.kode_order')
					  ->where('kode_tenant',$id_tenant)                      ;
                      //->where('date_format(transaksi.waktu_transaksi,"%Y-%m-%d")', 'CURDATE()', FALSE);
           
            if($KodeOrder){
                       $this->db->where('detail_transaksi.kode_order',$KodeOrder);
                                
                       
            }else{
                $this->db->where('date_format(transaksi.waktu_transaksi,"%Y-%m-%d")', 'CURDATE()', FALSE);
            }
            if($StatusAntar=="1" or $StatusAntar=="0") {
                $this->db->where('detail_transaksi.status_antar',$StatusAntar);
            }

            if($StatusBayar=="1" or $StatusBayar=="0") {
                $this->db->where('detail_transaksi.status_bayar',$StatusBayar);
            }


                      //->where('DATEDIFF(CURDATE(), create_at)<3')
                      $result =  $this->db->order_by("kode_order asc")
					  ->get();
		}
		else {
			$result = $this->db->select('detail_transaksi.id AS ID, CONCAT(menu.nama_menu, \' (\',jumlah_pesanan,\')\') AS Name
			, CONCAT(\'No. Meja : \', detail_transaksi.no_kursi) AS NoPesanan, DATE_FORMAT( detail_transaksi.create_at, \'%d/%m/%Y %H:%i\' ) AS TglPesan, status_antar AS StatusAntar, status_bayar AS StatusBayar, jumlah_pesanan AS Quantity, menu.desk_menu AS Description, total_harga_menu AS Price,cat_pesan AS CatPesan')
					  ->from('detail_transaksi')
					  ->join('menu','menu.id_menu = detail_transaksi.id_menu')
					  ->where('kode_tenant',$id_tenant)
					  ->get();
		}
        return $result->result_array();
    }

    public function get_pesanan_date($id_tenant, $tgl)
    {
		if ($tgl != "-1") {
			$result = $this->db->select('detail_transaksi.id AS ID, CONCAT(menu.nama_menu, \' (\',jumlah_pesanan,\')\') AS Name
			, CONCAT(\'No. Meja : \', detail_transaksi.no_kursi) AS NoMeja, DATE_FORMAT( detail_transaksi.create_at, \'%d/%m/%Y %H:%i\' ) AS TglPesan, status_antar AS StatusAntar, status_bayar AS StatusBayar, jumlah_pesanan AS Quantity, menu.desk_menu AS Description, total_harga_menu AS Price,cat_pesan AS CatPesan')
					  ->from('detail_transaksi')
					  ->join('menu','menu.id_menu = detail_transaksi.id_menu')
					  ->where('kode_tenant',$id_tenant)
					  ->where('detail_transaksi.create_at LIKE \''.$tgl.'%\'')
					  ->get();
		}
		else {
			$result = $this->db->select('detail_transaksi.id AS ID, CONCAT(menu.nama_menu, \' (\',jumlah_pesanan,\')\') AS Name
			, CONCAT(\'No. Meja : \', detail_transaksi.no_kursi) AS NoMeja, DATE_FORMAT( detail_transaksi.create_at, \'%d/%m/%Y %H:%i\' ) AS TglPesan, status_antar AS StatusAntar, status_bayar AS StatusBayar, jumlah_pesanan AS Quantity, menu.desk_menu AS Description, total_harga_menu AS Price,cat_pesan AS CatPesan')
					  ->from('detail_transaksi')
					  ->join('menu','menu.id_menu = detail_transaksi.id_menu')
					  ->where('kode_tenant',$id_tenant)
					  ->get();
		}
        return $result->result_array();
    }

    public function fn_ceknopesanan($id_tenant)
    {
        $result=$this->db->query("call sp_ceknopesanan('$id_tenant')");
        return $result->result_array();
    }




/* Netkrom punya
    public function get_notification($id_tenant)
    {
        $result = $this->db->select('message')
                  ->from('notifications')
                  ->where('id_tenant',$id_tenant)
                  ->get();
        return $result->result_array();
    } */

    public function get_notification($id_tenant)
    {
        $date = new DateTime("now");

        $curr_date = $date->format('Y-m-d ');

        $result = $this->db->select("kode_order,no_meja,TotalPembayaran,waktu_bayar")
                  ->from('transaksi')
                  ->where('id_tenant',$id_tenant)
                  ->where('status_bayar',1)
                  ->where('status_antar',0)
                  ->where('DATE(waktu_transaksi)',$curr_date)
                  ->get();
        return $result->result_array();
        
    }

    public function get_transaction_id($id_order)
    {
        $result = $this->db->select('id_transaksi')
                  ->from('transaksi')
                  ->where('kode_order',$id_order)
                  ->get();
        return $result->result_array();
    }
    public function get_transaction_idtenant($id_trans){
        $result = $this->db->select('id_tenant')
                  ->from('transaksi')
                  ->where('id_transaksi',$id_trans)
                  ->get();
        return $result->result_array();
    }

    public function get_transaction_idorder($kode_order){
        $result = $this->db->select('id_transaksi')
                  ->from('transaksi')
                  ->where('kode_order',$kode_order)
                  ->get();
        return $result->result_array();
    }

    public function delete_notification($id_tenant)
    {
        $this->db->where('id_tenant',$id_tenant)->delete("notifications");
    }

    public function delete_pesanan($id_transaksi,$id_order)
    {
        $this->db->where('id_transaksi',$id_transaksi)->delete("notifications");
        $this->db->where('kode_order',$id_order)->delete("detail_transaksi");
        $this->db->where('id_transaksi',$id_transaksi)->delete("transaksi");
    }

    public function delete_pesanan_tenant($id_tenant,$no_pesanan)
    {
        $this->db->query("CALL sp_delete_tenant_order ($id_tenant,$no_pesanan)");


        
    }
    public function delivery_pesanan($id_order)
    {
        $this->db->set('status_antar', '1'); //value that used to update column  
        $this->db->where('id', $id_order); //which row want to upgrade  
        $this->db->update('detail_transaksi');  //table name


        
    }

    public function get_transaction_response($id_transaksi)
    {
        $result = $this->db->select('u_id_transaksi,waktu_transaksi')
                  ->from('transaksi')
                  ->where('id_transaksi',$id_transaksi)
                  ->get();
        return $result->result_array();
    }

	public function generate_code($id_tenant) {
		$kode = date("Ymd").$id_tenant;
		$trx = $this->db->where("kode_order LIKE '".$kode."%'")->order_by("id","desc")->get("detail_transaksi",1)->row();
		if ($trx != null) {
			$kode_order = $trx->kode_order;
			$append = substr($kode_order, (strlen($kode_order) - strlen($kode)) * -1);
			$append = intval($append);
			$append++;

			if (strlen($append) == 1)
				$append = "0".$append;

			$kode = $kode.$append;
		}
		else {
			$kode = $kode."01";
		}

		return $kode;
    }
    

    public function search_history($id_tenant,$tanggal_start,$tanggal_end)
    {

            $this->db->select("transaksi.kode_order,id_transaksi,waktu_transaksi, IF(status_bayar = '0', 'Belum Bayar', 'Sudah Bayar') AS status_bayar, TotalPembayaran, no_meja, tenant.nama_tenant")
                  ->from("transaksi")
                  ->join('tenant','tenant.id_tenant=transaksi.id_tenant');
      
              //if (!empty($status)) {
            $this->db->where('status_bayar', 1);
              //}
              if (!empty($id_tenant)) {
                  $this->db->where("transaksi.id_tenant", $id_tenant);
              }
              if (empty($tanggal_start)) {
                  $tanggal_start = date('Y-m-d');
              }
              if (empty($tanggal_end)) {
                  $tanggal_end = date('Y-m-d');
              }
              $this->db->where("transaksi.waktu_transaksi >=", $tanggal_start . ' 00:00:00')
                  ->where("transaksi.waktu_transaksi <=", $tanggal_end . ' 23:59:59');
        $result =  $this->db->get();
        
        return $result->result_array();
    }

}

 ?>
