<?php
use Jenssegers\Date\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Fee_detail_model extends MY_Model
{
    const TYPE_KATEGORI = 'kategori';
    const TYPE_TRANSACTION = 'transaction';
    const UNIT_PERCENTAGE = '%';
    const UNIT_RUPIAH = 'Rp.';
    
    protected $table = 'fee_details';
    protected $form = array(
		'id' => array(
			'label' => 'ID', 
			'helper' => 'form_hidden',
			'rules' => 'trim',
        ),
        'fee_id' => array(
			'label' => 'Fee', 
            'helper' => 'form_hidden',
			'rules' => 'trim',
        ),
		'jenis' => array(
			'helper' => 'form_radiolabel',
			'label' => 'Jenis Fee',
			'rules' => 'trim|required'
		),
		'kategori_id' => array(
			'helper' => 'form_dropdownlabel',
			'label' => 'Kategori',
			'rules' => 'trim|required'
		),
		'fee_account_id' => array(
			'helper' => 'form_dropdownlabel',
			'label' => 'Fee Account',
			'rules' => 'trim|required',
		),
		'jumlah' => array(
			'helper' => 'form_inputlabel',
			'label' => 'Jumlah',
			'rules' => 'trim|required'
		),
		'satuan' => array(
			'helper' => 'form_dropdownlabel',
			'label' => 'Satuan',
			'rules' => 'trim|required'
		)
	);

	public function get_form()
	{
		$this->form['jenis']['options'] = array(
			Fee_detail_model::TYPE_KATEGORI => ucwords(Fee_detail_model::TYPE_KATEGORI),
			Fee_detail_model::TYPE_TRANSACTION => ucwords(Fee_detail_model::TYPE_TRANSACTION)
		);

		$this->form['satuan']['options'] = array(
			Fee_detail_model::UNIT_PERCENTAGE => Fee_detail_model::UNIT_PERCENTAGE,
			Fee_detail_model::UNIT_RUPIAH => Fee_detail_model::UNIT_RUPIAH
		);

		return $this->form;
	}

	public function fee_datatable($fee_id)
	{
		$this->datatables->select("$this->table.id, $this->table.jenis, $this->table.kategori_id, $this->table.fee_account_id, $this->table.jumlah, $this->table.satuan, kategori.nama AS nama_kategori, fee_accounts.nama AS nama_fee_account")
			->from($this->table)
			->join('kategori', "$this->table.kategori_id = kategori.id", 'left')
			->join('fee_accounts', "$this->table.fee_account_id = fee_accounts.id", 'left')
			->where("$this->table.fee_id", $fee_id);

		return $this->datatables->generate();
	}
}