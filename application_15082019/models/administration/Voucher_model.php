<?php
use Jenssegers\Date\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Voucher_model extends MY_Model
{
    protected $table = 'm_tu';
    protected $user = 'auth_users';
    protected $id_field = 'noAGP';
    protected $default_sort_field = 'nama_depan';
    protected $default_sort_order = 'asc';

    public function datatable()
    {
        $this->datatables->select("noAGP, CONCAT_WS(' ',TRIM(nama_depan),TRIM(nama_tengah),TRIM(nama_keluarga)) AS nama , nominal, tgl_exp, sts_tu, sts_data, kdJnstu, kodetu, tgl_input, sts_exp, u_id ,SUM(transaksi.`TotalPembayaran`) AS TotalPembelian")
            ->from($this->table)
            ->join('transaksi','transaksi.no_agp=m_tu.noAGP','Left')
            ->group_by('noAGP, nama_depan, nama_tengah, nama_keluarga, nominal, tgl_exp, sts_tu, sts_data, kdJnstu, kodetu, tgl_input, sts_exp, u_id ');
        return $this->datatables->generate();
    }

    public function datatabletrans($no_agp)
    {
        
        $this->datatables->select("m_tu.`nama_depan`,tenant.nama_tenant,transaksi.`TotalPembayaran`,transaksi.`waktu_bayar`,transaksi.`waktu_transaksi`,menu.`nama_menu`,menu.`harga_menu`")
            ->from('transaksi')
            ->join('m_tu','m_tu.noAGP=transaksi.no_agp')
            ->join('detail_transaksi','detail_transaksi.kode_order=transaksi.kode_order')
            ->join('menu','menu.id_menu=detail_transaksi.id_menu')
            ->join('tenant','tenant.id_tenant=transaksi.id_tenant')
            
            ->where('transaksi.no_agp' , $no_agp);
            
        return $this->datatables->generate();
    }


    public function get_user_id()
    {
        $this->db->from($this->user);
        $this->db->where('role_id' , 6);
        return $this->db->get()->result();
    }

    public function get_user_name($id)
    {
        $query = $this->db->query(
            "SELECT first_name,last_name FROM auth_users WHERE id='$id'");
        $row = $query->row();
        return $row->last_name;

    }

    function get_data_pemilik()
    {
      $this->db->select('*')->from('auth_users')->order_by('first_name asc, last_name asc');
      return $this->db->get()->result();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id_tenant' , $id);
        return $this->db->get()->result();
    }

    public function get_by_uid($id)
    {
        $this->db->from($this->table);
        $this->db->where('id_tenant' , $id);
        return $this->db->get()->row();
    }

    public function get_last_id()
    {
        $query = $this->db->query(
            "SELECT id_tenant FROM tenant ORDER BY id_tenant DESC LIMIT 1");
        $row = $query->row();
        return $row->id_tenant;
    }

    
    function mylist()
    {
        $this->db->from($this->table)
            ->order_by('nama_depan asc');
        return $this->db->get()->result_array();
    }
}
