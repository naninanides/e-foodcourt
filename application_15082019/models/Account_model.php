<?php
use Jenssegers\Date\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Account_model extends MY_Model 
{
    protected $table = 't_accounts';

    public function get_by_account($account, $name, $balance)
    {
        $acc = $this->db->where("acc_number", $account)
			->get($this->table,1)->row();

		if ($acc == null) 
		{
			$names = explode(' ', $name);
			$username = str_replace(' ', '', strtolower($name));

			$auth_user_id = $this->user_model->insert(array(
				'first_name' => $names[0],
				'last_name' => $names[count($names) - 1],
				'username' => $account,
				'email' => $username . '@names.com',
				'role_id' => 5
			));

			$datas = array('acc_number' => $account,
							'name' => $name,
							'auth_user_id' => $auth_user_id,
							'balance' => $balance);

			$this->insert($datas);

			return $this->db->insert_id();
		}
		else 
			return $acc->id;
    }
}