<?php
use Jenssegers\Date\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class ReportTopupModel extends MY_Model
{
    protected $table = 'V_reportTopup';
    protected $user = 'auth_users';
    protected $id_field = 'noAGP';
    protected $default_sort_field = 'tgl_tu';
    protected $default_sort_order = 'desc';

    public function datatable()
    {
        $this->datatables->select("noagp,kodetu,tgl_tu,u_id,harga,kdJnstu,nama_lengkap,operator")
            ->from($this->table);
          
            
        return $this->datatables->generate();
    }

    function get_data_karyawan()
    {
      $this->db->select('*')->from('v_karyawan')->order_by('nama_lengkap asc');
      return $this->db->get()->result();
    }

    function mylist()
    {
        $this->db->select('*')->from('v_karyawan')->order_by('nama_lengkap asc');
      return $this->db->get()->result();
    }

    public function reporttopupget($tipe = '', $noagp = '', $tanggal_start = '', $tanggal_end = '')
    {
        $this->datatables->select("noagp, nama_lengkap, harga,  tgl_tu, kdJnstu, operator")
            ->from($this->table);

        if (!empty($tipe) AND $tipe!='' ) {
            $this->datatables->where('tipe', $tipe);
        }
        if (!empty($noagp)) {
            $this->datatables->where("noagp", $noagp);
        }
        if (empty($tanggal_start)) {
            $tanggal_start = date('Y-m-d');
        }
        if (empty($tanggal_end)) {
            $tanggal_end = date('Y-m-d');
        }
        //$tanggal_start='2019-07-10';

        $this->datatables->where("$this->table.tgl_tu >=", $tanggal_start . ' 00:00:00')
            ->where("$this->table.tgl_tu <=", $tanggal_end . ' 23:59:59');
        return $this->datatables->generate();
    }
}
