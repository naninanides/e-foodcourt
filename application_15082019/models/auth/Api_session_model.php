<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api_session_model extends MY_Model {

    protected $table = 'api_session';
    protected $id_field = 'id';
    protected $default_sort_field = 'id';
    protected $default_sort_order = 'ASC';
	protected $has_updated_field = FALSE;
    protected $has_created_field = FALSE;
    protected $filters = array();
    protected $filter_fields = array(
        'id' => 'LIKE',
    );

    public function prep_query() {
        parent::prep_query();
    }

    public function prep_filters($filters = array()) {
        while (list($key, $val) = each($this->filters)) {
            if ($val != "") {
                $this->db->where('TRIM(' . $key . ') LIKE \'%' . trim($val) . '%\'', null, false);
            }
        }
        reset($this->filters);
    }

    /**
     * Get Session by access_token
     * 
     * @param string $access_token
     * @return object Api_session
     */
    function get_by_token($access_token) {
        $query = $this->db->get_where($this->table, array($this->table . '.access_token' => $access_token));
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }
}
