<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
*
 * AES Library
 * 
 * @package App
 * @category Libraries
 * @author Yan Virga
 */

 class Aes_encryption { //versi Pak Yan
	
	
    function __construct() {
    }

	function decrypt($str){ 
		$key = "594740586e6356483139727947774041";
		$iv = "DQLGBUa2NrwI36lS";
		
		$str = base64_decode($str);
		$str = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $str, MCRYPT_MODE_CBC, $iv);
		$block = mcrypt_get_block_size('rijndael_128', 'cbc');
		$pad = ord($str[($len = strlen($str)) - 1]);
		$len = strlen($str);
		$pad = ord($str[$len-1]);
		return substr($str, 0, strlen($str) - $pad);
   }

	function encrypt($text){
		$key = "594740586e6356483139727947774041";
		$iv = "DQLGBUa2NrwI36lS";

		$block = mcrypt_get_block_size('rijndael_128', 'cbc');
		$pad = $block - (strlen($text) % $block);
		$text .= str_repeat(chr($pad), $pad);
		return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $text, MCRYPT_MODE_CBC, $iv));
   }

} 


/* class Aes_encryption {
	
	private $KeyString = "34615545716f4c336269474253496665";
	
    function __construct() {
    }

    function decrypt($encrypted)
	{
		$KeyString = pack("H*",$this->KeyString);
		$encrypted = base64_decode($encrypted);
		$SaltString = substr($encrypted,0,32);
		$IVString = substr($encrypted,32,32); 
		$encrypted = substr($encrypted,64); 

		$HashIterations = 7; 
		$devkeylength = 20; 
		$devkey = $this->PBKDF1($KeyString,$SaltString,$HashIterations,$devkeylength);
		$Key = substr($devkey, 0, 16);
		$devkey = $this->PBKDF1($IVString,$SaltString,$HashIterations,$devkeylength);
		$IV = substr($devkey, 0,16);

		$encrypted = base64_decode($encrypted); 

		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $Key, $encrypted, MCRYPT_MODE_CBC, $IV), "\0");;
		return $decrypted;
	}

	function encrypt($message) 
	{
		$KeyString = pack("H*",$this->KeyString);
		$SaltString = $this->randomize(); 
		$IVString = $this->randomize(); 
		$HashIterations = 7; 
		$devkeylength = 20; 
		$devkey = $this->PBKDF1($KeyString,$SaltString,$HashIterations,$devkeylength);
		$Key = substr($devkey, 0, 16);
		$devkey = $this->PBKDF1($IVString,$SaltString,$HashIterations,$devkeylength);
		$IV = substr($devkey, 0,16);

		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $Key, $message, MCRYPT_MODE_CBC, $IV));
		$encrypted = $SaltString.$IVString.$encrypted;
		return base64_encode($encrypted);
	} 

	function PBKDF1($pass, $salt, $count, $dklen)
	{
		$t = $pass.$salt;
		$t = sha1($t, true);
		for($i=2; $i <= $count; $i++)
		{
			$t = sha1($t, true);
		}
		$t = substr($t,0,$dklen);
		return $t;
	}

	function randomize($length = 32) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.<>/:;[]{}!@#$%^&*()-=_+';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

}

 */