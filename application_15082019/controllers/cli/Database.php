<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Database extends CI_Controller
{
    public function seeder($class = '')
    {
        if (! $this->input->is_cli_request()) {
            show_error('Seeding can only happen from the command line.');
        }

        if ($class == 'all') {
            $class = '';
        }

        $this->load->library('seeder');

        if (!empty($class)) {
            $this->seeder->call($class);
        } else {
            $this->seeder->call('KategoriSeeder');
        }
    }

    public function migrate()
    {
        $this->load->library('migration');

        if ($this->migration->current() === false) {
            show_error($this->migration->error_string());
        }
    }
}
