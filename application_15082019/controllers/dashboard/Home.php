<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Home extends Admin_Controller {

	public function index()
	{
		$this->template->build('dashboard/test');
	}
}
