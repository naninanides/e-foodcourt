<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Home controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class Voucher extends Admin_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('administration/Voucher_model');
        $this->load->model('auth/User_model');
    }
    
    public function index()
    {
        $this->template
        ->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
        ->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
        ->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
        ->set_js_script('
        ')
        ->build('voucher/view');
    }
    
	function get_data_pemilik()
	{
        $data_user=$this->Voucher_model->get_data_pemilik();
        echo json_encode($data_user);
    }
    
	
    
	
    
}