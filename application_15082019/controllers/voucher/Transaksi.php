<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Home controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class Transaksi extends Admin_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('administration/Voucher_model');
        $this->load->model('auth/User_model');
    }
    function _remap($noagp) {
        $this->index($noagp);
    }
    
    public function index($noagp)
    {
        
        $this->template
        ->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
        ->set_css('../libs/jquery/plugins/integration/bootstrap/3/rowGroup.dataTables.min.css')
        ->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
        ->set_js('../bower_components/datatables/media/js/dataTables.rowGroup.min', TRUE)
        ->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
        ->set_js_script('
        ')
        ->build('voucher/transaksi');
    }
    
	function get_data_pemilik()
	{
        $data_user=$this->Transaksi_model->get_data_pemilik();
        echo json_encode($data_user);
    }
    
	
    
	
    
}