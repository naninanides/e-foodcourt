<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Account API.
 * 
 * @package App
 * @category Controller
 * @author Adam M Riyadi
 */
class Account extends Api_Controller 
{
    public function __construct() {
        parent::__construct();
        $this->load->library('Crypto/aes_encryption');
        $this->load->model('administration/tenant_model');
    }

	public function index()
	{
		$array["result"] = "Resource Not Found !!!";
		echo json_encode($array);
	}

	public function connect()
	{
		$array["result"] = "ok";
		echo json_encode($array);
	}

	public function token()
	{
		$timespan = $this->config->item("api_sess_expiration");
		$client_path = $this->config->item("client_path");
		
        $this->load->language('auth');
		
		$header = $this->input->get_request_header("Authorization", true);
		$header = trim(str_ireplace("Bearer","",$header));
		$split = explode(":", $header);
		if (count($split) < 2) 
			$split[1] = "";

		$username = $this->aes_encryption->decrypt($split[0]);

		$password = $this->aes_encryption->decrypt($split[1]);

		//$username = $split[0];
		//$password = $split[1];
        if ($username && $password) {
            // get user from database
            $user = $this->user_model->get_by_username($username);
            if ($user && $this->user_model->check_password($password, $user->password)) {
				$uid = $user->id;
				$id_tenant = $user->id_tenant;
				$tenant = $this->tenant_model->get_by_uid($id_tenant);
				$id_tenant = 0;
				if ($tenant != null) {
					$id_tenant = $tenant->id_tenant;

					$date = new DateTime();
					$timespan = 24 * 3600;
					$date->add(new DateInterval('PT'.$timespan.'S'));
					$expires = $date->format('Y-m-d H:i:s');

					$access_token = md5($username.microtime());

					$array["user_id"] = $user->id;
					$array["username"] = $username;
					$array["first_name"] = $user->first_name;
					$array["last_name"] = $user->last_name;
					$array["role_id"] = $user->role_id;
					$array["access_token"] = $access_token;
					$array["_expires"] = $expires;
					$array["_issued"] = date("Y-m-d H:i:s");
					$array["expires_in"] = $timespan;
					$array["Id_tenant"] = $id_tenant;

					$datas = array();
					$datas['access_token'] = $access_token;
					$datas['user_id'] = $user->id;
					$datas['expires'] = $expires;
					$datas['session_data'] = json_encode($array);
					$this->api_session_model->insert($datas);

					
					$array["user_id"] = $this->aes_encryption->encrypt($user->id);
					$array["username"] = $this->aes_encryption->encrypt($username);
					$array["role_id"] = $this->aes_encryption->encrypt($user->role_id);
					$array["access_token"] = $this->aes_encryption->encrypt($access_token);

					echo json_encode($array);
				}
				else {
					$this->output->set_status_header(401);

					$array["result"] = "error";
					$array["message"] = lang('login_attempt_failed');
					//$array["message"] = $this->aes_encryption->encrypt("soto1"); 

					echo json_encode($array);
				}
            } else {
				$this->output->set_status_header(401);

				$array["result"] = "error";
				//$array["message"] = $this->aes_encryption->encrypt("soto1"); //lang('login_attempt_failed');
				

				echo json_encode($array);
			}
        }
        else {
			$this->output->set_status_header(401);

			$array["result"] = "error";
			$array["message"] = lang('username_or_password_empty');
			//$array["message"] = $this->aes_encryption->encrypt("soto1"); //lang('login_attempt_failed');
			echo json_encode($array);
        }
	}

	public function logout()
	{
		$header = $this->input->get_request_header("Authorization", true);
		$access_token = $this->aes_encryption->decrypt(trim(str_ireplace("Bearer","",$header)));

		if ($access_token == null || $access_token == "") {
			$this->output->set_status_header(401);
			$array["result"] = "Access Not Allowed !!!";
			echo json_encode($array);
		}
		else {
			$sess = $this->api_session_model->get_by_token($access_token);
			if ($sess != null) {
				$this->api_session_model->delete($sess->id);
			}
			$array["result"] = "Success";
			
			$user_data = json_decode($sess->session_data);
			echo json_encode($array);
		}
	}
}