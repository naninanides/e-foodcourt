<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Home controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class ReportTopup extends Admin_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('topup/ReportTopupModel');
        $this->load->model('auth/user_model');
    }
    
    public function index()
	{
		echo $this->ReportTopupModel->datatable();
	}
    

	
}