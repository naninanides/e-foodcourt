<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* This can be removed if you use __autoload()
 in config.php OR use Modular Extensions
 @noinspection PhpIncludeInspection */

require_once 'application/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Dashboard extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
        $this->load->model('dashboard_model');
    }

    public function tenant_get()
    {
        $id = $this->get('id');
        $tenant = $this->dashboard_model->get_tenant($id);
        $this->response($tenant,200);
    }

    public function index_get($id_tenant)
    {
        //$id_tenant = $this->get('id');

        //echo $id_tenant;

        $transaksi = $this->dashboard_model->get_total_transaksi($id_tenant);
        $penjualan = $this->dashboard_model->get_total_penjualan($id_tenant);
        //$tenant = $this->dashboard_model->get_tenant($id_tenant);
        $total = 0;
        $tenant_dashboard = array();
        $transaksi = array_values($transaksi[0]);
        foreach ($penjualan as $row)
          $total += $row['jumlah_pesanan']*$row['harga_menu'];
        $total = strval($total);
        $tenant_dashboard = array(
          'total_penjualan' => $total,
          'total_transaksi' => $transaksi[0]
        );
        $result = array($tenant_dashboard);
        $this->response($result,200);
    }
    public function chart_summary_get($id_tenant)
    {
        //$tenant = $this->get('id_tenant');
        if ($id_tenant != '')
        {
          $chart = $this->dashboard_model->get_chartSummary($id_tenant);
          $this->response($chart, 200);
        }
        else $this->response(NULL, 404);

    }

    public function chart_menu_get()
    {
        $tenant = $this->get('id_tenant');
        $chart = $this->dashboard_model->get_chartMenu($tenant);
        $this->response($chart, 200);
    }
}



?>
