<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* This can be removed if you use __autoload()
 in config.php OR use Modular Extensions
 @noinspection PhpIncludeInspection */

require_once 'application/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Transaction extends REST_Controller
{
    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    public function index_get()
    {
        $id = $this->get('id');
        $this->load->model('trans_model');
        $transactions = $this->trans_model->get_transaksi($id);
        $transaction = array();
        foreach ($transactions as $row)
        {
            $result = new stdClass();
            $result->id_transaksi = $row['id_transaksi'];
            $result->id_kasir = $row['id_kasir'];
            $result->id_cust = $row['id_cust'];
            $result->created = $row['created'];
            $result->status_bayar= $row['status_bayar'];
            $result->no_meja= $row['no_meja'];
            $result->menus = $this->trans_model->get_menu_transaksi(
              $row['id_transaksi']);
            $transaction[$row['id_transaksi']] = $result;
        }
        $transaction = array_values($transaction);
        $this->response($transaction, 200);
    }
    public function index_put()
    {
        $_id = $this->put('id');
        $data = array('STATUS_BAYAR' => 1);
        $this->db->where('id_transaksi',$_id);
        $update = $this->db->update('transaksi',$data);
        echo $this->db->last_query();
        if ($update)
        {
            $this->response($data,201);
        }
        else
        {
            $this->response(array('status' => 'fail', 502));
        }
    }
    public function index_post()
    {

    }
}
?>
