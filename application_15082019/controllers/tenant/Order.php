<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Order extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/menu_model');
	}

	public function index()
	{	
		$this->load->vars('menu', $this->menu_model->getData());

		$this->template->build('tenant/Order');
	}

	public function indexMakanan()
	{	
		$this->load->vars('menu', $this->menu_model->getDataMakanan());
		echo $this->load->view('menulistmak.php');
	}

	public function change_status() {
    $id_menu = $this->input->post('id_menu');

    $status=$this->menu_model->update_status($id_menu);
    
    
    echo $status;


    
    // echo "1";
    // exit;
}
public function get_data_modal(){
    	$id_menu=$this->input->get('selected_id');
    	$result=$this->menu_model->getDataModal($id_menu);
    	echo json_encode($result);
    }

    public function data_form(){
    	$sel_id_menu=$this->input->post('selected_id_menu');
    	$sel_nama_menu=$this->input->post('sel_nama_menu');
    	$sel_harga_menu=$this->input->post('sel_harga_menu');
    	if(preg_match("/^[0-9,]+$/", $sel_harga_menu)) $sel_harga_menu = str_replace(',', '', $sel_harga_menu);
    	$totalQty=$this->input->post('totalQty');
    	$totalHarga = (int)$sel_harga_menu*(int)$totalQty;

    	$form_data= array($sel_id_menu,$sel_nama_menu,$sel_harga_menu,$totalQty,$totalHarga);

    	echo json_encode($form_data);
    }

    public function data_order(){
    	$array_tbl=$this->input->post('array');
    	$diskon=$this->input->post('diskontotal');
    	$tax=$this->input->post('taxTotal');
    	$pembayaran=$this->input->post('totalHarga');
    	$nomeja=$this->input->post('nomeja');
        $namapemesan=$this->input->post('namapemesan');
    	$totalPesanan=$this->input->post('totalPesanan');
    	$result=$this->menu_model->dataOrder($array_tbl,$diskon,$totalPesanan,$pembayaran,$nomeja,$namapemesan);
        return true;
    	
    	
    }


}
