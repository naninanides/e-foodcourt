<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Setting extends Admin_Controller {

	protected $menu_form = array(
		'id_menu' => array(
			'helper' => 'form_hidden'
		),
		'id_tenant' => array(
			'helper' => 'form_hidden'
		),
		'nama_menu' => array(
			'label'	=> 'lang:nama_menu',
			'rules' => 'trim|max_length[50]',
			'helper' => 'form_inputlabel'
		),
		'desk_menu' => array(
			'label'	=> 'lang:desk_menu',
			'rules' => 'trim|max_length[255]',
			'helper' => 'form_inputlabel'
		),
		'status_menu' => array(
			'label' => 'lang:status_menu',
			'rules' => 'trim|max_length[255]',
			'helper' => 'form_inputlabel'	
		),
		'harga_menu' => array(
			'label' => 'lang:harga_menu',
			'rules' => 'trim|max_length[255]',
			'helper' => 'form_inputlabel'
		),
		'foto_menu' => array(
			'label' => 'lang:foto_menu',
			'rules' => 'trim|max_length[255]',
			'helper' => 'form_inputlabel',
		)
	);

	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/menu_model');
	}

	public function index()
	{	
		$this->template
				->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
				->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
				->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
				->set_js_script(' 
				')
				->build('tenant/Setting');
	}

	function add()
	{
		$this->_updatedata();
	}

	function update($id)
	{
		$this->_updatedata();
	}

	function delete($id)
	{
		$menu = $this->menu_model->get_by_id($id);
		if ($menu)
			$this->menu_model->delete($id);
		
		redirect('tenant/Setting');
	}

	function hapus($id){
		$where = array('id_menu' => $id);
		$this->menu_model->hapus_data($where,'menu');
		redirect('tenant/Setting');
	}

	function tambah(){

		$foto_menu = $this->input->post('foto_menu');
		$nama_menu = $this->input->post('nama_menu');
		$desk_menu = $this->input->post('desk_menu');		
		$status_menu = $this->input->post('status_menu');
		$harga_menu = $this->input->post('harga_menu');		

		$data = array(
			'nama_menu' => $nama_menu,
			'desk_menu' => $desk_menu,
			'status_menu' => $status_menu,
			'harga_menu' => $harga_menu,
			'foto_menu' => $foto_menu
			);

	}
}
