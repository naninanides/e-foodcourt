<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Hasilorder extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/trans_model');
	}

	public function index()
	{	
		$tenants = false;
		$this->load->model('administration/Tenant_model');
		$tenants = $this->Tenant_model->get_list();
		
		$this->template
			->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
			->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
			->set_js('../bower_components/datatables/media/js/dataTables.buttons.min', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.flash.min', TRUE)
			->set_js('../bower_components/datatables/media/js/jszip.min', TRUE)
			->set_js('../bower_components/datatables/media/js/pdfmake.min', TRUE)
			->set_js('../bower_components/datatables/media/js/vfs_fonts', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.html5.min', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.print.min', TRUE)
			->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
			
			->set_js_script(' 
			')
			->build('tenant/Hasilorder', array('tenants' => $tenants));
	}

	public function download()
	{
		$status_bayar = $this->input->get('status_bayar');
		$id_tenant = $this->input->get('id_tenant');
		$tanggal_start = $this->input->get('tanggal_start');
		$tanggal_end = $this->input->get('tanggal_end');

		$rows = $this->trans_model->download($status_bayar, $id_tenant, $tanggal_start, $tanggal_end);

		ini_set('memory_limit', '256M');
		ini_set('max_execution_time', '0');
		$mpdf = new \Mpdf\Mpdf(array(
			'mode' => 'utf-8', 
			'format' => 'A4', 
			'orientation' => 'P'
		));
		$html = $this->load->view('tenant/hasilorder_download', array('rows' => $rows), TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output('hasil_order.pdf', 'D');
	}

	public function indexMakanan()
	{	
		$this->load->vars('menu', $this->menu_model->getDataMakanan());
		echo $this->load->view('menulistMak.php');
	}

	public function change_status() 
	{
		$id_menu = $this->input->post('id_menu');

		$status=$this->menu_model->update_status($id_menu);
		
		echo $status;
	}

	public function edit()
	{
		$id_transaksi = $_GET["id_transaksi"];
		$this->load->vars('detailtrans', $this->trans_model->getDataDetail($id_transaksi));
		$this->template->build('tenant/Editorder');
    }
    
	public function get_data_modal()
	{
    	$id_menu=$this->input->get('selected_id');
    	$result=$this->menu_model->getDataModal($id_menu);
    	echo json_encode($result);
    }

	public function data_form()
	{
    	$sel_id_menu=$this->input->post('selected_id_menu');
    	$sel_nama_menu=$this->input->post('sel_nama_menu');
    	$sel_harga_menu=$this->input->post('sel_harga_menu');
    	if(preg_match("/^[0-9,]+$/", $sel_harga_menu)) $sel_harga_menu = str_replace(',', '', $sel_harga_menu);
    	$totalQty=$this->input->post('totalQty');
    	$totalHarga = (int)$sel_harga_menu*(int)$totalQty;

    	$form_data= array($sel_id_menu,$sel_nama_menu,$sel_harga_menu,$totalQty,$totalHarga);

    	echo json_encode($form_data);
    }

	public function data_order()
	{
    	$array_tbl=$this->input->post('array');
    	$diskon=$this->input->post('diskontotal');
    	$tax=$this->input->post('taxTotal');
    	$pembayaran=$this->input->post('totalHarga');
    	$nomeja=$this->input->post('nomeja');
    	$totalPesanan=$this->input->post('totalPesanan');
    	$result=$this->menu_model->dataOrder($array_tbl,$diskon,$totalPesanan,$pembayaran,$nomeja);
    }

	function get_data_tenant()
	{
            $data_user=$this->trans_model->get_dt_tenant();
            echo json_encode($data_user);
    }
}
