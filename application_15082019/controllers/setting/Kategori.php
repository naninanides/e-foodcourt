<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Kategori controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Kategori extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/kategori_model');
	}

	public function index()
	{	
		$this->template
				->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
				->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
				->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
				->build('setting/kategori/index');
    }
    
    public function datatable()
    {
        echo $this->kategori_model->datatable();
    }

    public function add()
    {
        $this->_save();
    }

    public function edit($id)
    {
        $this->_save($id);
    }

    private function _save($id = 0)
    {
        $this->load->library('form_validation');
        $form = $this->kategori_model->form;
        
        // set default values for update
        if ($id > 0) {
            $product = $this->kategori_model->get_by_id($id);
			$this->form_validation->set_default($product);
		}
		
        $this->form_validation->init($form);
        
        if ($this->form_validation->run()) {
            $values = $this->form_validation->get_values();

            if ($id > 0) {
                $this->kategori_model->update($id, $values);
                $this->template->set_flashdata('success', 'Data Kategori Menu telah berhasil di-update.');
            } else {
                $id = $this->kategori_model->insert($values);
                $this->template->set_flashdata('success', 'Data Kategori Menu telah berhasil ditambah.');
            }
            redirect('setting/kategori');
        }

        $this->template->build('setting/kategori/form', array('id' => $id, 'form' => $this->form_validation));
    }
    
    function delete($id) 
    {
		$kategori = $this->kategori_model->get_by_id($id);
		if ($kategori)
		{
			$this->kategori_model->delete($id);
            $this->template->set_flashdata('info', 'Data Kategori Menu telah berhasil dihapus.');
            redirect('setting/kategori');
        } else
            show_404();
    }
}
