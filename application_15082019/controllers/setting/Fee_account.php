<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Fee_account controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Fee_account extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/fee_account_model');
	}

	public function index()
	{	
		$this->template
				->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
				->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
				->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
				->build('setting/fee_account/index');
    }
    
    public function datatable()
    {
        echo $this->fee_account_model->datatable();
    }

    public function add()
    {
        $this->_save();
    }

    public function edit($id)
    {
        $this->_save($id);
    }

    private function _save($id = 0)
    {
        $this->load->library('form_validation');
        $form = $this->fee_account_model->form;
        
        // set default values for update
        if ($id > 0) {
            $data = $this->fee_account_model->get_by_id($id);
			$this->form_validation->set_default($data);
		}
		
        $this->form_validation->init($form);
        
        if ($this->form_validation->run()) {
            $values = $this->form_validation->get_values();

            if ($id > 0) {
                $this->fee_account_model->update($id, $values);
                $this->template->set_flashdata('success', 'Data Fee Account telah berhasil di-update.');
            } else {
                $id = $this->fee_account_model->insert($values);
                $this->template->set_flashdata('success', 'Data Fee Account telah berhasil ditambah.');
            }
            redirect('setting/fee_account');
        }

        $this->template->build('setting/fee_account/form', array('id' => $id, 'form' => $this->form_validation));
    }
    
    function delete($id) 
    {
		$row = $this->fee_account_model->get_by_id($id);
		if ($row) {
			$this->fee_account_model->delete($id);
            $this->template->set_flashdata('info', 'Data Fee Account telah berhasil dihapus.');
            redirect('setting/fee_account');
        } else
            show_404();
    }
}
