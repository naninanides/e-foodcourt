<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Fee_detail controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Fee_detail extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/fee_detail_model');
	}

    public function datatable($fee_id)
    {
        echo $this->fee_detail_model->fee_datatable($fee_id);
    }

    public function add($fee_id)
    {
        $this->_save($fee_id);
    }

    public function edit($id)
    {
        $fee_detail = $this->fee_detail_model->get_by_id($id);
        if ($fee_detail) {
            $this->_save($fee_detail->fee_id, $id);
        } else
            show_404();
    }

    private function _save($fee_id, $id = 0)
    {
        $this->load->library('form_validation');
        $form = $this->fee_detail_model->get_form();
        $form['fee_id']['value'] = $fee_id;

        $this->load->model('administration/kategori_model');
        $kategories = $this->kategori_model->get_dropdown_array('nama');
        $form['kategori_id']['options'] = $kategories;

        $this->load->model('administration/fee_account_model');
        $accounts = $this->fee_account_model->get_dropdown_array('nama');
        $form['fee_account_id']['options'] = $accounts;
        
        // set default values for update
        if ($id > 0) {
            $fee_detail = $this->fee_detail_model->get_by_id($id);
			$this->form_validation->set_default($fee_detail);
		}
		
        $this->form_validation->init($form);
        
        if ($this->form_validation->run()) {
            $values = $this->form_validation->get_values();

            if ($id > 0) {
                $this->fee_detail_model->update($id, $values);
                $this->template->set_flashdata('success', 'Data Fee Detail telah berhasil di-update.');
            } else {
                $id = $this->fee_detail_model->insert($values);
                $this->template->set_flashdata('success', 'Data Fee Detail telah berhasil ditambah.');
            }
            redirect('setting/fee/view/' . $values['fee_id']);
        }

        $this->template->build('setting/fee_detail/form', array('id' => $id, 'form' => $this->form_validation, 'fee_id' => $fee_id));
    }
    
    function delete($id) 
    {
		$fee_detail = $this->fee_detail_model->get_by_id($id);
		if ($fee_detail) {
            $fee_id = $fee_detail->fee_id;
			$this->fee_detail_model->delete($id);
            $this->template->set_flashdata('info', 'Data Fee Detail telah berhasil dihapus.');
            redirect('setting/fee/view/' . $fee_id);
        } else
            show_404();
    }
}
