<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Home controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class Settingtenant extends Admin_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('administration/tenant_model');
        $this->load->model('auth/User_model');
    }
    
    public function index()
    {
        $this->template
        ->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
        ->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
        ->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
        ->set_js_script('
        ')
        ->build('setting/Settingtenant');
    }
    
	function get_data_pemilik()
	{
        $data_user=$this->tenant_model->get_data_pemilik();
        echo json_encode($data_user);
    }
    
	function tambah()
	{
        $config['upload_path'] = './images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']  = '20480000';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        
        if ($_FILES['foto_tenant']["name"] != "") {
            $output_file = "images/";
            $fileName = $output_file . time() . $_FILES["foto_tenant"]["name"];
            move_uploaded_file($_FILES["foto_tenant"]["tmp_name"], $fileName);
            $fileName = "";
            // throw new Exception(msg('msg_required', 'lbl_file_pendukung'), ERROR_CODE_DEFAULT);
        }
        $this->load->library('upload', $config);
        $this->upload->do_upload('foto_tenant');
        $id_tenant = $this->tenant_model->get_last_id()+1;
        $u_id = $this->input->post('data_pemilik');
        $nama_tenant = $this->input->post('nama_tenant');
        $pemilik_tenant = $this->tenant_model->get_user_name($u_id);
        $kat_tenant = $this->input->post('kat_tenant');
        $notel1_tenant = $this->input->post('notel1_tenant');
        $notel2_tenant = $this->input->post('notel2_tenant');
        $email_tenant = $this->input->post('email_tenant');
        $desk_tenant = $this->input->post('desk_tenant');
        $sewa_tenant = '';
        $created_by = $this->session->userdata('role_name');
        $created = date('Y-m-d H:i:s');
        
        $data = array(
        'id_tenant' => $id_tenant,
        'u_id' => $u_id,
        'nama_tenant' => $nama_tenant,
        'pemilik_tenant' => $pemilik_tenant,
        'kat_tenant' => $kat_tenant,
        'notel1_tenant' => $notel1_tenant,
        'notel2_tenant' => $notel2_tenant,
        'email_tenant' => $email_tenant,
        'desk_tenant' => $desk_tenant,
        'sewa_tenant' => $sewa_tenant,
        'created_by' => $created_by,
        'created' => $created,
        'foto_tenant' => $output_file.$fileName
        );
        
		$id_tenant = $this->tenant_model->tambah_data($data,'tenant');
		
		$this->user_model->update($u_id, array('id_tenant' => $id_tenant));
        redirect('setting/Settingtenant');
    }
    
	function edit($id)
	{
        $this->load->vars('data', $this->tenant_model->get_by_id($id));
        $this->load->vars('pemilik', $this->tenant_model->get_data_pemilik());
        
        $this->load->view('setting/Tenantedit');
    }
    
	function ubah_tenant()
	{
        if ($_FILES['foto_tenant']["name"] != "") {
            $output_file = "images/";
            $fileName = time() . $_FILES["foto_tenant"]["name"];
            move_uploaded_file($_FILES["foto_tenant"]["tmp_name"], $output_file . $fileName);
        } else {
            $fileName = "";
            // throw new Exception(msg('msg_required', 'lbl_file_pendukung'), ERROR_CODE_DEFAULT);
        }
        $id_tenant = $this->input->post('id_tenant');
        $u_id = $this->input->post('data_pemilik');
        $nama_tenant = $this->input->post('nama_tenant');
        $pemilik_tenant = $this->tenant_model->get_user_name($u_id);
        $kat_tenant = $this->input->post('kat_tenant');
        $notel1_tenant = $this->input->post('notel1_tenant');
        $notel2_tenant = $this->input->post('notel2_tenant');
        $email_tenant = $this->input->post('email_tenant');
        $desk_tenant = $this->input->post('desk_tenant');
        $status_tenant = $this->input->post('status_tenant');
        $updated_by = $this->session->userdata('role_name');
        $updated = date('Y-m-d H:i:s');
        
        $data = array(
        'u_id' => $u_id,
        'nama_tenant' => $nama_tenant,
        'pemilik_tenant' => $pemilik_tenant,
        'kat_tenant' => $kat_tenant,
        'notel1_tenant' => $notel1_tenant,
        'notel2_tenant' => $notel2_tenant,
        'email_tenant' => $email_tenant,
        'desk_tenant' => $desk_tenant,
        'status_tenant' => $status_tenant,
        'updated_by' => $updated_by,
        'updated' => $updated,
        'foto_tenant' => $output_file.$fileName
        // 'foto_tenant' => "testestes"
        );
		$this->tenant_model->update_tenant($data,'tenant',$id_tenant);
		$this->user_model->update($u_id, array('id_tenant' => $id_tenant));
        redirect('setting/Settingtenant');
    }
    
	function hapus($id)
	{
        $this->tenant_model->hapus_data($id,'tenant');
        redirect('setting/Settingtenant');
    }
    
}