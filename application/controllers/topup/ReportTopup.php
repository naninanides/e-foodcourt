<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Home controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class ReportTopup extends Admin_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('topup/ReportTopupModel');
        $this->load->model('auth/User_model');
    }
    
    public function index()
    {
        $karyawans = false;
        $this->load->model('topup/ReportTopupModel');
        $karyawans = $this->ReportTopupModel->mylist();
        
        $this->template
        ->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
			->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
			->set_js('../bower_components/datatables/media/js/dataTables.buttons.min', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.flash.min', TRUE)
			->set_js('../bower_components/datatables/media/js/jszip.min', TRUE)
			->set_js('../bower_components/datatables/media/js/pdfmake.min', TRUE)
			->set_js('../bower_components/datatables/media/js/vfs_fonts', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.html5.min', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.print.min', TRUE)
			->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
        ->set_js_script('
        ')
        ->build('topup/report', array('karyawans' => $karyawans));
        
    }
 
    public function download()
	{
		$tipe = $this->input->get('tipe');
		$noagp = $this->input->get('noagp');
		$tanggal_start = $this->input->get('tanggal_start');
		$tanggal_end = $this->input->get('tanggal_end');

		$rows = $this->ReportTopupModel->download($tipe, $noagp, $tanggal_start, $tanggal_end);

		ini_set('memory_limit', '256M');
		ini_set('max_execution_time', '0');
		$mpdf = new \Mpdf\Mpdf(array(
			'mode' => 'utf-8', 
			'format' => 'A4', 
			'orientation' => 'P'
		));
		$html = $this->load->view('tenant/hasilorder_download', array('rows' => $rows), TRUE);
		$mpdf->WriteHTML($html);
		$mpdf->Output('reporttopup.pdf', 'D');
	}

	
    public function ajax_list()
    {
        
        $tipe=isset($_GET['tipe']) ? $_GET['tipe'] : '';
        $noagp=isset($_GET['noagp']) ? $_GET['noagp'] : '0';
        $tanggal_start=isset($_GET['tanggal_start']) ? $_GET['tanggal_start'] : '';
        $tanggal_end=isset($_GET['tanggal_end']) ? $_GET['tanggal_end'] : '';
        
        $list = $this->ReportTopupModel->reporttopupget($tipe , $noagp, $tanggal_start, $tanggal_end);
        //$list = $this->ReportTopupModel->reporttopupget();
        echo $list;
        //print_r($this->db->last_query());  

    }
	
    
}