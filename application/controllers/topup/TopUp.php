<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class TopUp extends Admin_Controller {
  private $filename = "import_data"; // Kita tentukan nama filenya
  
  public function __construct(){
    parent::__construct();
    
    $this->load->model('topup/TopUpModel');
  }
  
  public function index(){
    $data = array(); // Buat variabel $data sebagai array
    
    if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
      // lakukan upload file dengan memanggil function upload yang ada di TopUpModel.php
      $upload = $this->TopUpModel->upload_file($this->filename);
      
      if($upload['result'] == "success"){ // Jika proses upload sukses
        // Load plugin PHPExcel nya
        #include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        include APPPATH.'third_party/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
        
        // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
        // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
        $data['sheet'] = $sheet; 
      }else{ // Jika proses upload gagal
        $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
      }
    }
    $this->template
        ->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
			->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
			->set_js('../bower_components/datatables/media/js/dataTables.buttons.min', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.flash.min', TRUE)
			->set_js('../bower_components/datatables/media/js/jszip.min', TRUE)
			->set_js('../bower_components/datatables/media/js/pdfmake.min', TRUE)
			->set_js('../bower_components/datatables/media/js/vfs_fonts', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.html5.min', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.print.min', TRUE)
			->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
        ->set_js_script('
        ')
        ->build('topup/UploadTopUp', $data);

   
  }
  
  public function form(){
    $data = array(); // Buat variabel $data sebagai array
    
    if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
      // lakukan upload file dengan memanggil function upload yang ada di TopUpModel.php
      $upload = $this->TopUpModel->upload_file($this->filename);
      
      if($upload['result'] == "success"){ // Jika proses upload sukses
        // Load plugin PHPExcel nya
        #include APPPATH.'third_party/PHPExcel/PHPExcel.php';
        include APPPATH.'third_party/PHPExcel.php';
        
        $excelreader = new PHPExcel_Reader_Excel2007();
        $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
        $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
        
        // Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
        // Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
        $data['sheet'] = $sheet; 
      }else{ // Jika proses upload gagal
        $data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
      }
    }
    
    $this->load->view('form', $data); 
  }
  
  public function import(){
    // Load plugin PHPExcel nya
    include APPPATH.'third_party/PHPExcel.php';
    
    $excelreader = new PHPExcel_Reader_Excel2007();
    
    $loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
    $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
    
    // Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
    $kodetu=date("Ymd").$this->TopUpModel->get_last_kodetu();
    $kodetu2="2019061701";
    $data = array();
    

    $numrow = 1;
    foreach($sheet as $row){
      // Cek $numrow apakah lebih dari 1
      // Artinya karena baris pertama adalah nama-nama kolom
      // Jadi dilewat saja, tidak usah diimport
      if($numrow > 1){
        // Kita push (add) array data ke variabel data
        array_push($data, array(
          'noAGP'=>$row['A'], // Insert data nis dari kolom A di excel
          'nik'=>$row['B'], // Insert data nis dari kolom A di excel
          'nama_depan'=>$row['C'], // Insert data nama dari kolom B di excel
          'nama_tengah'=>$row['D'], // Insert data jenis kelamin dari kolom C di excel
          'nama_keluarga'=>$row['E'], // Insert data alamat dari kolom D di excel
          'nominal'=>$row['F'], 
          'tgl_exp'=>$row['G'], 
          'sts_tu'=>'0',
          'sts_data'=>'1',
          'kdJnstu'=>'1', 
          'kodetu'=>'1',
          'tgl_input'=>date('Y-m-d'),
          'sts_exp'=>'0',
          'u_id'=>NULL,
        ));
      }
      ////
      $numrow++; // Tambah 1 setiap kali looping
    }
    // TOpup History
    $this->TopUpModel->insert_multiple($data);
    
    $this->TopUpModel->update_multiple($data);
    $this->template->set_flashdata('success', 'Upload berhasil');
    redirect("topup/TopUp"); // Redirect ke halaman awal (ke controller siswa fungsi index)
  }
}