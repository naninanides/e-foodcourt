<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Laporantenant extends Admin_Controller {


    private $thedblaporan;
    private $tgl_awal, $tgl_akhir,$statusbayar;
    private $filter_str = '';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/trans_model');
	}

	public function db_laporan()
	{	
        $this->thedblaporan = null;
        if ($this->input->is_ajax_request())
        {
            // $this->datatables->set_database('oracle');
            $this->thedblaporan = $this->datatables;
        }
        else
        {
            // $this->thedb = $this->load->database('oracle', TRUE);
            $this->thedblaporan = $this->db;
        }

        $this->thedblaporan->from('transaksi')
                           ->join('tenant','tenant.id_tenant=transaksi.id_tenant');

	}

    function apply_filter_laporan($p_db)
    {
        $this->statusbayar =$this->input->get_post('statusbayar');
        if(!empty($this->status_bayar))
        {
            $this->filter_str .= ' AND ';
            $this->filter_str .= 'status_bayar = "' . $this->statusbayar . '"';
            $p_db->where('status_bayar = "' . $this->statusbayar . '"');
        }

        $this->tgl_awal = $this->input->get_post('tgl_awal');
        if (!empty($this->tgl_awal))
        {
            if (strlen($this->filter_str) > 0)
                $this->filter_str .= ' AND ';
            $this->filter_str .= 'waktu_transaksi >= "' . $this->tgl_awal . '"';
            $p_db->where('waktu_transaksi >= "' . $this->tgl_awal . '"');
        }

        $this->tgl_akhir = $this->input->get_post('tgl_akhir');
        if (!empty($this->tgl_akhir))
        {
            if (strlen($this->filter_str) > 0)
                $this->filter_str .= ' AND ';
            $this->filter_str .= 'waktu_transaksi <= "' . $this->tgl_akhir . '"';
            $p_db->where('waktu_transaksi <= "' . $this->tgl_akhir . '"');
        }
    }

    function v_laporan()
    {
        $this->template->build('tenant/Laporantenant');
    }

    function dt_laporan(){
        $this->db_laporan();
        $this->apply_filter_laporan($this->thedblaporan);
        $tes=$this->thedblaporan->select("id_transaksi,waktu_transaksi, IF(status_bayar = '0', 'Belum Bayar', 'Sudah Bayar') AS status_bayar, TotalPembayaran, tenant.nama_tenant");
        if ($this->input->is_ajax_request())
            echo $this->thedblaporan->generate();
        else
            return $this->thedblaporan->get()->result_array();
    }

	public function indexMakanan()
	{	
		$this->load->vars('menu', $this->menu_model->getDataMakanan());
		echo $this->load->view('menuListMak.php');
	}

	public function change_status() {
    $id_menu = $this->input->post('id_menu');

    $status=$this->menu_model->update_status($id_menu);
    
    
    echo $status;


    
    // echo "1";
    // exit;
}

    public function edit(){
    $id_transaksi = $_GET["id_transaksi"];
    $this->load->vars('detailtrans', $this->trans_model->getDataDetail($id_transaksi));
    $this->template->build('tenant/editOrder');
    }
    
public function get_data_modal(){
    	$id_menu=$this->input->get('selected_id');
    	$result=$this->menu_model->getDataModal($id_menu);
    	echo json_encode($result);
    }

    public function data_form(){
    	$sel_id_menu=$this->input->post('selected_id_menu');
    	$sel_nama_menu=$this->input->post('sel_nama_menu');
    	$sel_harga_menu=$this->input->post('sel_harga_menu');
    	if(preg_match("/^[0-9,]+$/", $sel_harga_menu)) $sel_harga_menu = str_replace(',', '', $sel_harga_menu);
    	$totalQty=$this->input->post('totalQty');
    	$totalHarga = (int)$sel_harga_menu*(int)$totalQty;

    	$form_data= array($sel_id_menu,$sel_nama_menu,$sel_harga_menu,$totalQty,$totalHarga);

    	echo json_encode($form_data);
    }

    public function data_order(){
    	$array_tbl=$this->input->post('array');
    	$diskon=$this->input->post('diskontotal');
    	$tax=$this->input->post('taxTotal');
    	$pembayaran=$this->input->post('totalHarga');
    	$nomeja=$this->input->post('nomeja');
    	$totalPesanan=$this->input->post('totalPesanan');
    	$result=$this->menu_model->dataOrder($array_tbl,$diskon,$totalPesanan,$pembayaran,$nomeja);
    	
    	
    }


}
