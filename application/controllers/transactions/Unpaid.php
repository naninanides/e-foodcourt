<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/* use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx; */
/**
 * Home controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Unpaid extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
        $this->load->model('administration/trans_model');
        $this->load->model('topup/ReportTopupModel');
	}

	public function index()
	{	
        $karyawans = false;
        $this->load->model('topup/ReportTopupModel');
        $karyawans = $this->ReportTopupModel->mylist();

        $tenants = false;
        
		$this->load->model('administration/Tenant_model');
		$data['tenants'] = $this->Tenant_model->get_list();
		

		$data['noAGP'] = $this->input->get('noAGP');
		$data['tanggal'] = $this->input->get('tanggal');
		

		if(empty($data['tanggal'])){ $data['tanggal']=date('01/m/Y');}
	




		$data['datas'] = $this->trans_model->tx($data['noAGP'], $data['tanggal']);
		//print_r($this->db->last_query()); 
		
		if(!$this->input->get('download')){
		$this->template
			->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
			->set_css('../bower_components/datatables/jquery.dataTables.min.css')
			->set_css('../bower_components/datatables/plugins/buttons.dataTables.min.css')
			->set_js('../bower_components/datatables/media/js/jquery.dataTables.min.js', TRUE)
			->set_js('../bower_components/datatables/dataTables.rowGroup.min.js', TRUE)
			->set_js('../bower_components/datatables/plugins/dataTables.buttons.min.js', TRUE)
			->set_js('../bower_components/datatables/plugins/jszip.min.js', TRUE)
			->set_js('../bower_components/datatables/plugins/buttons.flash.min.js', TRUE)
			->set_js('../bower_components/datatables/plugins/pdfmake.min.js', TRUE)
			->set_js('../bower_components/datatables/plugins/vfs_fonts.js', TRUE)
			->set_js('../bower_components/datatables/plugins/buttons.html5.min.js', TRUE)
			->set_js('../bower_components/datatables/plugins/buttons.print.min.js', TRUE)
			->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
			->set_js_script(' 
			')->build('tx/dailytx', compact('data','karyawans'));
			
		}
    }
    public function updatestatus(){
        $kodeorder = $this->input->get('kodeorder');
        $data1['kode_order'] = $kodeorder;
        $data1['status_bayar']=0;
		$data2['status_bayar'] = 1;
	
        //echo $kodeorder;
        $this->trans_model->updatestatus($data1,$data2, 'detail_transaksi');
        $this->trans_model->updatestatus($data1,$data2, 'transaksi');
        //updatestatus($where,$data,$table)
		redirect('transactions/Dailytx');

    }

	
}
