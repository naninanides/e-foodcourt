<?php
use Jenssegers\Date\Date;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home controller.
 *
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Dashboard extends Admin_Controller 
{
	public function index()
	{
		$this->template->build('dashboard/index');
	}

	public function get_grafik_transaksi()
	{
		$this->load->model('administration/trans_model');

		$start = $this->input->get_post('start');
		$end = $this->input->get_post('end');

		if (!empty($start)) {
			$start = Date::createFromFormat('d/m/Y', $start)->format('Y-m-d');
		} else
			$start = date('Y-m-d');
		if (!empty($end)) {
			$end = Date::createFromFormat('d/m/Y', $end)->format('Y-m-d');
		} else
			$end = date('Y-m-d');

		if (!empty($this->session->userdata('id_tenant'))) {
			$grafik_transaksi = $this->trans_model->grafik_transaksi($start, $end);
		} else {
			$grafik_transaksi = $this->trans_model->grafik_transaksi_tenant($start, $end);
		}
		
		$jumlah_transaksi = $this->trans_model->jumlah_transaksi($start, $end);
		$total_transaksi = $this->trans_model->total_transaksi($start, $end);

		echo json_encode(array(
			'grafik_transaksi' => $grafik_transaksi,
			'jumlah_transaksi' => $jumlah_transaksi,
			'total_transaksi' => $total_transaksi,
			'id_tenant' => $this->session->userdata('id_tenant')
		));
	}

	public function get_grafik_2()
	{
		$this->load->model('administration/trans_model');

		$tahun = $this->input->get_post('tahun');

		if (empty($tahun))
			$tahun = date('Y');
		
		$grafik_2 = $this->trans_model->grafik_2($tahun);

		$result = array();
		Date::setLocale('id');
		for($i = 1; $i <= 12; $i++) {
			$t_date = Date::parse('2000-' . substr('0' . $i, -2) . '-01');
			$result[] = array(
				'bulan' => $t_date->format('F'),
				'jumlah' => $grafik_2->{'jumlah_' . $i},
				'total' => $grafik_2->{'total_' . $i}
			);
		}

		echo json_encode($result);
	}
}
