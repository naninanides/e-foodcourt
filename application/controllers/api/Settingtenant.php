<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User management API.
 *
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Settingtenant extends Admin_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/tenant_model');
		$this->load->model('auth/user_model');
	}

	public function index()
	{
		echo $this->tenant_model->datatable();
	}

}
