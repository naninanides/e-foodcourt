<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User management API.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata	
 */
class Settingmenu extends Admin_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/menu_model');
		$this->load->model('auth/user_model');
	}

	public function index() 
	{
		echo $this->menu_model->datatable();
		
	}

}