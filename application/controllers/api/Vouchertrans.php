<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Home controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class Vouchertrans extends Admin_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('administration/Voucher_model');
        $this->load->model('auth/user_model');
    }
    function _remap($param) {
        $this->index($param);
    }
    public function index($param)
	{
        
		echo $this->Voucher_model->datatabletrans($param);
	}
    

	
}