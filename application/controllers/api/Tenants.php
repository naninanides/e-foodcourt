<?php


defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * mobile API.
 *
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
 require_once 'application/libraries/REST_Controller.php';
 use Restserver\Libraries\REST_Controller;

class Tenants extends REST_Controller
{
     function __construct($config = 'rest')
     {
         parent::__construct($config);
         $this->load->database();
         $this->load->model('api_model');
     }

     public function index_get()
     {
        $id_tenant = $this->uri->segment(3);
        $tenants = $this->api_model->get_tenant($id_tenant);
        if ($id_tenant == NULL)
        {
          foreach($tenants as $i => $row)
          {
            $tenants[$i]['menus'] = $this->api_model->get_menus($row['id_tenant']);
          }
          $this->response($tenants,200);
        }
        else
        {
          $tenant = array();

          foreach ($tenants as $row) {
            $result = new stdClass();
            $result->id_tenant = $row['id_tenant'];
            $result->nama_tenant = $row['nama_tenant'];
            $result->desk_tenant = $row['desk_tenant'];
            $result->foto_tenant = $row['foto_tenant'];
            $result->menus = $this->api_model->get_menus($id_tenant);
            $tenant[0] = $result;
          }
          $tenant = array_values($tenant);
          $this->response($tenant,200);
        }
     }

     public function menus_get()
     {
        $id_menu = $this->uri->segment(3);
     }

     public function notification_get($id_tenant)
     {
        //$id_tenant = $this->uri->segment(4);
		$notif = $this->api_model->get_notification($id_tenant);
		$msg = array();
		/* foreach ($notif as $obj) {
			$msg[] = $obj["message"];
      } */
      //print_r($this->db->last_query());    
		   //$this->api_model->delete_notification($id_tenant);
        $this->response($notif, 200);
     }
     

     public function order_get()
     {
        $id_tenant = $this->uri->segment(4);
		$orders = $this->api_model->get_pesanan($id_tenant);
        $this->response($orders, 200);
     }

     public function order_post()
     {
        $id_tenant = $this->post('id_tenant');
        $tgl = $this->post('tgl');
		$orders = $this->api_model->get_pesanan_date($id_tenant, $tgl);
		
        $this->response($orders, 200);
     }


     public function orderstatus_post()
     {
         $id_tenant = $this->post('id_tenant');
         $tgl = $this->post('tgl');
         $StatusBayar = $this->post('StatusBayar');
         $StatusAntar = $this->post('StatusAntar');
         $KodeOrder = $this->post('KodeOrder');
         $NoPesanan = $this->post('NoPesanan');
        
         //echo $StatusAntar;
        
         $orders = $this->api_model->get_pesananstatus($id_tenant,$tgl,$StatusBayar,$StatusAntar,$KodeOrder,$NoPesanan);

         //print_r($this->db->last_query());
         $this->response($orders, 200);

        
     }

     public function ceknopesanan_get()
     {
      $id_tenant = $this->uri->segment(4);
        
         //echo $StatusAntar;
         $PesananAvailable = $this->api_model->fn_ceknopesanan($id_tenant);
        $this->response($PesananAvailable, 200);
     }

     public function search_history_post()
     {
        //echo $this->post('id_tenant');
        $id_tenant = $this->post('id_tenant');
        $tanggal_start = $this->post('tanggal_start');
        $tanggal_end = $this->post('tanggal_end');

        $history = $this->api_model->search_history($id_tenant,$tanggal_start,$tanggal_end);        
        $this->response($history, 200);
        
     }


}

?>
