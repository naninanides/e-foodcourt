<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* This can be removed if you use __autoload()
 in config.php OR use Modular Extensions
 @noinspection PhpIncludeInspection */

require_once 'application/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
use Hashids\Hashids;

class Order extends REST_Controller
{

    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
        $this->load->model('api_model');

    }

    public function transaction_post()
    {
        $u_id = new Hashids('',7,'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
        $id_tenant = $this->post('id_tenant');
        /* $id_tenant = $this->db->select('id_tenant')->from('tenant')
                      ->get()->row()->id_tenant; */
        $id_transaksi = $this->db->select_max('id_transaksi')->from('transaksi')->get()->row()->id_transaksi;
        $index = $id_transaksi + 1;

        $data = array(
                    'id_transaksi'      => $index,
                    'u_id_transaksi'    => $u_id->encode($index,$id_tenant),
                    'nama_cust'         => $this->post('nama_cust'),
                    'notel_cust'        => $this->post('notel_cust'),
                    'status_bayar'      => 0,
                    'id_tenant'         => $id_tenant
        );
        $insert = $this->api_model->post_transaction($data);
        $response = $this->api_model->get_transaction_response($index);
        if ($insert)
            $this->response($response, 200);
        else
            $this->response(array('code' => 502,
                                  'status' => 'fail'),502);
    }
    public function order_post(){
        ini_set('display_errors', 0);
        $item = $this->post("item");
        $id_menu = $this->post("id_menu");
		$jumlah_pesanan = $this->post("jumlah_pesanan");
		$id_tenant = $this->post('id_tenant');
        $cat_pesan = $this->post('cat_pesan');
        
        $kode_pesanan = $this->api_model->generate_code($id_tenant);
        $hash_id = $kode_pesanan;

        //Header Transaction

        $id_transaksi = $this->db->select_max('id_transaksi')->from('transaksi')->get()->row()->id_transaksi;
        $index = $id_transaksi + 1;

        $data = array(
                    'kode_order'        => $kode_pesanan,
                    'id_transaksi'      => $index,
                    'u_id_transaksi'    => $kode_pesanan,
                    'nama_cust'         => $this->post('nama_cust'),
                    'notel_cust'        => $this->post('notel_cust'),
                    'status_bayar'      => 0,
                    'id_tenant'         => $id_tenant,
                    'no_meja'           => $this->post('no_meja')
        );
        $insert = $this->api_model->post_transaction($data);
        $response = $this->api_model->get_transaction_response($index);
        if ($insert){
            //$this->response($response, 200);
            
            // Detail Transaction
            while (list($key, $val) = each($id_menu)) {
                
                $data = array(
                    //'notel_cust'			=> $this->post('notel_cust'),
                    'kode_order'		=> $kode_pesanan,
                    'nama_cust'			=> $this->post('nama_cust'),
                    'no_agp'			=> $this->post('no_agp'),
                    'no_kursi'			=> $this->post('no_meja'),
                    'cat_pesan'			=> $val['cat_pesan'],
                    'kode_tenant'		=> $id_tenant,
                    'u_id_transaksi'    => $hash_id,
                    'id_menu'           => $val['id_menu'],
                    'jumlah_pesanan'    => $val['jumlah_pesanan'],
                );
                //echo $val['id_menu'];
            //print_r($data);
            $insert = $this->api_model->post_detail_transaction($data);
            
            }
            if ($insert){
                //$id_transaksi=$this->api_model->get_transaction_id($kode_pesanan);
                $this->response(array('id_transaksi'=>$index,'kode_pesanan' => $kode_pesanan,'status'=>'Order berhasil disimpan'), 200);
                //echo "test";
            }
            else{
                $this->response($insert, 200);  
        }
    }
        else {
            $this->response(array('code' => 502,
                                  'status' => 'fail'),502);
        }

    }
   

    public function payment_post()
    {
        $id_transaksi = $this->post('u_id_transaksi');
        $data_transaksi = $this->api_model->get_detail_transaction($id_transaksi);
        $biaya_pesanan = 0;
        $discount = 0;
        $total_pembayaran = 0;
        foreach ($data_transaksi as $row)
            $biaya_pesanan += ($row['harga_menu']*$row['jumlah_pesanan']);
        $discount = ($biaya_pesanan * 10/100);
        $total_pembayaran = $biaya_pesanan - $discount;
        $data = array(
                    'biaya_pesanan'     => $biaya_pesanan,
                    'discount'          => $discount,
                    'total_pembayaran'  => $total_pembayaran
        );
        $update = $this->api_model->post_payment($data,$id_transaksi);
        if ($update)
            $this->response(array('code' => 200,
                                  'status' => 'success',
                                  'message' => $data), 200);
        else
            $this->response(array('code' => 502,
                                  'status' => 'fail'),502);
    }

     public function delete_get($id_order)
     {
        $trans_id=$this->api_model->get_transaction_id($id_order);
        $orders = $this->api_model->delete_pesanan($id_order);
        
        


        $this->response(array('code' => 200,
                                  'status' => 'success',
                                  'message' => ""), 200);
     }

     public function delete_tenant_order_post()
     {
        //echo $this->post('id_tenant');
        $orders = $this->api_model->delete_pesanan_tenant($this->post('id_tenant'),$this->post('no_pesanan'));        

        $this->response(array('code' => 200,
                                'IdTenant' => $this->post('id_tenant'),
                                  'status' => 'success',
                                  'message' => ""), 200);
     }

     public function delivery_get($id_item)
     {
        $orders = $this->api_model->delivery_pesanan($id_item);
        //print_r($this->db->last_query());    
        $this->response(array('code' => 200,
                                  'status' => 'success',
                                  'message' => ""), 200);  
     }




}
?>
