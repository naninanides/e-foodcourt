<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User management API.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Menu extends Admin_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/menu_model');
	}

	public function index() 
	{
		echo $this->menu_model->getData();
		$this->load->vars('menu', $this->menu_model->getData());
	}

}