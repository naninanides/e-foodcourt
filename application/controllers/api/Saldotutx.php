<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Home controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class Saldotutx extends Admin_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('administration/Saldotutx_model');
        $this->load->model('auth/user_model');
    }
    
    public function index()
	{
		echo $this->Saldotutx_model->datatable();
	}
    
    public function saldokartu()
	{
		echo $this->Saldotutx_model->dt_saldokartu();
	}
	
}