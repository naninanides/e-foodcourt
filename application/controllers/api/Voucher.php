<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Home controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class Voucher extends Admin_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('administration/Voucher_model');
        $this->load->model('auth/user_model');
    }
    
    public function index()
	{
		echo $this->Voucher_model->datatable();
	}
    

	
}