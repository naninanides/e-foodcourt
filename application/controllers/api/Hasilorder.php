<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User management API.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata	
 */
class HasilOrder extends Api_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/trans_model');
		$this->load->model('auth/user_model');
	}

	public function index() 
	{
		$status_bayar = $this->input->post('status_bayar');
		$id_tenant = $this->input->post('id_tenant');
		$tanggal_start = $this->input->post('tanggal_start');
		$tanggal_end = $this->input->post('tanggal_end');

		echo $this->trans_model->datatable($status_bayar, $id_tenant, $tanggal_start, $tanggal_end);
		
	}
	
}