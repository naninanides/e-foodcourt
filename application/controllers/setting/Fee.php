<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Fee controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Fee extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/fee_model');
	}

	public function index()
	{	
		$this->template
				->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
				->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
				->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
				->build('setting/fee/index');
    }
    
    public function datatable()
    {
        echo $this->fee_model->datatable();
    }

    public function add()
    {
        $this->_save();
    }

    public function edit($id)
    {
        $this->_save($id);
    }

    private function _save($id = 0)
    {
        $this->load->library('form_validation');
        $form = $this->fee_model->form;
        
        // set default values for update
        if ($id > 0) {
            $fee = $this->fee_model->get_by_id($id);
			$this->form_validation->set_default($fee);
		}
		
        $this->form_validation->init($form);
        
        if ($this->form_validation->run()) {
            $values = $this->form_validation->get_values();

            if ($id > 0) {
                $this->fee_model->update($id, $values);
                $this->template->set_flashdata('success', 'Data Fee telah berhasil di-update.');
            } else {
                $id = $this->fee_model->insert($values);
                $this->template->set_flashdata('success', 'Data Fee telah berhasil ditambah.');
            }
            redirect('setting/fee');
        }

        $this->template->build('setting/fee/form', array('id' => $id, 'form' => $this->form_validation));
    }

    function view($id)
    {
        $fee = $this->fee_model->get_by_id($id);
		if ($fee) {
            $this->template->build('setting/fee/view', array('fee' => $fee));
        } else
            show_404();
    }
    
    function delete($id) 
    {
		$fee = $this->fee_model->get_by_id($id);
		if ($fee) {
			$this->fee_model->delete($id);
            $this->template->set_flashdata('info', 'Data Fee telah berhasil dihapus.');
            redirect('setting/fee');
        } else
            show_404();
    }
}
