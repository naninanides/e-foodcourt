<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Home controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Settingmenu extends Admin_Controller 
{
	protected $menu_form = array(
		'id_menu' => array(
			'helper' => 'form_hidden'
		),
		'id_tenant' => array(
			'helper' => 'form_hidden'
		),
		'nama_menu' => array(
			'label'	=> 'lang:nama_menu',
			'rules' => 'trim|max_length[50]',
			'helper' => 'form_inputlabel'
		),
		'desk_menu' => array(
			'label'	=> 'lang:desk_menu',
			'rules' => 'trim|max_length[255]',
			'helper' => 'form_inputlabel'
		),
		'status_menu' => array(
			'label' => 'lang:status_menu',
			'rules' => 'trim|max_length[255]',
			'helper' => 'form_inputlabel'	
		),
		'harga_menu' => array(
			'label' => 'lang:harga_menu',
			'rules' => 'trim|max_length[255]',
			'helper' => 'form_inputlabel'
		),
		'foto_menu' => array(
			'label' => 'lang:foto_menu',
			'rules' => 'trim|max_length[255]',
			'helper' => 'form_inputlabel',
		)
	);

	public function __construct()
	{
		parent::__construct();
		$this->load->model('administration/menu_model');
	}

	public function index()
	{	
		$tenants = false;
		$this->load->model('administration/Tenant_model');
		$tenants = $this->Tenant_model->get_list();
		$this->template
				->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
				->set_css('../bower_components/datatables/media/css/jquery.dataTables.min.css')
				->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
				->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
				->set_js_script(' 
				')
				->build('setting/Settingmenu', array('tenants' => $tenants));
	}

	function get_data_tenant(){
			$data_user=$this->menu_model->get_data_tenant();
			echo json_encode($data_user);
	}

	function get_data_category(){
		$data_category=$this->menu_model->get_data_category();
		echo json_encode($data_category);
	}

	function hapus_data($id){
		$this->load->model('administration/trans_model');
		$detail_trans = $this->trans_model->get_detail_by_id_menu($id_menu);
		if($detail_trans){
			$where = array('id_menu' => $id);
			$this->menu_model->hapus_data($where,'menu');
		}else{
			$this->template->set_flashdata('error', 'Menu tidak dapat dihapus!');
		}
		redirect('setting/Settingmenu');
	}

	function tambah()
	{
		if ($_FILES['foto_menu']["name"] != "") {
			$output_file = "images/";
			$fileName = $output_file . time() . $_FILES["foto_menu"]["name"];
			move_uploaded_file($_FILES["foto_menu"]["tmp_name"], $fileName);
		} else {
			$fileName = "";
			// throw new Exception(msg('msg_required', 'lbl_file_pendukung'), ERROR_CODE_DEFAULT);
		}

		$data = $this->input->post();

		$id_tenant = $this->session->userdata('id_tenant');
		if (empty($id_tenant) && isset($data['id_tenant']))
			$id_tenant = $data['id_tenant'];

		$data['id_menu'] = $this->menu_model->get_new_id($id_tenant);
		$data['id_tenant'] = $id_tenant;
		$data['foto_menu'] = $fileName;
		$data['u_id'] = $this->session->userdata('id');
		$data['created_by'] = $this->session->userdata('id');
		$data['created'] = date('Y-m-d H:i:s');

		$this->menu_model->insert($data);
		redirect('setting/Settingmenu');
	}

	function ubah($id)
	{
		$menu = $this->menu_model->edit_data(array('id_menu' => $id), 'menu')->row();

		$tenants = $this->menu_model->get_data_tenant();
		$kategori= $this->menu_model->get_data_category();
		$this->load->view('setting/Menuedit', array('menu' => $menu, 'tenants' => $tenants, 'kategori' => $kategori)); 
	}

	function update()
	{
		$data = $this->input->post();

		if ($_FILES['foto_menu']["name"] != "") {
			$output_file = "images/";
			$fileName = $output_file . time() . $_FILES["foto_menu"]["name"];
			move_uploaded_file($_FILES["foto_menu"]["tmp_name"], $fileName);
			$data['foto_menu'] = $fileName;
		} else {
			$fileName = "";
			// throw new Exception(msg('msg_required', 'lbl_file_pendukung'), ERROR_CODE_DEFAULT);
		}

		$id_tenant = $this->session->userdata('id_tenant');
		if (empty($id_tenant) && isset($data['id_tenant']))
			$id_tenant = $data['id_tenant'];

		$data['id_tenant'] = $id_tenant;
		$data['u_id'] = $this->session->userdata('id');
		$data['updated_by'] = $this->session->userdata('id');
		$data['updated'] = date('Y-m-d H:i:s');

		$this->menu_model->update($data['id_menu'], $data);
		redirect('setting/Settingmenu');
	}

}
