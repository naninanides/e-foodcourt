<?php

use Jenssegers\Date\Date;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Dashboarrd controller.
 * 
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Home extends Admin_Controller {

	public function index()
	{		
		$this->template
				->build('dashboard/index');

	}
}
