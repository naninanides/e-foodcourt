<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;
/**
 * Home controller.
 *
 * @package App
 * @category Controller
 * @author Ardi Soebrata
 */
class Tenant extends Admin_Controller {

	public function index()
	{
			$client = new GuzzleHttp\Client();
			$tenant = '';
			$tenant_json = $client->request('GET', 'http://115.85.80.15/e-foodcourt/api/dashboard/tenant?id_tenant='.$tenant);
			$data['dashboard'] = json_decode($tenant_json->getBody());
			$this->template->build('dashboard/prototype', $data);
	}
}
