<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Home controller.
*
* @package App
* @category Controller
* @author Ardi Soebrata
*/
class Saldotutx extends Admin_Controller 
{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('administration/Saldotutx_model');
        $this->load->model('auth/User_model');
    }
    
    public function index()
    {
        $startdate="2019-08-01";
        $data['enddate'] = $this->input->get('enddate');
        
        if(empty($data['enddate'])){ $data['enddate']=date('d/m/Y');}
        
        list($start_d,$start_m,$start_y) = explode("/",$data['enddate']); 
        $enddate=$start_y.'-'.$start_m.'-'.$start_d;

        $this->Saldotutx_model->regenerateRekap($startdate,$enddate);
        //print_r($this->db->last_query());
        $this->template
        ->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
			->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
			->set_js('../bower_components/datatables/media/js/dataTables.buttons.min', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.flash.min', TRUE)
			->set_js('../bower_components/datatables/media/js/jszip.min', TRUE)
			->set_js('../bower_components/datatables/media/js/pdfmake.min', TRUE)
			->set_js('../bower_components/datatables/media/js/vfs_fonts', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.html5.min', TRUE)
			->set_js('../bower_components/datatables/media/js/buttons.print.min', TRUE)
			->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
        ->set_js_script('
        ')
        ->build('saldotutx/view',$data);
    }
    public function saldokartu()
    {
        $this->template
        ->set_css('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap')
        ->set_js('../bower_components/datatables/media/js/jquery.dataTables.min', TRUE)
        ->set_js('../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min')
        ->set_js_script('
        ')
        ->build('saldotutx/saldokartu');
    }

    
	
    
	
    
}