<div class="app app-header-fixed ">
	<div class="wrapper-md">
		<div class="col-md-offset-3 col-md-6 panel panel-default" style="box-shadow: 2px 2px 3px rgba(150, 150, 150, 0.6)">
			<div class="container w-xxl w-auto-xs" ng-controller="SigninFormController" ng-init="app.settings.container = false;">
				<span class="navbar-brand block m-t" style="font-size: 4em; color: #7f7f7f; text-shadow: 2px 2px 3px rgba(150, 150, 150, 0.6)">Food Court 	</span>
				<div class="m-b-lg">
					<form method="post" name="form" class="form-validation" >
						<div class="text-danger wrapper text-center" ng-show="authError"></div>
						<?php echo messages() ?>
						<div class="list-group list-group-sm" style="box-shadow: 2px 2px 3px rgba(150, 150, 150, 0.6)">
							<div class="list-group-item">
								<input type="text" name="username" placeholder="<?php echo lang('username'); ?>" class="form-control no-border" required autofocus value="<?php echo (isset($username)) ? $username : ''; ?>">
							</div>
							<div class="list-group-item">
								<input type="password" name="password" placeholder="Password" class="form-control no-border" required>
							</div>
						</div>
						<button type="submit" class="btn btn-lg btn-dark btn-block" style="box-shadow: 2px 2px 3px rgba(150, 150, 150, 0.6)">Log in</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
