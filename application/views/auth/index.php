<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">
		<a href="<?php echo site_url('auth/user/add') ?>" class="btn btn-primary pull-right">
			<i class="fa fa-plus"></i> <?php echo lang('add') ?>
		</a>
		<i class="fa fa-user"></i> <?php echo lang('users') ?>
	</h1>
</div>
<div class="wrapper-md">
	<?php echo messages() ?>
	<div class="panel panel-default">
		<div class="table-responsive">
			<table id="dataTable_user" ui-jq="dataTable" ui-options="{
			   ajax: '<?php echo site_url('api/user/index') ?>',
			   columns: [
				   { data: 'full_name' },
				   { data: 'username', render: render_username },
				   { data: 'email'},
				   { data: 'role' },
				   { data: 'tenant' }, 
				   { data: 'action', render: render_action, orderable: false }
			   ],
			   order: [[0, 'desc']]
			   }" class="table table-striped b-t b-b b-light">
				<thead>
					<tr>
						<th>Nama Lengkap</th>
						<th><?php echo lang('username'); ?></th>
						<th><?php echo lang('email'); ?></th>
						<th>Role</th>
						<th>Tenant</th>
						<th style="width: 15px;"></th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
	
<?php $this->load->view('delete-modal'); ?>

<script>
function render_username(data, type, row, meta) {
	return '<a href="<?php echo site_url('auth/user/edit/') ?>/' + row.id + '">' + data + '</a>';
}

function render_action(data, type, row, meta) {
	return '<a href="<?php echo site_url('auth/user/delete/') ?>/' + row.id + '" title="Delete" data-button="delete"><i class="fa fa-trash"></i></a>';
}
</script>