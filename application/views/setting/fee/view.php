<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">
		<i class="fa fa-money"></i> Fee
	</h1>
</div>
<div class="wrapper-md">
	<?php echo messages() ?>
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Fee</h3>
                </div>
                <div class="panel-body form-horizontal">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama</label>
                        <div class="col-md-8">
                            <p class="form-control-static"><?php echo $fee->nama ?></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Deskripsi</label>
                        <div class="col-md-8">
                            <p class="form-control-static"><?php echo $fee->deskripsi ?></p>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <a href="<?php echo site_url('setting/fee/edit/' . $fee->id) ?>" class="btn btn-primary">
                        <i class="fa fa-pencil"></i> Edit
                    </a>
                    <a href="<?php echo site_url('setting/fee') ?>" class="btn btn-default">
                        <i class="fa fa-repeat"></i> Back
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3 class="font-thin m-b-md">
                Fee Details
                <a href="<?php echo site_url('setting/fee_detail/add/' . $fee->id) ?>" class="btn btn-primary pull-right">
                    <i class="fa fa-plus"></i> Tambah
                </a>
            </h3>
            <div class="panel panel-default">
                <div class="table-responsive">
                    <table id="tbl2" ui-jq="dataTable" ui-options="{
                        ajax: '<?php echo site_url('setting/fee_detail/datatable/' . $fee->id) ?>',
                        columns: [			
                            { data: 'jenis', 'render': render_jenis },					   
                            { data: 'nama_kategori' }, 
                            { data: 'nama_fee_account' },
                            { data: 'jumlah', class: 'text-right' },
                            { data: 'satuan', class: 'text-right' }, 
                            { data: 'action', render: render_action, orderable: false }
                        ],
                        order: [[0, 'asc']]
                        }" class="table table-striped b-t b-b">
                            <thead>
                                <tr>
                                    <th>Jenis Fee</th>
                                    <th>Kategori</th>
                                    <th>Account</th>
                                    <th>Jumlah</th>
                                    <th style="width: 15px;">Satuan</th>
                                    <th style="width: 15px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('delete-modal'); ?>
<script>
	function render_jenis(data, type, row, meta) {

    	return '<a href="<?php echo site_url('setting/fee_detail/edit') ?>/' + row.id + '" title="Edit" ">' + toTitleCase(data) + '</a>';
	}

	function render_action(data, type, row, meta) {
		return '<a href="<?php echo site_url('setting/fee_detail/delete') ?>/' + row.id + '" title="Delete" data-button="delete"><i class="fa fa-trash"></i></a>';
	}

    function toTitleCase(str) {
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }
</script>