<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3"><i class="fa fa-money"></i> Fee Account</h1>
</div>
<div class="wrapper-md">
	<form class="form-horizontal box" method="post">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php echo $form->fields(); ?>
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a href="<?php echo site_url('setting/fee_account') ?>" class="btn btn-default"><i class="fa fa-repeat"></i> Batal</a>
			</div>
		</div>
	</form>
</div>