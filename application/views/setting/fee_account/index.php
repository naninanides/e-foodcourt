<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">
        <a href="<?php echo site_url('setting/fee') ?>" class="btn btn-default pull-right m-l-sm">
			<i class="fa fa-repeat"></i> Fees
		</a>
		<a href="<?php echo site_url('setting/fee_account/add') ?>" class="btn btn-primary pull-right">
			<i class="fa fa-plus"></i> Tambah
		</a>
		<i class="fa fa-money"></i> Fee Account
	</h1>
</div>
<div class="wrapper-md">
	<?php echo messages() ?>
	<div class="panel panel-default">
		<div class="table-responsive">
            <table id="tbl2" ui-jq="dataTable" ui-options="{
                ajax: '<?php echo site_url('setting/fee_account/datatable') ?>',
                columns: [			
                    { data: 'nama', 'render': render_nama },					   
                    { data: 'deskripsi' }, 
				    { data: 'action', render: render_action, orderable: false }
                ],
                order: [[0, 'asc']]
                }" class="table table-striped b-t b-b">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Dekripsi</th>
						    <th style="width: 15px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->load->view('delete-modal'); ?>
<script>
	function render_nama(data, type, row, meta) {
    	return '<a href="<?php echo site_url('setting/fee_account/edit') ?>/' + row.id + '" title="Edit" ">' + data + '</a>';
	}

	function render_action(data, type, row, meta) {
		return '<a href="<?php echo site_url('setting/fee_account/delete') ?>/' + row.id + '" title="Delete" data-button="delete"><i class="fa fa-trash"></i></a>';
	}	
</script>