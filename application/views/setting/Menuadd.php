<div id="menuAdd" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-left">Tambah Menu</h4>
      </div>
      <form class="form-horizontal" role="form" action="<?php echo site_url('setting/Settingmenu/tambah') ?>" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          <?php if (empty($auth_user['id_tenant'])): ?>
          <div class="form-group">
            <label class="col-sm-3 control-label">Tenant</label>
            <div class="col-sm-9">
              <select  name="id_tenant" id="id_tenant" class="form-control" style="width: 100%;">
                <option value="" selected>Pilih Tenant</option>
              </select>
            </div>
          </div>
          <?php endif; ?>
          <div class="form-group">
            <label class="col-sm-3 control-label">Foto</label>
            <div class="col-sm-9">
              <input id="foto_menu" class="form-control" type="file" placeholder="" name="foto_menu">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-9">
              <input id="nama_menu" class="form-control" type="text" placeholder="" name="nama_menu">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Deskipsi</label>
            <div class="col-sm-9">
              <input id="desk_menu" class="form-control" type="text" placeholder="" name="desk_menu">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Status</label>
            <div class="col-sm-9">
              <label class="checkbox-inline i-checks">
                <input type="radio" name="status_menu" value="1" checked>
                <i></i>
                Tersedia
              </label>
              <label class="checkbox-inline i-checks">
                <input type="radio" name="status_menu" value="0">
                <i></i>
                Tidak Tersedia
              </label>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Kategori Menu</label>
            <div class="col-sm-9">
              <select  name="kategori" id="kategori" class="form-control" style="width: 100%">
                <option value="" selected>(Pilih Kategori Menu)</option> 
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Harga Menu</label>
            <div class="col-sm-9">
              <input id="harga_menu" class="form-control text-right" placeholder="" name="harga_menu" type="number" value="1000" min="0" step="500" data-number-to-fixed="2" data-number-stepfactor="100">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Diskon Menu</label>
            <div class="col-sm-9">
              <input id="discount" class="form-control text-right" placeholder="" name="discount" type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="1">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Simpan</button>
          <a href="#" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-repeat"></i> Batal </a>
        </div>
      </form>
    </div>
  </div>
</div>