<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">
        <a href data-toggle="modal" class="btn btn-primary open-Modal pull-right"><i class="fa fa-plus"></i> Tambah</a>
        <i class="fa fa-cutlery"></i> Menu
    </h1>
</div>
<div class="col">
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="row">
			<div class="col-lg-12">			
				<div class="panel panel-default">
                    <div id="table-filter" class="panel-body form-inline">
                        <label class="control-label m-r-xs m-l-md">Tenant</label>
                        <select id="filter-tenant" name="filter-tenant" class="form-control" style="width: 200px">
                            <?php if ($this->session->userdata('id_tenant')){ ?>
                            <?php foreach($tenants as $tenant):
                                if ($tenant->id_tenant==$this->session->userdata('id_tenant')){ ?>
                                    <option value="<?php echo $tenant->id_tenant; ?>" selected><?php echo $tenant->nama_tenant ?></option>
                            <?php } ?>
                            <?php endforeach; ?>
                        <?php }
                        else { ?>
                            <option value="">(Semua Tenant)</option>
                            <?php foreach($tenants as $tenant):?>
                                <option value="<?php echo $tenant->id_tenant; ?>"><?php echo $tenant->nama_tenant ?></option>
                            <?php endforeach;
                        } ?>
                        </select>
                        
                    </div>
					<div class="table-responsive">
						<?php echo messages() ?>
						<table id="dataTable_menus" class="table table-striped b-t b-b b-light" ui-jq="dataTable" ui-options="{
							ajax: '<?php echo site_url('api/Settingmenu/index') ?>',
							columns: [			
									{ data: 'foto_menu', render: render_foto, orderable: false },
                                    <?php if (empty($auth_user['id_tenant'])): ?>{ data: 'nama_tenant' },<?php endif; ?>			   
									{ data: 'id_menu' },
									{ data: 'nama_menu', render: render_nama_menu },					   
									{ data: 'desk_menu' },
									{ data: 'status_menu' },					   
									{ data: 'harga_menu', render: render_number, class: 'text-right' },
									{ data: 'action_delete', render: render_action, orderable: false, visible: false }
							],
							order: [[1, 'asc']]
							}">
                            <thead>
                                <tr>
                                    <th>Foto</th>
                                    <?php if (empty($auth_user['id_tenant'])): ?><th style="min-width: 100px">Tenant</th><?php endif; ?>
                                    <th>Id</th>
                                    <th style="min-width: 200px">Nama</th>
                                    <th>Deskripsi</th>
                                    <th style="width: 100px">Status</th>
                                    <th style="width: 100px">Harga</th>
                                    <th style="width: 15px;">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
						</table>
					</div>
					<?php include('Menuadd.php'); ?>

                    <div id="menuEdit" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-left">Edit Menu</h4>
                                </div>
                                <form class="form-horizontal" role="form" action="<?php echo site_url('setting/Settingmenu/update') ?>" method="POST" enctype="multipart/form-data">
                                    <div class="modal-body">
                                        Loading...
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Simpan</button>
                                        <a href="#" type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-repeat"></i> Batal </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>	
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('delete-modal'); ?>

<script>
    $('#filter-tenant').on('change', function(){
        $('#dataTable_menus').dataTable().fnFilter($("#filter-tenant option:selected").text());
    });
    
    function render_foto(data, type, row, meta) {
        if (data)
            return '<img src="<?php echo base_url() ?>' + data + '" style="max-width: 100px; max-height: 100px;" />';
        else
            return data;
    }

    function render_nama_menu(data, type, row, meta) {
        return '<a class="edit-menu" href="#" data-id="' + row.id_menu + '" title="Edit">' + data + '</a>';
    }

    function render_date(data, type, row, meta) {
        if (type === 'display')
            return moment(data).format('DD/MM/YYYY');
        return data;
    }
    function render_number(data, type, row, meta) {
        if (type === 'display')
            return numeral(data).format('0,0');
        return data;
    }

	function render_action(data, type, row, meta) {
		return '<a href="<?php echo site_url('setting/Settingmenu/hapus_data') ?>/' + row.id_menu + '" title="Delete" data-button="delete"><i class="fa fa-trash"></i></a>';
	}

	$('.open-Modal').click(function(e) {
        select = document.getElementById('id_tenant');
        e.preventDefault();
		$.ajax({
            type:'ajax',
            method:'get',
            url:'<?php echo site_url('setting/settingmenu/get_data_tenant') ?>',
            dataType:'json',
            success:function(data) {
                for (index = 0; index < data.length; ++index) {
                    var opt = document.createElement('option');
                    opt.value = data[index]['id_tenant'];
                    opt.innerHTML = data[index]['nama_tenant'];
                    select.appendChild(opt);
                }
		    }
        });


        select2 = document.getElementById('kategori');
        e.preventDefault();
		$.ajax({
            type:'ajax',
            method:'get',
            url:'<?php echo site_url('setting/settingmenu/get_data_category') ?>',
            dataType:'json',
            success:function(data) {
                for (index = 0; index < data.length; ++index) {
                    var opt = document.createElement('option');
                    opt.value = data[index]['id'];
                    opt.innerHTML = data[index]['nama'];
                    select2.appendChild(opt);
                }
		    }
        });

        $('#menuAdd').modal('show');
    });

  


    <?php if (empty($auth_user['id_tenant'])): ?>
    $('#dataTable_menus').on('click', '.edit-menu', function(e) {
        e.preventDefault();
        var menu_id = $(this).data('id');
        $('#menuEdit .modal-body').html('Loading...');
        $('#menuEdit').modal('show');
        $('#menuEdit .modal-body').load('<?php echo site_url('setting/Settingmenu/ubah/') ?>' + menu_id, function() {
            $('select:not(.manual-select2)').select2({
                minimumResultsForSearch: 20
            });
        });
    });
    <?php endif; ?>
</script>