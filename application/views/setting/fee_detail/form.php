<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3"><i class="fa fa-money"></i> Fee Detail</h1>
</div>
<div class="wrapper-md">
	<form class="form-horizontal box" method="post">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php echo $form->fields(); ?>
			</div>
			<div class="panel-footer">
				<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
				<a href="<?php echo site_url('setting/fee/view/' . $fee_id) ?>" class="btn btn-default"><i class="fa fa-repeat"></i> Batal</a>
			</div>
		</div>
	</form>
</div>

<script>
$(document).ready(function() {
	$('[name=jenis]').on('change', function() {
		if (!$('#jenis_kategori').is(':checked')) {
			$('#kategori_id').parents('.form-group').slideUp('slow');
			$('#kategori_id').val('');
		} else {
			$('#kategori_id').parents('.form-group').slideDown('slow');
		}
	});
});
</script>