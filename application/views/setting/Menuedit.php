<?php if (empty($auth_user['id_tenant'])): ?>
<div class="form-group">
  <label class="col-sm-3 control-label">Tenant</label>
  <div class="col-sm-9">
    <select  name="id_tenant" id="id_tenant" class="form-control" style="width: 100%;">
      <option value="" selected>Pilih Tenant</option>
      <?php foreach($tenants as $row) {
        if($row->id_tenant == $menu->id_tenant){ ?>
          <option value="<?php echo $row->id_tenant ?>" selected> <?php echo $row->nama_tenant ?> </option>
        <?php }
        else{ ?>
          <option value="<?php echo $row->id_tenant ?>"> <?php echo $row->nama_tenant ?> </option>
        <?php }
        } ?>
    </select>
  </div>
</div>
<?php endif; ?>
<input type="hidden" id="id_menu" name="id_menu" value="<?php echo $menu->id_menu ?>" />
<div class="form-group">
  <label class="col-sm-3 control-label">Foto</label>
  <div class="col-sm-9">
    <input id="foto_menu" class="form-control" type="file" placeholder="" name="foto_menu">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Nama</label>
  <div class="col-sm-9">
    <input id="nama_menu" class="form-control" type="text" placeholder="" name="nama_menu" value="<?php echo $menu->nama_menu ?>">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Deskipsi</label>
  <div class="col-sm-9">
    <input id="desk_menu" class="form-control" type="text" placeholder="" name="desk_menu" value="<?php echo $menu->desk_menu ?>">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Status</label>
  <div class="col-sm-9">
    <label class="checkbox-inline i-checks">
      <input type="radio" name="status_menu" value="1" <?php echo ($menu->status_menu == 1 ? 'checked' : '') ?>>
      <i></i>
      Tersedia
    </label>
    <label class="checkbox-inline i-checks">
      <input type="radio" name="status_menu" value="0" <?php echo ($menu->status_menu != 1 ? 'checked' : '') ?>>
      <i></i>
      Tidak Tersedia
    </label>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Kategori Menu</label>
  <div class="col-sm-9">
    <select  name="kategori" id="kategori" class="form-control" style="width: 100%">
      <option value="" selected>Pilih Tenant</option>
      <?php foreach($kategori as $row) {
        if($row->id == $menu->kategori){ ?>
          <option value="<?php echo $row->id ?>" selected> <?php echo $row->nama ?> </option>
        <?php }
        else{ ?>
          <option value="<?php echo $row->id ?>"> <?php echo $row->nama ?> </option>
        <?php }
        } ?>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Harga Menu</label>
  <div class="col-sm-9">
    <input id="harga_menu" class="form-control text-right" placeholder="" name="harga_menu" type="number" value="<?php echo $menu->harga_menu ?>" min="0" step="500" data-number-to-fixed="2" data-number-stepfactor="100">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Diskon Menu</label>
  <div class="col-sm-9">
    <input id="discount" class="form-control text-right" placeholder="" name="discount" type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="1" value="<?php echo $menu->discount ?>">
  </div>
</div>