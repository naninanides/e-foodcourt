<input id="id_tenant" class="form-control" type="hidden" name="id_tenant" value="<?php echo $data[0]->id_tenant; ?>">
<div class="form-group">
  <label class="col-sm-3 control-label">Pemilik</label>
  <div class="col-sm-9">
    <select  name="data_pemilik" id="data_pemilik" class="form-control" style="width: 100%">
      <?php foreach($pemilik as $row) {
        if($row->id == $data[0]->u_id){ ?>
          <option value="<?php echo $row->id ?>" selected> <?php echo $row->first_name." ".$row->last_name ?> </option>
        <?php }
        else{ ?>
          <option value="<?php echo $row->id ?>"> <?php echo $row->first_name." ".$row->last_name ?> </option>
        <?php }
        } ?>
    </select>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Nama Tenant</label>
  <div class="col-sm-9">
    <input id="nama_tenant" class="form-control" type="text" name="nama_tenant" value="<?php echo $data[0]->nama_tenant; ?>">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Kategori Tenant</label>
  <div class="col-sm-9">
    <input id="kat_tenant" class="form-control" type="text" name="kat_tenant" value="<?php echo $data[0]->kat_tenant; ?>">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Deskripsi Tenant</label>
  <div class="col-sm-9">
    <textarea id="desk_tenant" class="form-control" rows="2" name="desk_tenant"><?php echo $data[0]->desk_tenant; ?></textarea>
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">No Telepon 1 </label>
  <div class="col-sm-9">
    <input id="notel1_tenant" class="form-control" type="text" name="notel1_tenant" value="<?php echo $data[0]->notel1_tenant; ?>">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">No Telepon 2 </label>
  <div class="col-sm-9">
    <input id="notel2_tenant" class="form-control" type="text" name="notel2_tenant" value="<?php echo $data[0]->notel2_tenant; ?>">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Email </label>
  <div class="col-sm-9">
    <input id="email_tenant" class="form-control" type="email" name="email_tenant" value="<?php echo $data[0]->email_tenant; ?>">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Foto tenant</label>
  <div class="col-sm-9">
    <input id="foto_tenant" class="form-control" type="file" name="foto_tenant">
  </div>
</div>
<div class="form-group">
  <label class="col-sm-3 control-label">Status</label>
  <div class="col-sm-9">
    <label class="checkbox-inline i-checks">
      <input type="radio" name="status_tenant" value="1" <?php echo ($data[0]->status_tenant == 1 ? 'checked' : '') ?>>
      <i></i>
      Aktif
    </label>
    <label class="checkbox-inline i-checks">
      <input type="radio" name="status_tenant" value="0" <?php echo ($data[0]->status_tenant != 1 ? 'checked' : '') ?>>
      <i></i>
      Tidak Aktif
    </label>
  </div>
</div>