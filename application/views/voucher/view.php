
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">
    <a href data-toggle="modal" class="btn btn-primary open-Modal pull-right"><i class="fa fa-plus"></i> Tambah</a>
    <i class="fa fa-building"></i> Voucher 
  </h1>
</div>
<div class="col">
  <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="table-responsive">
            <table id="dataTable_vouchers" ui-jq="dataTable" ui-options="{
              processing: true,
              serverSide: true,
              ajax: {
                url: '<?php echo site_url('api/Voucher/') ?>',
                type: 'POST'
              },
              columns: [
              { 
                data: 'noAGP',
                render: function ( data, type, row, meta ) {
                  return '<a href=Transaksi/'+data+' target=_blank>'+data+'</a>';
                }
              },
              { data: 'nama'},
              { data: 'nominal',render: $.fn.dataTable.render.number( ',', '.', 0 ),className: 'text-right'  },
              { data: 'tgl_exp' },
              { data: 'sts_tu'},
              { data: 'TotalPembelian',render: $.fn.dataTable.render.number( ',', '.', 0 ),className: 'text-right'  },
            
              ],
              order: [[0, 'asc']],
              pageLength: 25
			}" class="table table-striped b-t b-b">
              <thead>
                <tr>
                  <th>No AGP</th>
                  <th>Nama</th>
                  <th class="text-left">Nominal</th>
                  <th>Tanggal Expired</th>
                  <th>Status</th>
                  <th>Total Pembelian</th>
                 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
         
        </div>
      </div>
    </div>
  </div>
</div>

<script>
 
  function render_noAGP(data, type, row, meta) {
    return '<a class="edit-tenant" href="#" data-id="' + row.id_tenant + '" title="Edit">' + data + '</a>';
  }

  
</script>