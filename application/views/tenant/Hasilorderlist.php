  <script>
  $( function() {
    $( "#datepicker" ).datepicker({
    	format: 'yyyy-mm-dd'
    });
  } );
  </script>
<div class="table-responsive" style="margin:10px;">
<div class="h-30">
	<p class="col-xs-2">
Status: 
<select id="table-filter">
<option value="">All</option>
<option>Sudah Bayar</option>
<option>Belum Bayar</option>
</select>
</p>
	<p class="col-xs-5">
Tenant: 
<select id="select_tenant">
<option value="">    Semua Tenant   </option>

</select>
</p>
<p> Tanggal :     <input type="text" id="datepicker" name=""></p>
</div>
	<table id="tbl2" class="table table-striped b-t b-light" ui-jq="dataTable" ui-options="{
		   ajax: '<?php echo site_url('api/hasilorder/index') ?>',
		   columns: [			
		   		{ data: 'id_transaksi' },			   
			   	{ data: 'waktu_transaksi' },
			   	{ data: 'status_bayar' },					   
			   	{ data: 'TotalPembayaran' },
			   	{ data: 'nama_tenant' }
		   ],
		   order: [[0, 'desc']],
		   dom: 'lrtip'
		   }" class="table table-striped b-t b-b">
			<thead>
				<tr>
					<th class="col-md-1">ID Transaksi</th>
					<th class="col-md-2">Waktu</th>														
					<th class="col-md-2">Status</th>							
					<th class="col-md-2">Total Pembayaran</th>													
					<th class="col-md-2">Tenant</th>
							
				</tr>
			</thead>
			<tbody>
			</tbody>
	</table>
</div>
<?php $this->load->view('delete-modal'); ?>
<script>

	$('#table-filter').on('change', function(){
       $('#tbl2').DataTable().search(this.value).draw();   
    });

	$('#select_tenant').on('change', function(){
       $('#tbl2').DataTable().search(this.value).draw();   
    });

$('#datepicker').on('keyup', function(){
       $('#tbl2').DataTable().search(this.value).draw();   
    });

$( document ).ready(function() {
   select = document.getElementById('select_tenant');

	 $.ajax({
      type:'ajax',
      method:'get',
      url:'<?php echo site_url('tenant/hasilorder/get_data_tenant') ?>',
      dataType:'json',
      success:function(data){
      			console.log('tes');
				for (index = 0; index < data.length; ++index) {
					var opt = document.createElement('option');
				    opt.value = data[index]['nama_tenant'];
				    opt.innerHTML = data[index]['nama_tenant'];
				    select.appendChild(opt);
				}

      }
   });
});

</script>