<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3"><i class="fa fa-cutlery"></i> Order</h1>
</div>
<div class="col">
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="row">
			<div class="col-lg-7">
				<div class="panel panel-default">
					<div class="panel-heading">
						<a>  Menu List  </a>	|
						<a id="makanan">  Makanan  </a>|
						<a id="minuman">  Minuman  </a>
					</div>
					<div class ="panel-body" >
						<div  class="table-responsive">					
							<div id="tabelMenu" class="text-center">
								<?php include('menuList.php'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5" ng-controller="FormDemoCtrl">
				<div class="panel panel-default">
					<div class="panel-heading">
						Order List
					</div>
					<div class="panel-heading ">
						<span class="font-bold">No Meja : </span><input type="text" name="nomeja" id="nomeja">
					</div>
					<div class="table-responsive">
						<table id="tabelOrder" class="table table-striped b-t b-light">
							<thead>
							  <tr>
							  <th style="visibility: hidden;">ID</th>
								<th class="col-md-7">Nama Makanan</th>
								<th class="col-md-2 text-center">Qty</th>
								<th class="col-md-1 text-center"></th>
								<th class="col-md-2 text-center">Harga</th>
							  </tr>
							</thead>
							<tbody>
							  
							</tbody>
						</table>
						<div>
						<span id="totalPesanan" hidden="true">0</span>
							<span class="col-md-9 font-bold">Diskon</span>
							<span class="col-md-1 font-bold text-center">Rp.</span>
							<span id="diskontotal" class="col-md-2 text-right font-bold">0.000</span>
							<span class="col-md-9 font-bold">PPN 10%</span>
							<span id="taxTotal" class="col-md-3 text-right font-bold">0</span>
							<span class="col-md-9 font-bold ">Total</span>
							<span class="col-md-1 font-bold text-center">Rp.</span>
							<span id="totalHarga" class="col-md-2 text-right font-bold ">0</span>
							<div class="panel col-md-12 wrapper-md">
								<button type="button" id="btnOrder" class="btn btn-success col-md-12" data-toggle="modal" data-target="#om00"><i class="fa fa-money"></i> Bayar</button>
							</div>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<script>

	$(document).on("submit",'#form_order',function(e) {
		var total =parseInt(document.getElementById('totalHarga').innerHTML.replace(/,/g, ''));
		e.preventDefault();
      $.ajax({
         type:"post",
         url:"<?php echo site_url('tenant/Order/data_form');?>",
         dataType: 'json',
         data: $(this).serialize(),
         success:function(data){
     
          var dt = data;
          alert("Order Berhasil");
           var table = document.getElementById ("tabelOrder");
            var row = table.insertRow (1);
            var cellID = row.insertCell (0);
            var cellNama = row.insertCell (1);
            var cellQty = row.insertCell (2);
            var cellRp = row.insertCell(3);
            var cellHarga = row.insertCell (4);
            cellID.innerHTML = dt[0];
            cellID.style.visibility ='hidden';
            cellNama.innerHTML = dt[1];
            cellNama.className = "col-md-7";
            cellRp.innerHTML="Rp.";
            cellRp.className="col-md-1 font-bold text-center";
            cellHarga.innerHTML=dt[2];
            cellHarga.className = "col-md-2 text-center";
            cellQty.innerHTML=dt[3];
            cellQty.className = "col-md-2 text-center";
            total=total+parseInt(dt[4]);
            var tax = total * 0.1;
            totalOrder=total+tax;
            total=numberWithCommas(total);
            document.getElementById('totalPesanan').innerHTML=total;
            tax = numberWithCommas(tax);
            document.getElementById('taxTotal').innerHTML=tax;
           totalOrder=numberWithCommas(totalOrder);
           document.getElementById('totalHarga').innerHTML=totalOrder;

         },
         error: function(XMLHttpRequest, textStatus, errorThrown) { 
        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
    }       
      });
  });

$(function() {
    $('#makanan').click(function() {
        $('#tabelMenu').fadeOut('slow').load('tenant/menuList.php').fadeIn("slow");
    });
});

	$(document).on("click",'#btnOrder',function(){
		var diskontotal=parseInt(document.getElementById('diskontotal').innerHTML.replace(/,/g, ''));
		var taxTotal=parseInt(document.getElementById('taxTotal').innerHTML.replace(/,/g, ''));
		var totalPesanan=parseInt(document.getElementById('totalPesanan').innerHTML.replace(/,/g, ''));
		var totalHarga = parseInt(document.getElementById('totalHarga').innerHTML.replace(/,/g, ''));
		var nomeja = document.getElementById('nomeja').value;
  var array = [];
    var headers = [];
    $('#tabelOrder th').each(function(index, item) {
        headers[index] = $(item).html();
    });
    $('#tabelOrder tr').has('td').each(function() {
        var arrayItem = {};
        $('td', $(this)).each(function(index, item) {
            arrayItem[headers[index]] = $(item).html();
        });
        array.push(arrayItem);
    });

    $.ajax({
    	type:"post",
         url:"<?php echo site_url('tenant/Order/data_order');?>",
         dataType: 'json',
         data: {'array':array, 'diskontotal':diskontotal, 'taxTotal':taxTotal, 'totalHarga':totalHarga, 'nomeja':nomeja,'totalPesanan':totalPesanan},
         success:function(){
         	
         }

    });
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}



  </script>