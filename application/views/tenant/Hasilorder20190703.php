<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3">
		<div class="pull-right">
			<a id="btn-download" class="btn btn-info" href="" download>
				<i class="fa fa-cloud-download"></i>
				Download
			</a>
		</div>
		<i class="fa fa-shopping-cart"></i> Hasil Order
	</h1>
</div>
<div class="col">
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div id="table-filter" class="panel-body form-inline">
						<label class="control-label m-r-xs">Status</label>
						<select id="filter-status" name="filter-status" class="form-control">
							<option value="">(Semua)</option>
							<option value="1">Sudah Bayar</option>
							<option value="0">Belum Bayar</option>
						</select>
						<label class="control-label m-r-xs m-l-md">Tenant</label>
						<select id="filter-tenant" name="filter-tenant" class="form-control" style="width: 200px">
							<?php if ($this->session->userdata('id_tenant')){ ?>
							<?php foreach($tenants as $tenant):
								if ($tenant->id_tenant==$this->session->userdata('id_tenant')){ ?>
									<option value="<?php echo $tenant->id_tenant; ?>" selected><?php echo $tenant->nama_tenant ?></option>
							<?php } ?>
							<?php endforeach; ?>
						<?php }
						else { ?>
							<option value="">(Semua Tenant)</option>
							<?php foreach($tenants as $tenant):?>
								<option value="<?php echo $tenant->id_tenant; ?>"><?php echo $tenant->nama_tenant ?></option>
							<?php endforeach;
						} ?>
						</select>
						<label class="control-label m-r-xs m-l-md">Tanggal</label>
						<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" style="width: 170px;">
							<input type="text" name="filter-tanggal-start" id="filter-tanggal-start" value="<?php echo date('d/m/Y') ?>" class="form-control" placeholder="DD/MM/YYYY">
							<div class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</div>
						</div>
						<label class="control-label m-r-sm m-l-sm">s/d</label>
						<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" style="width: 170px;">
							<input type="text" name="filter-tanggal-end" id="filter-tanggal-end" value="<?php echo date('d/m/Y') ?>" class="form-control" placeholder="DD/MM/YYYY">
							<div class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</div>
						</div>
					</div>
					<div class="table-responsive">
						<table id="dataTable_orders" class="table table-striped b-t b-b b-light" ui-jq="dataTable" ui-options="{
							ajax: {
								url: '<?php echo site_url('api/hasilorder/index') ?>',
								type: 'POST',
								data: table_params
							},
							columns: [			
								{ data: 'id_transaksi', class: 'text-right' },			   
								{ data: 'waktu_transaksi', render: render_date },
								{ data: 'status_bayar' },					   
								{ data: 'TotalPembayaran', render: render_number, class: 'text-right' },
								{ data: 'nama_tenant' }
							],
							order: [[0, 'desc']],
							dom: 'Bfrtip',
							buttons: [
								'excel', 'pdf', 'print'
							]
							}">
							<thead>
								<tr>
									<th style="width: 100px">ID Transaksi</th>
									<th style="width: 150px">Waktu</th>														
									<th style="width: 150px">Status</th>							
									<th style="width: 150px">Total Pembayaran</th>													
									<th>Tenant</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						<?php //include('Hasilorderlist.php'); ?>
						<?php include('Menuadd.php'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<script>
function table_params(params) {
	var tanggal_start = moment($('#filter-tanggal-start').val(), "DD/MM/YYYY");
	if (!tanggal_start.isValid()) {
		tanggal_start = moment();
	}
	var tanggal_end = moment($('#filter-tanggal-end').val(), "DD/MM/YYYY");
	if (!tanggal_end.isValid()) {
		tanggal_end = moment();
	}
	params.status_bayar = $('#filter-status').val();
	params.id_tenant = $('#filter-tenant').val();
	params.tanggal_start = tanggal_start.format('YYYY-MM-DD');
	params.tanggal_end = tanggal_end.format('YYYY-MM-DD');
	return params;
}

function render_date(data, type, row, meta) {
	if (type === 'display')
		return moment(data).format('DD/MM/YYYY HH:MM:SS');
	return data;
}
function render_number(data, type, row, meta) {
	if (type === 'display')
		return numeral(data).format('0,0');
	return data;
}

$(window).load(function() {
	$('#table-filter').on('change', function() {
		var params = table_params({});
		$('#btn-download').attr('href', '<?php echo site_url('tenant/hasilorder/download/') ?>?' + $.param(params));
		$('#dataTable_orders').DataTable().ajax.reload();
	}).trigger('change');
});
</script>