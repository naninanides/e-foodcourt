<?php
header('Content-Type: application/vnd.ms-excel');
header("Content-Disposition:attachment;filename=laporan.xls");
header("Pragma:no-cache");
header("Expires:0");
?>
<style>
body {
    font-family: "Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 12px;
}
table {
    width: 100%;
    border: 1px solid #edf1f2;
    border-spacing: 0;
    border-collapse: collapse;
}
td, th {
    padding: 8px 15px;
    border-top: 1px solid #eaeff0;
    line-height: 1.42857143;
    vertical-align: top;
    box-sizing: content-box;
}
th {
    text-align: left;
}
.odd td {
    background-color: #f9f9f9;
}
.subfoot{
    background-color: #f2e2e1;
}
</style>
<table id="dataTable_orders" class="nowrap table-bordered table table-striped " style="width:100%" >
    <thead>
        <tr>
            
            <th style="width: 0%">No.</th>
            <th style="width: 0%">Tanggal</th>	
            <th style="width: 0%">Tenant</th>
            <th style="width: 0%">Menu</th>
            <th style="width: 1%">Jumlah</th>
            <th  style="width: 10%">Harga Satuan</th>
            <th  style="width: 10%">Harga</th>

        </tr>
    </thead>
    <tbody>
        <?php 
        foreach($tenant_count as $tc){
            $tenant_arr[$tc->id_tenant]=$tc->t_count;
        }
        $ix=0;
        $tenant="";
        $total_tenant=0;
        $grand_total=0;
        ?>
        <?php 
        foreach($datas as $index => $row): 					
            ?>
            <tr class="<?php echo ($index % 2 == 0 ? '' : 'odd') ?>">
            <?php 
            $ix++;
            $tenant=$row->nama_tenant;
            $total_tenant=($row->jumlah_pesanan * $row->harga) + $total_tenant;
            $grand_total=($row->jumlah_pesanan * $row->harga) + $grand_total;
            $count_tenant=$tenant_arr[$row->id_tenant];

            ?>

                <td style="text-align: right"><?php echo $ix; ?></td>
                <td><?php echo $row->waktu_transaksi ?></td>
                <td><?php echo $row->nama_tenant ?></td>
                <td><?php echo $row->nama_menu ?></td>
                <td><?php echo $row->jumlah_pesanan ?></td>
                <td align="right"><?php echo $row->harga ?></td>

                <td style="text-align: right"><?php echo ($row->jumlah_pesanan * $row->harga)  ?></td>
                
                
            </tr>
            <?php
            if($count_tenant==$ix){ 
                $ix=0;
                
            ?>
            <tr class="subfoot">
                
                <td colspan="6"><?php echo $tenant; ?></td>
                <td align="right"><?php $total_tenant;?></td>

            </tr>
            <?php 
                $total_tenant=0;
            }
            ?>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="6" style="text-align:right">Total:</th>
            <th style="text-align:right"><?php echo $grand_total; ?></th>
        </tr>
    </tfoot>
</table>