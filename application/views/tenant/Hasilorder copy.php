<style>
	.printonly{
		display: none;
	}
	@media print { 
		.hideprint { 
		display:none;
		} 
		.dataTables_length  { 
		display:none;
		} 
		.input-group{
			width: 40%;
		}
		.dataTables_filter, .pagination ,.app-footer,.app-header,.navbar,.app-aside { 
		display:none;
		} 
		.control-label {
			width:100px;display:inline-block;
		}
		.printonly{
			display: block;
		}
		.judulreport{
			text-align: right;
		}
	} 
	.control-label{
		width: 80px;
	}
</style>
<div class="bg-light lter b-b wrapper-md">
	
	
		<!-- <div class="pull-right">
			<a id="btn-download" class="btn btn-info" href="" download>
				<i class="fa fa-cloud-download"></i>
				Download
			</a>
		</div> -->
		<table width="100%">
			<tr>
				<td class="printonly">
					<img src="<?php echo assets_url('img/logomag.png'); ?>" width="100px">
				</td>
				<td class="judulreport">
					<h1 class="m-n font-thin h3">
						<i class="fa fa-shopping-cart"></i> Report Penjualan
					</h1>
				</td>
			</tr>
		</table>
		
		
	
</div>
<div class="col">
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div id="table-filter" class="panel-body form-inline  ">
						<table width="100%">
							<tr>
								<td>
									Status
								</td>
								<td>
									<select id="filter-status" name="filter-status" class="form-control hideprint">
										<option value="">(Semua)</option>
										<option value="1">Sudah Bayar</option>
										<option value="0">Belum Bayar</option>
									</select>
								</td>
								<td>
									Tenant
								</td>
								<td>
									<select id="filter-tenant" name="filter-tenant" class="form-control" >
										<?php if ($this->session->userdata('id_tenant')){ ?>
										<?php foreach($tenants as $tenant):
											if ($tenant->id_tenant==$this->session->userdata('id_tenant')){ ?>
												<option value="<?php echo $tenant->id_tenant; ?>" selected><?php echo $tenant->nama_tenant ?></option>
										<?php } ?>
										<?php endforeach; ?>
									<?php }
									else { ?>
										<option value="">(Semua Tenant)</option>
										<?php foreach($tenants as $tenant):?>
											<option value="<?php echo $tenant->id_tenant; ?>"><?php echo $tenant->nama_tenant ?></option>
										<?php endforeach;
									} ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>
									Tanggal
								</td>
								<td>
									<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" >
										<input type="text" name="filter-tanggal-start" id="filter-tanggal-start" value="<?php echo date('d/m/Y') ?>" class="form-control" placeholder="DD/MM/YYYY">
										<div class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</div>
									</div>
								</td>
								<td>
									s/d
								</td>
								<td>
									<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" >
										<input type="text" name="filter-tanggal-end" id="filter-tanggal-end" value="<?php echo date('d/m/Y') ?>" class="form-control" placeholder="DD/MM/YYYY">
										<div class="input-group-addon">
											<span class="fa fa-calendar"></span>
										</div>
									</div>
								</td>
							</tr>
							
						</table>
						
					</div>
					<div class="panel-body form-inline">
					<div class="table-responsive">
						<table id="dataTable_orders" class="nowrap table-bordered table table-striped " style="width:100%" >
							<thead>
								<tr>
									
									<th style="width: 0%">ID Transaksi</th>
									<th style="width: 0%">Tenant</th>
									<th style="width: 0%">Waktu</th>	
									<th style="width: 0%">Total Pembayaran</th>														
									<th style="width: 2%">Status</th>							
																					
									
									<th style="width: 20%">Item</th>
									<th style="width: 1%">Jumlah</th>
									
									<th style="width: 15%">Pembeli</th>
									<th  style="width: 10%">Harga</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="8" style="text-align:right">Total:</th>
									<th></th>
								</tr>
							</tfoot>
						</table>
						<?php //include('Hasilorderlist.php'); ?>
						<?php include('Menuadd.php'); ?>
					
					
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<script>
function table_params(params) {
	var tanggal_start = moment($('#filter-tanggal-start').val(), "DD/MM/YYYY");
	if (!tanggal_start.isValid()) {
		tanggal_start = moment();
	}
	var tanggal_end = moment($('#filter-tanggal-end').val(), "DD/MM/YYYY");
	if (!tanggal_end.isValid()) {
		tanggal_end = moment();
	}
	params.status_bayar = $('#filter-status').val();
	params.id_tenant = $('#filter-tenant').val();
	params.tanggal_start = tanggal_start.format('YYYY-MM-DD');
	params.tanggal_end = tanggal_end.format('YYYY-MM-DD');
	return params;
}

function render_date(data, type, row, meta) {
	if (type === 'display')
		return moment(data).format('DD/MM/YYYY HH:MM:SS');
	return data;
}
function render_number(data, type, row, meta) {
	if (type === 'display')
		return numeral(data).format('0,0');
	return data;
}

$(window).load(function() {
	$('#table-filter').on('change', function() {
		var params = table_params({});
		$('#btn-download').attr('href', '<?php echo site_url('tenant/hasilorder/download/') ?>?' + $.param(params));
		$('#dataTable_orders').DataTable().ajax.reload();
	}).trigger('change');
});

$(document).ready(function() {
	var groupColumn = 0;
	var collapsedGroups = {};
	function addCommas(nStr)
	{
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + '.' + '$2');
		}
		return x1 + x2;
	}
    var table = $('#dataTable_orders').DataTable({
		dom: 'Bfrtip',
        buttons: [
            'pdf'
        ],
		/* "lengthChange": false, */
		"pageLength": 300,
		ajax: {
			url: '<?php echo site_url('api/hasilorder/index') ?>',
			type: 'POST',
			data: table_params
		},
		"columnDefs": [
            { "visible": false, "targets": groupColumn },
			{ "visible": false, "targets": 1 },
			{ "visible": false, "targets": 2 },
			{ "visible": false, "targets": 3 }
        ],
		columns: [	
					
			{ data: 'id_transaksi', class: 'text-right' },	
			{ data: 'nama_tenant' },		   
			{ data: 'waktu_transaksi', render: render_date },
			{ data: 'TotalPembayaran', render: render_number, class: 'text-right' },
			
			{ data: 'status_bayar' },					   
			
			{ data: 'nama_menu' },
			{ data: 'jumlah_pesanan', class: 'text-right'  },
			
			{ data: 'nama_depan' },
			{ data: 'harga' , render: render_number, class: 'text-right' }
		],
		"order": [[ groupColumn, 'asc' ]],
		"drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
			
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
					var rowData = table.row(i).data();
					
                    $(rows).eq( i ).before(
                        '<tr class="group" ><td colspan="9" style="background-color:#b6dbc0;"><strong>ID Transaksi</strong> : '+group+' | <strong>Tenant</strong> :' +rowData['nama_tenant']+' | <strong>Waktu Transaksi</strong> : ' + rowData['waktu_transaksi'] + ' | <strong>Total</strong> : ' +addCommas(rowData['TotalPembayaran'])+ ' </td></tr>'
                    );
 
                    last = group;
                }
            } );
		},
		
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/['\$',]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 8 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 8, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 8 ).footer() ).html(
                'Rp '+ addCommas(total) 
            );
        }


		/* rowGroup: {
			// Uses the 'row group' plugin
			dataSrc: 'id_transaksi',
			startRender: function (rows, group) {
				var collapsed = !!collapsedGroups[group];

				rows.nodes().each(function (r) {
					r.style.display = collapsed ? 'none' : '';
				});    

				// Add category name to the <tr>. NOTE: Hardcoded colspan
				return $('<tr/>')
					.append('<td colspan="8"><strong>Id Transaksi</strong> :' + group + '  </td>')
					.attr('data-name', group)
					.toggleClass('collapsed', collapsed);
			}
		} */
		
	} );
/* 	$('#dataTable_orders tbody').on('click', 'tr.group', function () {
        var name = $(this).data('name');
        collapsedGroups[name] = !collapsedGroups[name];
        table.draw();
    }); */
 

} );
</script>