<script>
$( function() {
	$( ".datepicker" ).datepicker({
		format: 'dd/mm/yyyy'
	});
} );
</script>
<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3"><i class="fa fa-table"></i> Laporan</h1>
</div>
<div class="wrapper-md">
	<?php echo messages() ?>
	<div class="panel panel-default">
        <div class="panel-body">
					<div class="form-group">
						<form name="tglfilter" action="<?php echo site_url('laporan/peminjaman_pdf') ?>" method="post">
					<label for="tgl_awal" class="control-label col-md-10 col-lg-8">Tanggal</label>
					<div class="col-md-8 col-lg-10">
						<div class="w-md pull-left">
							<div class="input-group date">
								<input type="text" name="tgl_awal" id="tgl_awal" class="form-control datepicker" placeholder="DD/MM/YYYY">
								<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</div>
						<div class="w-xxs pull-left text-center">
							<p class="form-control-static" style="font-weight: normal">s/d</p>
						</div>
						<div class="w-md pull-left">
							<div class="input-group date">
								<input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control datepicker" placeholder="DD/MM/YYYY">
								<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
							</div>
						</div><button id="filter-btn" type="button" class="btn btn-primary"><i class="fa fa-search"></i> Filter Tanggal</button>
					</div>						
					<label for="status" class="control-label col-md-10 col-lg-8">Status</label>
					<div class="col-md-8 col-lg-10">
						<div class="w-md pull-left">
<select id="statusbayar">
<option >All</option>
<option >Sudah Bayar</option>
<option >Belum Bayar</option>
</select>
						</div>

					</div>

					<div class="btn btn-primary pull-right" style="padding:0px 4px;"><i class="fa fa-file-pdf-o"></i><input type="submit" value="Download" class="btn btn-primary" ></div>
					<!-- <input href="<?php// echo site_url('laporan/kunjungan_pdf') ?>" target="_blank" class="btn btn-primary pull-right"> Download -->

				</form>



				</div>
			



        </div>
		<div class="table-responsive">
			<table id="dataTable_laporan" ui-jq="dataTable" ui-options="{
				processing: true,
				serverSide: true,
				ajax: {
					url: '<?php echo site_url('tenant/Laporantenant/dt_laporan') ?>',
					data: addFilter,
					type: 'POST'
				},
				columns: [
					{ data: 'id_transaksi' },			   
			   	{ data: 'waktu_transaksi' },
			   	{ data: 'status_bayar' },					   
			   	{ data: 'TotalPembayaran' },
			   	{ data: 'nama_tenant' }
				],
				order: [[0, 'desc']],
				pageLength: 25
				}" class="table table-striped b-t b-b">
				<thead>
					<tr>
						<th class="col-md-1">ID Transaksi</th>
					<th class="col-md-2">Waktu</th>														
					<th class="col-md-2">Status</th>							
					<th class="col-md-2">Total Pembayaran</th>													
					<th class="col-md-2">Tenant</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>
function render_judul(data, type, row, meta) {
	if (type === 'display')
		return '<a href="<?php echo site_url('pustaka/view') ?>/' + row.pustaka_id + '" target="_blank">' + row.no_barcode + '<br>' + data + '</a>';
	return data;
}

function render_member(data, type, row, meta) {
	if (type === 'display')
		return '<a href="<?php echo site_url('member/view') ?>/' + row.user_id + '" target="_blank">' + row.member_num + '<br>' + data + '</a>';
	return data;
}

$('#statusbayar').on('change', function(){

       $('#dataTable_laporan').DataTable().search(this.value).draw();   
    });
function addFilter(params) {
	if ($('#statusbayar').val() !== '')
		params.statusbayar = $('#statusbayar').val();
	if ($('#tgl_awal').val() !== '')
		params.tgl_awal = moment($('#tgl_awal').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
	if ($('#tgl_akhir').val() !== '')
		params.tgl_akhir = moment($('#tgl_akhir').val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
	return params;
}

$(function() {

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href !== "" && (this.href === url || url.href.indexOf(this.href) === 0);
    // }).parent().addClass('active').parent().parent();
	// if (element.is('li')) {
    //     element.addClass('active');
    // }

	$(".input-group.date").datepicker(defaultDateOptions);



	// Filter Collapse
	applyFilterCollapse();

	$('#filter-btn').on('click', function() {
		var $dataTable = $('[ui-jq="dataTable"]');
		if ($dataTable.length > 0) {
			$dataTable.DataTable().ajax.reload();
		}
	});
	$('#reset-filter-btn').on('click', function() {
		var $parent = $(this).parents('.panel-body');
		$parent.find('input').val('');
		$parent.find('select').val('').trigger('change');

		$('#filter-btn').click();
	});
	$('#download-pdf-btn').on('click', function(e) {
		var filterParams = addFilter({});
		var $this = $(this);
		$this.attr('href', $this.attr('siteurl') + '?' + $.param(filterParams));
	});

	$('body').trigger('mainloaded');
});

function filter(table, url) {
	$('#'+table+' > tbody').html('<tr><td>Loading ...</td></tr>');
	$('#'+table).DataTable().ajax.url(url).load();
}
</script>
