<style>
	.printonly{
		display: none;
	}
	@media print { 
		.hideprint { 
		display:none;
		} 
		.dataTables_length  { 
		display:none;
		} 
		.input-group{
			width: 40%;
		}
		.dataTables_filter, .pagination ,.app-footer,.app-header,.navbar,.app-aside { 
		display:none;
		} 
		.control-label {
			width:100px;display:inline-block;
		}
		.printonly{
			display: block;
		}
		.judulreport{
			text-align: right;
		}
	} 
	.control-label{
		width: 80px;
	}
</style>
<div class="bg-light lter b-b wrapper-md">
	
	
		<div class="pull-right">
			<a id="btn-download" class="btn btn-info" href="Hasilorder?<?php echo $_SERVER['QUERY_STRING'] ?>&download=yes" >
				<i class="fa fa-cloud-download"></i>
				Download
			</a>
		</div>
		<table width="100%">
			<tr>
				<td class="printonly">
					<img src="<?php echo assets_url('img/logomag.png'); ?>" width="100px">
				</td>
				<td class="judulreport">
					<h1 class="m-n font-thin h3">
						<i class="fa fa-shopping-cart"></i> Report Penjualan
					</h1>
				</td>
			</tr>
		</table>
		
		
	
</div>
<div class="col">
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div id="table-filter" class="panel-body form-inline  ">
						<form action="">
							<table width="100%">
								<tr>
									
									<td>
										Tenant
									</td>
									<td>
										<select id="id_tenant" name="id_tenant" class="form-control" >
											<?php if ($this->session->userdata('id_tenant')){ ?>
											<?php foreach($tenants as $tenant):
												if ($tenant->id_tenant==$this->session->userdata('id_tenant')){ ?>
													<option value="<?php echo $tenant->id_tenant; ?>" selected><?php echo $tenant->nama_tenant ?></option>
											<?php } ?>
											<?php endforeach; ?>
										<?php }
										else { ?>
											<option value="">(Semua Tenant)</option>
											<?php foreach($tenants as $tenant):?>
												<option value="<?php echo $tenant->id_tenant;?>" <?php if($id_tenant==$tenant->id_tenant){echo 'SELECTED';} ?>><?php echo $tenant->nama_tenant ?></option>
											<?php endforeach;
										} ?>
										</select>
									</td>
									<td>
										&nbsp;
									</td>
									<td>
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										Tanggal
									</td>
									<td>
										
										<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" >
											<input type="text" name="tanggal_start" id="tanggal_start" value="<?php echo $tanggal_start ?>" class="form-control" placeholder="DD/MM/YYYY">
											<div class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</div>
										</div>
									</td>
									<td>
										s/d
									</td>
									<td>
										<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" >
											<input type="text" name="tanggal_end" id="tanggal_end" value="<?php echo $tanggal_end ?>" class="form-control" placeholder="DD/MM/YYYY">
											<div class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td colspan='4' ><input type="submit"> </td>
								</tr>
								
							</table>
						</form>	
					</div>
					<div class="panel-body form-inline">
					<div class="table-responsive">
						<table id="dataTable_orders" class="nowrap table-bordered table table-striped " style="width:100%" >
							<thead>
								<tr>
									
									<th style="width: 0%">No.</th>
									<th style="width: 0%">Kode Order</th>
									<th style="width: 0%">Tanggal</th>	
									<th style="width: 0%">Tenant</th>
									<th style="width: 0%">Menu</th>
									<th style="width: 1%">Jumlah</th>
									<th  style="width: 10%">Harga Satuan</th>
									<th  style="width: 10%">Harga</th>

								</tr>
							</thead>
							<tbody>
								<?php 
								foreach($tenant_count as $tc){
									$tenant_arr[$tc->id_tenant]=$tc->t_count;
								}
								$ix=0;
								$tenant="";
								$total_tenant=0;
								$grand_total=0;
								?>
								<?php 
								foreach($datas as $index => $row): 					
									?>
									<tr class="<?php echo ($index % 2 == 0 ? '' : 'odd') ?>">
									<?php 
									$ix++;
									$tenant=$row->nama_tenant;
									$total_tenant=($row->jumlah_pesanan * $row->harga) + $total_tenant;
									$grand_total=($row->jumlah_pesanan * $row->harga) + $grand_total;
									$count_tenant=$tenant_arr[$row->id_tenant];

									?>

										<td style="text-align: right"><?php echo $ix; ?></td>
										<td><?php echo $row->kode_order ?></td>
										<td><?php echo $row->waktu_transaksi ?></td>
										<td><?php echo $row->nama_tenant ?></td>
										<td><?php echo $row->nama_menu ?></td>
										<td><?php echo $row->jumlah_pesanan ?></td>
										<td align="right"><?php echo number_format($row->harga, 0, ',', '.') ?></td>

										<td style="text-align: right"><?php echo number_format(($row->jumlah_pesanan * $row->harga) , 0, ',', '.') ?></td>
										
										
									</tr>
									<?php
									if($count_tenant==$ix){ 
										$ix=0;
										
									?>
									<tr>
										
										<td colspan="7"><?php echo $tenant; ?></td>
										<td align="right"><?php echo number_format($total_tenant,0,',','.');?></td>

									</tr>
									<?php 
										$total_tenant=0;
									}
									?>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="7" style="text-align:right">Total:</th>
									<th style="text-align:right"><?php echo number_format($grand_total,0,',','.'); ?></th>
								</tr>
							</tfoot>
						</table>
						
					
					
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<script>
	$(document).ready(function(){
        $('#btn-download').each(function(){ 
			newhref= urlParams.toString();
			newhref=newhref +escape("&download=yes");
            $(this).attr("href", newhref ); // Set herf value
        });
    });
</script>
