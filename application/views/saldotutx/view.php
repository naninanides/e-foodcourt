
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">
    
    <i class="fa fa-building"></i> Rekap Karyawan 
  </h1>
</div>
<div class="col">
  <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
        <div id="table-filter" class="panel-body form-inline  ">
						<form action="">
							<table >
								
								<tr>
									<td>
										As of &nbsp;&nbsp;
									</td>
									<td>				
										<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" >
											<input type="text" name="enddate" id="enddate" value="<?php echo $enddate ?>" class="form-control" placeholder="DD/MM/YYYY">
											<div class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</div>
										</div>
                  </td>
                  <td>&nbsp;&nbsp;<input type="submit"> </td>
								</tr>
								
							</table>
						</form>	
          </div>
          

          <div class="table-responsive">
            <table id="dataTable_vouchers" ui-jq="dataTable"  class="table table-striped b-t b-b">
              <thead>
                <tr>
                  <th>No AGP</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Topup Voucher</th>
                  <th>Topup Cash</th>
                  <th>Topup Other</th>
                  <th>Transaction</th>
                  <th>Topup Refund</th>
                  <th>Saldo</th>
                  
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
         
        </div>
      </div>
    </div>
  </div>
</div>

<script>
 
  function render_noAGP(data, type, row, meta) {
    return '<a class="edit-tenant" href="#" data-id="' + row.id_tenant + '" title="Edit">' + data + '</a>';
  }

  $(document).ready(function() {
    $('#dataTable_vouchers').DataTable( {
  
              processing: true,
              serverSide: true,
              dom: 'Bfrtip',
              buttons: [
                'excel'
              ],
              ajax: {
                url: '<?php echo site_url('api/Saldotutx/') ?>',
                type: 'POST'
              },
              paging:         false,
              columns: [
              { 
                data: 'noAGP',
                render: function ( data, type, row, meta ) {
                  return '<a href=Transaksi/'+data+' target=_blank>'+data+'</a>';
                }
              },
              { data: 'qr'},
              { data: 'nama_lengkap'},
              { data: 'topupvoucher',render: $.fn.dataTable.render.number( ',', '.', 0 ),className: 'text-right'  },
              { data: 'topupcash',render: $.fn.dataTable.render.number( ',', '.', 0 ),className: 'text-right'  },
              { data: 'topupother',render: $.fn.dataTable.render.number( ',', '.', 0 ),className: 'text-right'  },
              { data: 'totaltx',render: $.fn.dataTable.render.number( ',', '.', 0 ),className: 'text-right'  },
              { data: 'refund',render: $.fn.dataTable.render.number( ',', '.', 0 ),className: 'text-right'  },
              { data: 'saldo',render: $.fn.dataTable.render.number( ',', '.', 0 ),className: 'text-right'  },
              
            
              ],
              order: [[2, 'asc']],
              
            } );
} );
  
</script>