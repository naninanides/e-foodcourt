
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">
    
    <i class="fa fa-building"></i> Saldo Kartu
  </h1>
</div>
<div class="col">
  <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="table-responsive">
            <table id="dataTable_vouchers" ui-jq="dataTable" ui-options="{
              processing: true,
              serverSide: true,
              ajax: {
                url: '<?php echo site_url('api/Saldotutx/saldokartu') ?>',
                type: 'POST'
              },
              columns: [
              { 
                data: 'noAGP',
                render: function ( data, type, row, meta ) {
                  return '<a href=Transaksi/'+data+' target=_blank>'+data+'</a>';
                }
              },
              { data: 'nik'},
              { data: 'nama_lengkap'},
              { data: 'saldo',render: $.fn.dataTable.render.number( ',', '.', 0 ),className: 'text-right'  },

            
              ],
              order: [[0, 'asc']],
              pageLength: 25
			}" class="table table-striped b-t b-b">
              <thead>
                <tr>
                  <th>No AGP</th>
                  <th>NIK</th>
                  <th>Nama</th>
                  <th>Saldo</th>
                 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
         
        </div>
      </div>
    </div>
  </div>
</div>

<script>
 
  function render_noAGP(data, type, row, meta) {
    return '<a class="edit-tenant" href="#" data-id="' + row.id_tenant + '" title="Edit">' + data + '</a>';
  }

  
</script>