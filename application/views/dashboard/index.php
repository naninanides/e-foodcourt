<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3"><i class="fa fa-dashboard"></i> Dashboard</h1>
</div>
<div class="col">
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="row">
			<div class="col-md-12">
				<form class="form-inline">
					<div class="panel">
						<div class="panel-body form-inline">
							<label class="control-label m-r-xs m-l-md">Tanggal</label>
							<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" style="width: 170px;">
								<input type="text" name="start" id="filter-tanggal-start" value="<?php echo date('d/m/Y') ?>" class="form-control" placeholder="DD/MM/YYYY">
								<div class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</div>
							</div>
							<label class="control-label m-r-sm m-l-sm">s/d</label>
							<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" style="width: 170px;">
								<input type="text" name="end" id="filter-tanggal-end" value="<?php echo date('d/m/Y') ?>" class="form-control" placeholder="DD/MM/YYYY">
								<div class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</div>
							</div>
							<button id="btn-grafik-transaksi-filter" type="submit" class="btn btn-default m-l-sm">Filter</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-heading">
						<h3 class="panel-title">Grafik Transaksi</h3>
					</div>
					<div class="panel-body">
						<div id="chart_main" class="chart-main"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 col-md-3 text-center">
				<div class="block panel padder-v bg-danger item">
					<div class="h4">Transaksi</div>
					<div id="jumlah_transaksi" class="h1 text-white font-bold">0</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3 text-center">
				<div class="block panel padder-v bg-primary item">
					<div class="h4">Total Penjualan</div>
					<span id="total_transaksi" class="text-white font-bold h1 block">Rp. 0 </span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<form class="form-inline">
					<div class="panel">
						<div class="panel-body form-inline">
							<label class="control-label m-r-xs m-l-md">Tahun</label>
							<select id="filter-tahun" class="select2 form-control">
							<?php for ($tahun = 2018; $tahun < (date('Y') + 10); $tahun++): ?>
								<option value="<?php echo $tahun ?>"><?php echo $tahun ?></option>
							<?php endfor; ?>
							</select>
							<button id="btn-grafik-2" type="submit" class="btn btn-default m-l-sm">Filter</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel">
					<div class="panel-heading">
						<h3 class="panel-title">Daftar Transaksi dan Pendapatan per Bulan</h3>
					</div>
					<div class="panel-body">
						<div id="chart_2" class="chart-main"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
var chart, chart2;

$(document).ready(function() {
	chart = AmCharts.makeChart( "chart_main", {
		"type": "serial",
		"valueAxes": [ {
			"gridColor": "#FFFFFF",
			"gridAlpha": 0.2,
			"dashLength": 0
		},
		],
		"gridAboveGraphs": true,
		"startDuration": 1,
		"graphs": [ {
			"balloonText": "[[nama_menu]]: <b>[[jumlah_pesanan]]</b>",
			"fillAlphas": 0.8,
			"lineAlpha": 0.2,
			"type": "column",
			"valueField": "jumlah_pesanan"
		} ],
		"chartCursor": {
			"categoryBalloonEnabled": false,
			"cursorAlpha": 0,
			"zoomable": false
		},
		"categoryField": "nama"
	});

	chart2 = AmCharts.makeChart( "chart_2", {
		"type": "serial",
		"valueAxes": [ {
			"id": "jumlahAxis", 
			"gridColor": "#FFFFFF",
			"gridAlpha": 0.2,
			"dashLength": 0,
			"title": "Jumlah Transaksi"
		}, {
			"id": "totalAxis", 
			"position": "right",
			"title": "Pendapatan"
		} ],
		"gridAboveGraphs": true,
		"startDuration": 1,
		"graphs": [ {
			"balloonText": "[[bulan]]: <b>[[jumlah]]</b>",
			"fillAlphas": 0.8,
			"lineAlpha": 0.2,
			"type": "column",
			"valueField": "jumlah",
        	"valueAxis": "jumlahAxis"
		}, {
			"balloonText": "[[bulan]]: <b>Rp. [[total]]</b>",
			"bullet": "round",
			"lineThickness": 3,
			"bulletSize": 7,
			"bulletBorderAlpha": 1,
			"bulletColor": "#FFFFFF",
			"useLineColorForBulletBorder": true,
			"bulletBorderThickness": 3,
			"fillAlphas": 0,
			"lineAlpha": 0.5,
			"valueField": "total",
        	"valueAxis": "totalAxis"
		} ],
		"chartCursor": {
			"categoryBalloonEnabled": false,
			"cursorAlpha": 0,
			"zoomable": false
		},
		"categoryField": "bulan"
	});

	update_grafik_transaksi();
	update_grafik_2();

	$('#btn-grafik-transaksi-filter').on('click', function(e) {
		e.preventDefault();
		update_grafik_transaksi();
	});

	$('#btn-grafik-2').on('click', function(e) {
		e.preventDefault();
		update_grafik_2();
	})
});

function update_grafik_transaksi() {
	$.post('<?php echo site_url('dashboard/get_grafik_transaksi') ?>', {
		'start': $('#filter-tanggal-start').val(),
		'end': $('#filter-tanggal-end').val()
	}, function(data) {
		chart.dataProvider = data.grafik_transaksi;
		chart.validateData();
		$('#jumlah_transaksi').html(numeral(data.jumlah_transaksi).format());
		$('#total_transaksi').html('Rp. ' + numeral(data.total_transaksi).format());
	}, 'json');
}

function update_grafik_2() {
	$.post('<?php echo site_url('dashboard/get_grafik_2') ?>', {
		'tahun': $('#filter-tahun').val()
	}, function(data) {
		chart2.dataProvider = data;
		chart2.validateData();
	}, 'json');
}
</script>