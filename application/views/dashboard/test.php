<div class="bg-light lter b-b wrapper-md">
	<h1 class="m-n font-thin h3"><i class="fa fa-dashboard"></i> Dashboard</h1>
</div>
<div class="col">
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="row">
			<div class="col-md-12">
			  <div class="row row-sm text-center">
				<div class="col-xs-3">
				  <div class="panel padder-v item">
					<div class="h1 text-info font-bold h1">3 <i class="text-muted text-xl icon icon-bell"></i></div>
					<span class="text-muted text-xs">Event Active</span>
				  </div>
				</div>
				<div class="col-xs-3">
				  <a href class="block panel padder-v bg-primary item">
					<span class="text-white font-bold h1 block">2 <i class="text-muted fa fa-gavel"></i></span>
					<span class="text-muted text-xs">Event Prepare</span>
				  </a>
				</div>
				<div class="col-xs-3">
				  <a href class="block panel padder-v bg-info item">
					<span class="text-white font-bold h1 block">3 <i class="text-muted icon-docs"></i></span>
					<span class="text-muted text-xs">Event Request</span> 
					<span class="bottom text-right w-full">
					  <i class="animated infinite tada fa fa-exclamation-triangle text-muted m-r-sm" ></i>
					</span>
				  </a>
				</div>
				<div class="col-xs-3">
				  <a href="" class="block panel padder-v bg-success item">
					<span class="text-white font-bold h1 block">12 <i class="text-muted fa fa-check-square-o"></i></span>
					<span class="text-muted text-xs">Total Event</span> 
				  </a>
				</div>
				<div class="col-xs-12">
				  <div class="panel padder-v item b-r shadow col-sm-2">
					<div class="font-thin h1">500</div>
					<span class="text-muted text-xs">Total Packages</span>					
				  </div>
				  <div class="panel padder-v item b-r col-sm-2">
					<div class="font-thin h1">500</div>
					<span class="text-muted text-xs">Total Recipients</span>					
				  </div>
				  <div class="panel padder-v item b-r col-sm-4">
					<div class="font-thin h1">Rp 15.000.000</div>
					<span class="text-muted text-xs">Total Prices Purchased</span>					
				  </div>
				  <div class="panel padder-v item b-r col-sm-4">
					<div class="font-thin h1">Rp 12.500.000</div>
					<span class="text-muted text-xs">Total Sold Prices</span>					
				  </div>
				</div>
				
			  </div>
			</div>
		</div>
	</div>

	<div class="col-lg-8" ng-controller="FormDemoCtrl">
		<div class="panel panel-default">
			<div class="panel-heading">
				Even List	
			</div>
			<div class ="panel-body" >
				<div class="table-responsive">					
					<table ui-jq="dataTable" ui-options="{
						  sAjaxSource: '<?php echo base_url()."assets/"; ?>api/datatable.json',
						  aoColumns: [
							{ mData: 'event' },
							{ mData: 'location' },
							{ mData: 'numbOfCoup' },
							{ mData: 'numbOfReceipt' },
							{ mData: 'date' },
							{ mData: 'status' }
						  ]
						}" class="table table-striped b-t b-b b-light" >
							<thead>
							  <tr>
								<th  style="width:20%">Nama Event</th>
								<th  style="width:25%">Lokasi</th>
								<th  style="width:25%">Jumlah kupon</th>
								<th  style="width:15%">Jumlah Penerima</th>
								<th  style="width:15%">Tanggal</th>
								<th  style="width:15%">Status</th>
							  </tr>
							</thead>
					</table>					  
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4" ng-controller="FormDemoCtrl">
		<div class="panel panel-default">
			<div class="panel-heading">
				Recently Closed
			</div>
			<div class="table-responsive">
				<table class="table table-striped b-t b-light">
					<thead>
					  <tr>
						<th>Nama Event</th>
						<th>Location</th>
						<th>Close Date</th>
					  </tr>
					</thead>
					<tbody>
					  <tr href="#indv1" data-toggle="collapse" data-parent="#accordion">
						<td>Pasar Murah</td>
						<td>Bandung</td>
						<td>2017, Maret 13</td>
					  </tr>
					  <tr href="#indv1" data-toggle="collapse" data-parent="#accordion">
						<td>Pasar Murah</td>
						<td>Jakarta</td>
						<td>2017, Februari 28</td>
					  </tr>
					  <tr>
						<td>Pasar Murah</td>
						<td>Jakarta</td>
						<td>2017, Februari 20</td>
					  </tr>
					  <tr>
						<td>Pasar Murah</td>
						<td>Cilacap</td>
						<td>2017, Januari 2</td>
					  </tr>
					  <tr>
						<td>Pasar Murah</td>
						<td>Cilegon</td>
						<td>2016, Desember 27</td>
					  </tr>
					  <tr>
						<td>Pasar Murah</td>
						<td>Cimahi</td>
						<td>2016, Desember 26</td>
					  </tr>
					  <tr>
						<td>Pasar Murah</td>
						<td>Kuningan</td>
						<td>2016, Desember 2</td>
					  </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
</div>
<script>
</script>