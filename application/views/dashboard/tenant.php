<div class="bg-light lter b-b wrapper-md">
    <h1 class="m-n font-thin h3"><i class="fa fa-dashboard"></i> Dashboard</h1>
</div>
<div class="col">
    <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
        <div class="row">
            <div class="col-md-12">
              <div class="row row-sm">
                  <!--script for chart-->
                <script>

              			var chartData1 = [];
              			var chartData2 = [];
              			var chartData3 = [];
              			var chartData4 = [];

              			generateChartData();

              			function generateChartData() {
              				var firstDate = new Date();
              				firstDate.setDate(firstDate.getDate() - 500);
              				firstDate.setHours(0, 0, 0, 0);

              				for (var i = 0; i < 500; i++) {
              					var newDate = new Date(firstDate);
              					newDate.setDate(newDate.getDate() + i);

              					var a1 = Math.round(Math.random() * (40 + i)) + 100 + i;
              					var b1 = Math.round(Math.random() * (1000 + i)) + 500 + i * 2;

              					var a2 = Math.round(Math.random() * (100 + i)) + 200 + i;
              					var b2 = Math.round(Math.random() * (1000 + i)) + 600 + i * 2;

              					chartData1.push({
              						date: newDate,
              						value: a1,
              						volume: b1
              					});
              					chartData2.push({
              						date: newDate,
              						value: a2,
              						volume: b2
              					});
              				}
              			}

              			AmCharts.makeChart("chartdiv", {
              				type: "stock",
                      categoryAxesSetting: {
                        maxSeries:1,
                        minPeriod:"DD"
                      },
              				dataSets: [{
              					title: "Transaksi",
              					fieldMappings: [{
              						fromField: "value",
              						toField: "value"
              					}, {
              						fromField: "volume",
              						toField: "volume"
              					}],
              					dataProvider: chartData1,
              					categoryField: "date"
              				},

              				{
              					title: "Transaksi sebelumnya",
              					compared:true,
              					fieldMappings: [{
              						fromField: "value",
              						toField: "value"
              					}, {
              						fromField: "volume",
              						toField: "volume"
              					}],
              					dataProvider: chartData2,
              					categoryField: "date"
              				}],

              				panels: [{
              					title: "Value",
              					percentHeight: 70,
              					stockGraphs: [{
              						id: "g1",
                          type: "smoothedLine",
              						valueField: "value",
              						comparable: true,
              						compareField: "value",
              						bullet: "round",
              						bulletBorderColor: "#FFFFFF",
              						bulletBorderAlpha: 1,
              						balloonText: "[[title]]:<b>[[value]]</b>",
              						compareGraphBalloonText: "[[title]]:<b>[[value]]</b>",
              						compareGraphBullet: "round",
              						compareGraphBulletBorderColor: "#FFFFFF",
              						compareGraphBulletBorderAlpha: 1
              					}],

              					stockLegend: {
              						periodValueTextComparing: "[[percents.value.close]]%",
              						periodValueTextRegular: "[[value.close]]"
              					}
              				}],

              				chartScrollbarSettings: {
              					graph: "g1",
              					updateOnReleaseOnly:false,
                        enabled: false
              				},

              				chartCursorSettings: {
              					valueBalloonsEnabled: true,
              					valueLineEnabled:true,
              					valueLineBalloonEnabled:true,
                        zoomable:true
              				},

              				periodSelector: {
              					position: "top",
                        inputFieldsEnabled: false,
              					periods: [{
              						period: "DD",
              						count: 7,
              						label: "1 minggu terakhir"
              					}, {
              						period: "MM",
              						selected: true,
              						count: 1,
              						label: "1 bulan terakhir"
              					}, {
              						period: "YYYY",
              						count: 1,
              						label: "1 tahun terakhir"
              					}]
              				}
              			});
              		</script>
                <div class="col-xs-12">
                    <div class="panel padder-v item">
                      <div class="panel header" style="padding-left:3%">
                        <h3>Grafik Transaksi</h3>
                      </div>
                        <div id="chartdiv" class="stock-main"></div>
                    </div>

                </div>
                <div class="col-xs-3 text-center">
                  <div class="block panel padder-v bg-danger item">
                    <div class="h4">Transaksi</div>
                    <div class="h1 text-white font-bold"><?php echo $dashboard[0]->total_transaksi ?></div>
                  </div>
                </div>
                <div class="col-xs-3 text-center">
                  <div class="block panel padder-v bg-primary item">
                    <div class="h4">Total Penjualan</div>
                    <span class="text-white font-bold h1 block">Rp. <?php echo number_format($dashboard[0]->total_penjualan , 0, ',', '.');?></span>
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="block panel padder-v item">
                    <div class="h4 text-center">Grafik menu terjual</div>
                    <script>
                        var chart = AmCharts.makeChart( "chart_mini", {
                        "type": "serial",
                        "dataProvider": [ {
                          "menu": "nasi goreng",
                          "terjual": 500
                        }, {
                          "menu": "ayam bakar",
                          "terjual": 475
                        }, {
                          "menu": "sate padang",
                          "terjual": 450
                        }, {
                          "menu": "ketoprak",
                          "terjual": 425
                        }, {
                          "menu": "bubur ayam",
                          "terjual": 400
                        }, {
                          "menu": "mie goreng",
                          "terjual": 375
                        }, {
                          "menu": "mie rebus",
                          "terjual": 350
                        }, {
                          "menu": "sate kambing",
                          "terjual": 325
                        }, {
                          "menu": "kari ayam",
                          "terjual": 300
                        }, {
                          "menu": "mie ayam",
                          "terjual": 275
                        }],
                        "valueAxes": [ {
                          "gridColor": "#FFFFFF",
                          "gridAlpha": 0.2,
                          "dashLength": 0
                        },
                       ],
                        "gridAboveGraphs": true,
                        "startDuration": 1,
                        "graphs": [ {
                          "balloonText": "[[menu]]: <b>[[value]]</b>",
                          "fillAlphas": 0.8,
                          "lineAlpha": 0.2,
                          "type": "column",
                          "valueField": "terjual"
                        } ],
                        "chartCursor": {
                          "categoryBalloonEnabled": false,
                          "cursorAlpha": 0,
                          "zoomable": false
                        },

                        "categoryField": "menu",
                        "categoryAxis": {
                          "labelsEnabled": false,
                        }
                        } );
                    </script>
                    <div class="row">
                      <div class="col-xs-8 chart-mini" id="chart_mini"></div>
                      <div class="col-xs-4" style="padding-top:4%">
                          <p>1. nasi goreng</p>
                          <p>2. ayam bakar</p>
                          <p>3. sate padang</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
