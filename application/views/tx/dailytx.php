<style>
	.printonly{
		display: none;
	}
	@media print { 
		.hideprint { 
		display:none;
		} 
		.dataTables_length  { 
		display:none;
		} 
		.input-group{
			width: 40%;
		}
		.dataTables_filter, .pagination ,.app-footer,.app-header,.navbar,.app-aside { 
		display:none;
		} 
		.control-label {
			width:100px;display:inline-block;
		}
		.printonly{
			display: block;
		}
		.judulreport{
			text-align: right;
		}
	} 
	.control-label{
		width: 80px;
	}
</style>
<div class="bg-light lter b-b wrapper-md">
	
	
		<div class="pull-right">
			<a id="btn-download" class="btn btn-info" href="Hasilorder?<?php echo $_SERVER['QUERY_STRING'] ?>&download=yes" >
				<i class="fa fa-cloud-download"></i>
				Download
			</a>
		</div>
		<table width="100%">
			<tr>
				<td class="printonly">
					<img src="<?php echo assets_url('img/logomag.png'); ?>" width="100px">
				</td>
				<td class="judulreport">
					<h1 class="m-n font-thin h3">
						<i class="fa fa-shopping-cart"></i> Transaksi belum dibayar
					</h1>
				</td>
			</tr>
		</table>
		
		
	
</div>
<div class="col">
	<div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div id="table-filter" class="panel-body form-inline  ">
						<form action="">
							<table width="100%">
								<tr>
									
									<td>
										Karyawan
									</td>
									<td>
										<select id="noAGP" name="noAGP" class="form-control" >
                                        <option value="" >ALL</option>
											<?php foreach($karyawans as $karyawan):?>

													<option value="<?php echo $karyawan->noAGP; ?>" ><?php echo $karyawan->nama_lengkap ?></option>
											
											<?php endforeach; ?>
										
										</select>
									</td>
									<td>
										&nbsp;
									</td>
									<td>
										&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										Tanggal
									</td>
									<td>
										
										<div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" >
											<input type="text" name="tanggal" id="tanggal" value="<?php echo $data['tanggal'] ?>" class="form-control" placeholder="DD/MM/YYYY">
											<div class="input-group-addon">
												<span class="fa fa-calendar"></span>
											</div>
										</div>
									</td>
									
								</tr>
								<tr>
									<td colspan='4' ><input type="submit"> </td>
								</tr>
								
							</table>
						</form>	
					</div>
					<div class="panel-body form-inline">
					<div class="table-responsive">
						<table id="dataTable_orders" class="nowrap table-bordered table table-striped " style="width:100%" >
							<thead>
								<tr>
                                    <th style="width: 0%">Nama Lengkap</th>
									<th style="width: 0%">Kode Order</th>
									<th style="width: 0%">Tanggal</th>	
									<th style="width: 0%">Tenant</th>
									<th style="width: 0%">Menu</th>
									<th style="width: 1%">Jumlah</th>
									<th  style="width: 10%">Harga Satuan</th>
									<th  style="width: 10%">Harga</th>

								</tr>
							</thead>
							<tbody>
								
                                <?php 
                                $ix=0;
                                foreach($data['datas'] as $index => $row): 			
                                    $ix++;		
									?>
									<tr class="<?php echo ($index % 2 == 0 ? '' : 'odd') ?>">
                                        <td><?php echo $row->nama_lengkap ?></td>
                                        <td><?php echo $row->kode_order ?>
                                            <br><a href="Dailytx/updatestatus?kodeorder=<?php echo $row->kode_order; ?>">Bayar</a>
                                        </td>
										<td><?php echo $row->create_at ?></td>
										<td><?php echo $row->nama_tenant ?></td>
										<td><?php echo $row->nama_menu ?></td>
										<td><?php echo $row->jumlah_pesanan ?></td>
										<td align="right"><?php echo number_format($row->total_harga_menu/$row->jumlah_pesanan, 0, ',', '.') ?></td>

										<td style="text-align: right"><?php echo number_format(($row->total_harga_menu) , 0, ',', '.') ?></td>
										
										
									</tr>
								
								<?php endforeach; ?>
							</tbody>
							
						</table>
						
					
					
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<script>
	$(document).ready(function(){
        $('#btn-download').each(function(){ 
			newhref= urlParams.toString();
			newhref=newhref +escape("&download=yes");
            $(this).attr("href", newhref ); // Set herf value
        });
    });
</script>
