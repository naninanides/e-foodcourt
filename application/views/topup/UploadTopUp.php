<script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
<script>
  $(document).ready(function(){
    // Sembunyikan alert validasi kosong
    $("#kosong").hide();
  });
  </script>

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">   
    <i class="fa fa-building"></i> Upload TopUp
  </h1>
</div>

<div class="col">
  <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
  <?php echo messages();?>  
  <div class="row">
      <div class="col-lg-12">
        <div class="modal-content">
          

          <!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
          <form method="post" action="" enctype="multipart/form-data" class="form-horizontal">
            <!-- 
            -- Buat sebuah input type file
            -- class pull-left berfungsi agar file input berada di sebelah kiri
            -->
            <div class="modal-body">
              <a href="<?php echo base_url("excel/format.xlsx"); ?>">Download Format</a>
              <input type="file" name="file" class="form-control"><input type="submit" name="preview" value="Preview" class="btn btn-primary">
              
              <!--
              -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
              -->
              
            </div>
          </form>

          <?php
            if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
              if(isset($upload_error)){ // Jika proses upload gagal
                echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
              }else{
                // Buat sebuah tag form untuk proses import data ke database
              echo "<form method='post' action='".site_url("topup/topup/import")."' enctype='multipart/form-data'>";
              
              // Buat sebuah div untuk alert validasi kosong
              echo "<div style='color: red;' id='kosong'>
              Ada <span id='jumlah_kosong'></span> data yang belum diisi.
              </div>";
              
              echo "<table class='display table table-striped' id='table1' >
              <thead>
              <tr>
                <th colspan='6'>Preview Data</th>
              </tr>
              <tr>
                <th>No AGP</th>
                <th>NIK</th>
                <th>Nama Depan</th>
                <th>Nama Tengah</th>
                <th>Nama Belakang</th>
                <th>Nominal</th>
              </tr>
              </thead>
              ";
              
              $numrow = 1;
              $kosong = 0;
              
              // Lakukan perulangan dari data yang ada di excel
              // $sheet adalah variabel yang dikirim dari controller
              foreach($sheet as $row){ 
                // Ambil data pada excel sesuai Kolom
                $no_agp = $row['A']; 
                $nik = $row['B']; 
                $nama_depan = $row['C']; 
                $nama_tengah = $row['D']; 
                $nama_keluarga = $row['E']; 
                $nominal = $row['F']; 
                
                // Cek jika semua data tidak diisi
                if(empty($no_agp) && empty($nama_depan) && empty($nama_tengah) && empty($nama_keluarga)
                && empty($nominal))
                  continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
                
                // Cek $numrow apakah lebih dari 1
                // Artinya karena baris pertama adalah nama-nama kolom
                // Jadi dilewat saja, tidak usah diimport
                if($numrow > 1){
                  // Validasi apakah semua data telah diisi
                  // Jika kosong, beri warna merah
                  $td_no_agp = ( ! empty($no_agp))? "" : " style='background: #E07171;'"; 
                  $td_nik = ( ! empty($nik))? "" : " style='background: #E07171;'"; 
                  $td_nama_depan = ( ! empty($nama_depan))? "" : " style='background: #E07171;'";
                  //$td_nama_tengah = ( ! empty($nama_tengah))? "" : " style='background: #E07171;'";
                  $td_nama_tengah = "";
                  //$td_nama_keluarga = ( ! empty($nama_keluarga))? "" : " style='background: #E07171;'";
                  $td_nama_keluarga="";
                  $td_nominal = ( ! empty($nominal))? "" : " style='background: #E07171;'";

                  
                  // Jika salah satu data ada yang kosong
                  if((empty($no_agp)) or empty($nama_depan) or (empty($nominal)and is_numeric($nominal))){
                    $kosong++; // Tambah 1 variabel $kosong
                  }
                  
                  echo "<tbody><tr>";
                  echo "<td".$td_no_agp.">".$no_agp."</td>";
                  echo "<td".$td_nik.">".$nik."</td>";
                  
                  echo "<td".$td_nama_depan.">".$nama_depan."</td>";
                  echo "<td".$td_nama_tengah.">".$nama_tengah."</td>";
                  echo "<td".$td_nama_keluarga.">".$nama_keluarga."</td>";
                  echo "<td".$td_nominal.">".$nominal."</td>";
                  echo "</tr></tbody>";
                }
                
                $numrow++; // Tambah 1 setiap kali looping
              }
              
              echo "</table>";
              
              // Cek apakah variabel kosong lebih dari 0
              // Jika lebih dari 0, berarti ada data yang masih kosong
              if($kosong > 0){
              ?>  
                <script>
                $(document).ready(function(){
                  // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                  $("#jumlah_kosong").html('<?php echo $kosong; ?>');
                  
                  $("#kosong").show(); // Munculkan alert validasi kosong
                });
                </script>
              <?php
              }else{ // Jika semua data sudah diisi
                echo "<hr>";
                
                // Buat sebuah tombol untuk mengimport data ke database
                echo "<button type='submit' name='import' value='import'>Import</button>";
                echo "<a href='".base_url("topup/TopUp")."'>Cancel</a>";
              }
              
              echo "</form>";
              }
              
              
            }
            ?>

        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
    $('#table1').DataTable( {
        "order": [[ 3, "desc" ]]
    } );
} );
</script>