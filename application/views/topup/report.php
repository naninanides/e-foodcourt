
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">
    
    <i class="fa fa-building"></i> Report Topup Kasir
  </h1>
</div>
<div class="col">
  <div class="wrapper-md" ng-controller="FlotChartDemoCtrl">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
            <div id="table-filter" class="panel-body form-inline">
                    <label class="control-label m-r-xs">Tipe</label>
                    <select id="filter-tipe" name="filter-tipe" class="form-control">
                        <option value="">(Semua)</option>
                        <option value="1">Voucher</option>
                        <option value="2">Regular</option>
			<option value="5">Perbaikan Data</option>
                    </select>
                    <label class="control-label m-r-xs m-l-md">Karyawan</label>
                    <select id="filter-noagp" name="filter-noagp" class="form-control" style="width: 200px">
                        
                        <option value="">(Semua Karyawan)</option>
                        <?php foreach($karyawans as $karyawan):?>
                            <option value="<?php echo $karyawan->noAGP; ?>"><?php echo $karyawan->nama_lengkap ?></option>
                        <?php endforeach;?>
                    </select>
                    <label class="control-label m-r-xs m-l-md">Tanggal</label>
                    <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" style="width: 170px;">
                        <input type="text" name="filter-tanggal-start" id="filter-tanggal-start" value="<?php echo date('d/m/Y') ?>" class="form-control" placeholder="DD/MM/YYYY">
                        <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </div>
                    </div>
                    <label class="control-label m-r-sm m-l-sm">s/d</label>
                    <div class="input-group date" data-provide="datepicker" data-date-format="dd/mm/yyyy" data-date-language="id" data-date-autoclose="true" data-date-today-highlight="true" style="width: 170px;">
                        <input type="text" name="filter-tanggal-end" id="filter-tanggal-end" value="<?php echo date('d/m/Y') ?>" class="form-control" placeholder="DD/MM/YYYY">
                        <div class="input-group-addon">
                            <span class="fa fa-calendar"></span>
                        </div>
                    </div>
                </div>
          <div class="table-responsive">
            <table id="dataTable_vouchers" ui-jq="dataTable" ui-options="{
              processing: true,
              serverSide: true,
	      paging: false,
              ajax: {
                url: '<?php echo site_url('topup/ReportTopup/ajax_list') ?>',
                type: 'POST'
              },
              columns: [
              { data: 'noagp' },
              { data: 'nama_lengkap'},
              { data: 'harga',render: $.fn.dataTable.render.number( ',', '.', 0 ),className: 'text-right'  },
              { data: 'tgl_tu' },
              { data: 'kdJnstu'},
              { data: 'operator'},
             
            
              ],
              order: [[0, 'asc']],
              pageLength: 25,
              dom: 'Bfrtip',
							buttons: [
								'excel', 'pdf', 'print'
							]
			}" class="table table-striped b-t b-b">
              <thead>
                <tr>
                  <th>No AGP</th>
                  <th>Nama</th>
                  <th class="text-left">Nominal</th>
                  <th>Tanggal</th>
                  <th>Tipe Topup</th>
                  <th>Operator</th>
                 
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
         
        </div>
      </div>
    </div>
  </div>
</div>

<script>
 
  function render_noAGP(data, type, row, meta) {
    return '<a class="edit-tenant" href="#" data-id="' + row.id_tenant + '" title="Edit">' + data + '</a>';
  }

  
</script>

<script>
function table_params(params) {
	var tanggal_start = moment($('#filter-tanggal-start').val(), "DD/MM/YYYY");
	if (!tanggal_start.isValid()) {
		tanggal_start = moment();
	}
	var tanggal_end = moment($('#filter-tanggal-end').val(), "DD/MM/YYYY");
	if (!tanggal_end.isValid()) {
		tanggal_end = moment();
	}
	params.tipe = $('#filter-tipe').val();
	params.noagp = $('#filter-noagp').val();
	params.tanggal_start = tanggal_start.format('YYYY-MM-DD');
	params.tanggal_end = tanggal_end.format('YYYY-MM-DD');
	return params;
}

function render_date(data, type, row, meta) {
	if (type === 'display')
		return moment(data).format('DD/MM/YYYY HH:MM:SS');
	return data;
}
function render_number(data, type, row, meta) {
	if (type === 'display')
		return numeral(data).format('0,0');
	return data;
}

$(window).load(function() {
	$('#table-filter').on('change', function() {
    var tanggal_start = moment($('#filter-tanggal-start').val(), "DD/MM/YYYY");
    if (!tanggal_start.isValid()) {
      tanggal_start = moment();
    }
    var tanggal_end = moment($('#filter-tanggal-end').val(), "DD/MM/YYYY");
    if (!tanggal_end.isValid()) {
      tanggal_end = moment();
    }
    url="<?php echo site_url('topup/ReportTopup/ajax_list') ?>";
    tipe=$('#filter-tipe').val();
    noagp = $('#filter-noagp').val();
	  tanggal_start = tanggal_start.format('YYYY-MM-DD');
	  tanggal_end = tanggal_end.format('YYYY-MM-DD');
    var params2=url+'?tipe='+tipe+'&noagp='+noagp+'&tanggal_start='+tanggal_start+'&tanggal_end='+tanggal_end;
		var params = table_params({});
      $('#dataTable_vouchers').DataTable().ajax.url(params2).load();
		  //$('#dataTable_orders').DataTable().ajax.reload();
	}).trigger('change');
});
</script>

