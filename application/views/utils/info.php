<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3"><i class="fa fa-wrench"></i> Utils</h1>
</div>
<div class="wrapper-md">
	<h2>Server Info</h2>
	<?php echo messages(); ?>
	<section class="panel">
		<div class="panel-body">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe src="<?php echo site_url('utils/info/display_info') ?>" class="embed-responsive-item"></iframe>
			</div>
		</div>
	</section>
</div>