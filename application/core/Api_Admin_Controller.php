<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Controller for authenticate controllers.
 * 
 * @package CI-Beam
 * @category Controller
 * @author Ardi Soebrata
 */
class Api_Admin_Controller extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Crypto/aes_encryption');

		$this->output->set_content_type('application/json');

		$header = $this->input->get_request_header("Authorization", true);
		if ($header == null) {
			header('Content-Type: application/json'); 
			http_response_code(401);
			$array["result"] = "Access Not Allowed !!!";
			echo json_encode($array);
			exit;
		}
		$access_token = $this->aes_encryption->decrypt(trim(str_ireplace("Bearer","",$header)));
		
        if ($access_token == null || $access_token == "") {
			http_response_code(401);
			$array["result"] = "Access Not Allowed !!!";
			echo json_encode($array);
		}
		else {
			$session = $this->api_session_model->get_by_token($access_token);
			if ($session != null) {
				/*$expires = $session->expires;

				$datetime1 = new DateTime();
				$datetime2 = new DateTime($expires);

				if($datetime2 <= $datetime1) {
					$this->api_session_model->delete($session->id);

					$array["result"] = "Access Token expired !!!";
					echo json_encode($array);
				}*/
			}
			else {
				http_response_code(401);
				$array["result"] = "Access Not Allowed !!!";
				echo json_encode($array);
			}
		}
    }
	
	protected function get_session() {
		$header = $this->input->get_request_header("Authorization", true);
		$access_token = $this->aes_encryption->decrypt(trim(str_ireplace("Bearer","",$header)));
		$session = $this->api_session_model->get_by_token($access_token);
		
		return $session;
	}

	protected function set_output($response_code, $message) {
		$this->output->set_status_header('400',$message);
		$array["message"] = $message;
		echo json_encode($array);
	}
}
