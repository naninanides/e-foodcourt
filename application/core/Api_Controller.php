<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api_Controller extends MY_Controller {
	
	public function __construct()
	{
		parent::__construct();
		
		$this->output->set_content_type('application/json');
	}
}