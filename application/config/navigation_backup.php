<?php

/**
 * Main Navigation.
 * Primarily being used in views/layouts/admin.php
 * 
 */
$config['navigation'] = array(
	'dashboard' => array(
		'uri' => 'dashboard/index',
		'title' => 'Dashboard',
		'icon' => 'fa fa-dashboard',
	),
	'order' => array(
		'uri' => 'tenant/Hasilorder',
		'title' => 'Hasil Order',
		'icon' => 'fa fa-shopping-cart',
	),
	'reporttopup' => array(
		'uri' => 'topup/ReportTopup',
		'title' => 'Report Topup',
		'icon' => 'fa fa-arrow-circle-o-up',
	),
	'menu' => array(
		'uri' => 'setting/Settingmenu',
		'title' => 'Menu',
		'icon' => 'fa fa-cutlery'
	),
	'tenant' => array(
		'uri' => 'setting/Settingtenant',
		'title' => 'Tenant',
		'icon' => 'fa fa-building',
	),
	'fee' => array(
		'uri' => 'setting/fee',
		'title' => 'Fee',
		'icon' => 'fa fa-money'
	),
	'kategori' => array(
		'uri' => 'setting/kategori',
		'title' => 'Kategori Menu',
		'icon' => 'fa fa-tag'
	),
	'user-management' => array(
		'uri' => 'auth/user',
		'title' => 'User Management',
		'icon' => 'fa fa-user'
	),
	'karyawan' => array(
		'title' => 'Karyawan',
		'icon' => 'fa fa-users',
		'children' => array(
			'topup' => array(
				'uri' => 'topup/TopUp',
				'title' => 'Top Up'
			),
			'voucher' => array(
				'uri' => 'voucher/Voucher',
				'title' => 'Voucher Karyawan'
			),
			
		)
	),
'tx' => array(
		'title' => 'Transaksi',
		'icon' => 'fa fa-tag',
		'children' => array(

			'unpaid' => array(
				'uri' => 'transactions/Unpaid',
				'title' => 'Belum terbayar'
			)
			
			
		)
	),
	
	'acl' => array(
		'title' => 'ACL',
		'icon' => 'fa fa-unlock-alt',
		'children' => array(
			'rules' => array(
				'uri' => 'acl/rule',
				'title' => 'Rules'
			),
			'roles' => array(
				'uri' => 'acl/role',
				'title' => 'Roles'
			),
			'resources' => array(
				'uri' => 'acl/resource',
				'title' => 'Resources'
			)
		)
	),
	'utils' => array(
		'title' => 'Utils',
		'icon' => 'fa fa-wrench',
		'children' => array(
			'system_logs' => array(
				'uri' => 'utils/logs/system',
				'title' => 'System Logs'
			),
			'deploy_logs' => array(
				'uri' => 'utils/logs/deploy',
				'title' => 'Deploy Logs'
			),
			'info' => array(
				'uri' => 'utils/info',
				'title' => 'Info'
			)
		)
	)
);