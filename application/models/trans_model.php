<?php

    class trans_model extends CI_Model {

      //models

      public function get_transaksi($id_transs)
      {
        $this->db->select('*'/*'ID_MENU,NAMA_MENU,DESK_MENU,FOTO_MENU,
        HARGA_MENU,JUMLAH_ORDER'*/);
        $this->db->from('transaksi');

        if ($id_transs != '')
        {
          $where = array('id_transaksi' => $id_transs);
          $this->db->where($where);
        }
        /*else {
          $this->db->where('status_bayar',0);
        }*/
        $query = $this->db->get();
        return $query->result_array();
      }

      public function get_menu_transaksi($id_trans)
      {
        {
          $this->db->select('MENU.ID_MENU,ID_TENANT,NAMA_MENU,DESK_MENU,FOTO_MENU,
          HARGA_MENU,JUMLAH_PESANAN');
          $this->db->from('menu');
          $this->db->join('detail_transaksi','detail_transaksi.id_menu =
            menu.id_menu');
          if ($id_trans != '') {
            $this->db->where('id_transaksi',$id_trans);
          }
          $trans = $this->db->get();
          return $trans->result_array();
        }
      }
    }

 ?>
