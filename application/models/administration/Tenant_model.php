<?php
use Jenssegers\Date\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Tenant_model extends MY_Model
{
    protected $table = 'tenant';
    protected $user = 'auth_users';
    protected $id_field = 'id_tenant';
    protected $default_sort_field = 'nama_tenant';
    protected $default_sort_order = 'asc';

    public function datatable()
    {
        $this->datatables->select("id_tenant, nama_tenant, pemilik_tenant, kat_tenant, notel1_tenant, email_tenant, IF(status_tenant = '0', 'Tidak Aktif', 'Aktif') AS status_tenant, notel2_tenant,  '' AS action_edit, '' AS action_delete")
            ->from($this->table);
        return $this->datatables->generate();
    }

    public function get_user_id()
    {
        $this->db->from($this->user);
        $this->db->where('role_id' , 6);
        return $this->db->get()->result();
    }

    public function get_user_name($id)
    {
        $query = $this->db->query(
            "SELECT first_name,last_name FROM auth_users WHERE id='$id'");
        $row = $query->row();
        return $row->last_name;

    }

    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('id_tenant' , $id);
        return $this->db->get()->result();
    }

    public function get_by_uid($id)
    {
        $this->db->from($this->table);
        $this->db->where('id_tenant' , $id);
        return $this->db->get()->row();
    }

    public function get_last_id()
    {
        $query = $this->db->query(
            "SELECT id_tenant FROM tenant ORDER BY id_tenant DESC LIMIT 1");
        $row = $query->row();
        return $row->id_tenant;
    }

    public function tambah_data($data, $table)
    {
        // $imgs=$data['foto_tenant'];
        // $imgdata = file_get_contents($imgs['full_path']);
        // var_dump($data);
        // exit;
        return $this->insert($data);
    }

    public function update_tenant($data, $table,$id_tenant)
    {
        // var_dump($data);
        $this->db->where('id_tenant', $id_tenant);
        $this->db->update($table, $data); 
        return $this->db->affected_rows();
    }

    function hapus_data($id,$table)
    {
        $this->db->where('id_tenant',$id);
        $this->db->delete($table);
    }

    function get_data_pemilik()
    {
      $this->db->select('*')->from('auth_users')->order_by('first_name asc, last_name asc');
      return $this->db->get()->result();
    }

    function mylist()
    {
        $this->db->select("id_tenant, nama_tenant");
        $this->db->from('tenant')
            ->order_by('nama_tenant asc');
        return $this->db->get()->result();
    }
}
