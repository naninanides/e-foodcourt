<?php
use Jenssegers\Date\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Fee_model extends MY_Model
{
    protected $table = 'fees';
    public $form = array(
		'id' => array(
			'helper' => 'form_hidden'
		),
		'nama' => array(
			'label'	=> 'Nama',
			'rules' => 'trim|max_length[255]|required',
			'helper' => 'form_inputlabel'
		),
		'deskripsi' => array(
			'label'	=> 'Deskripsi',
			'rules' => 'trim',
			'helper' => 'form_textarealabel'
		),
	);
}