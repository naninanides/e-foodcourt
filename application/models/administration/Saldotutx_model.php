<?php
use Jenssegers\Date\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Saldotutx_model extends MY_Model
{
    protected $table = 'm_tu';
    protected $user = 'auth_users';
    protected $id_field = 'noAGP';
    protected $default_sort_field = 'nama_depan';
    protected $default_sort_order = 'asc';

    public function datatable()
    {
    
     
       /*  $this->datatables->select("vk.noAGP,vk.`nik`,vk.`nama_lengkap`
            ,(SELECT vktt.totaltopup FROM `v_karyawan_total_topup` vktt WHERE vktt.noagp=vk.`noAGP` ) AS totaltopup,
            (SELECT DISTINCT vktx.totaltx FROM `v_karyawan_total_tx` vktx WHERE vktx.no_agp=vk.`noAGP` )  AS totaltx	
        ")
        ->from('v_karyawan_list as vk')->where('LEFT(vk.noagp,2)','Q7',true);    */
        
        //$this->db->close();
        $this->datatables->from("rekapKaryawan");
        return $this->datatables->generate();
        
    }
    public function regenerateRekap($startdate,$enddate){
       
        

        $query = $this->db->query("CALL sp_rekapKaryawan('".$startdate."','".$enddate."')");
    }
    public function dt_saldokartu()
    {
        $this->datatables->select("vk.noAGP,vk.`nik`,vk.`nama_lengkap`,
        (SELECT DISTINCT vks.saldo FROM `v_karyawan_saldo` vks WHERE vks.noagp=vk.`noAGP` ) AS saldo   
        ")
        ->from('v_karyawan_list as vk')->where('LEFT(vk.noagp,2)','Q7',true);   



            return $this->datatables->generate();
    }
}
