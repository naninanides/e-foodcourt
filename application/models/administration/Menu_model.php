<?php
use Jenssegers\Date\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends MY_Model
{
    protected $table = 'menu';
    protected $id_field = 'id_menu';

    public function datatable()
    {
        $this->datatables->select($this->table . ". foto_menu,id_menu, nama_menu, desk_menu, IF(status_menu = '0', 'Tidak Tersedia', 'Tersedia') AS status_menu, harga_menu, tenant.nama_tenant AS nama_tenant")
            ->from($this->table)
            ->join('tenant', "tenant.id_tenant = $this->table.id_tenant", 'left');
        
        if (!empty($this->session->userdata('id_tenant'))) {
            $this->datatables->where("$this->table.id_tenant", $this->session->userdata('id_tenant'));
        }
        return $this->datatables->generate();
    }

    public function addNew($data)
    {
        $query = $this->insert($data);
        return $query;
    }

    public function coba(){
        $this->datatables->add_column('foto_menu', '<img src="foto_menu" width=20>', 'foto_menu');
        $this->datatables->select('id_menu,nama_menu,desk_menu,status_menu,harga_menu');
        $this->datatables->add_column('action', anchor('','Edit',array('class'=>'btn btn-danger btn-sm')), 'id_menu');
        $this->datatables->from('menu');
        return $this->datatables->generate();
    }

    public function getData()
    {
        $this->db->select('*')->from($this->table);
        return $this->db->get()->result_array();

    }

    public function update_status($id_menu) {
        $this->db->select('status_menu')->from($this->table)->where('id_menu',$id_menu);

        $status=$this->db->get()->result();
        $status=$status[0]->status_menu;
        if ($status == 1){
            $status=0;
            $this->db->query("UPDATE menu SET status_menu=0 WHERE id_menu=$id_menu");
        }
        elseif ($status == 0) {
            $status=1;
            $this->db->query("UPDATE menu SET status_menu=1 WHERE id_menu=$id_menu");
        }

        return $status;
    }

public function getDataMakanan()
    {
        $this->db->select('*')->from($this->table)->where('kategori',1);
        $this->db->get()->result_array();

    }
    public function getDataModal($id_menu)
    {
        $this->db->select('*')->from($this->table)->where('id_menu',$id_menu);
        return $this->db->get()->result();

    }

    public function dataOrder($array_tbl,$diskon,$totalPesanan,$pembayaran,$nomeja,$namapemesan)
    {
        $this->db->select_max('id_transaksi')->from('transaksi');
        $testes=$this->db->get()->result_array();
        $testes=$testes[0]["id_transaksi"];
        if($testes==null){
            $testes=1;
        }
        else{
            $testes=$testes+1;
        }
        $this->db->set('id_transaksi',$testes);
        $this->db->set('nama_cust',$namapemesan);
        $this->db->set('status_bayar','0');
        $this->db->set('biaya_pesanan',$totalPesanan);
        $this->db->set('discount',$diskon);
        $this->db->set('total_pembayaran',$pembayaran);
        $this->db->set('no_meja',$nomeja);
        $this->db->insert('transaksi');

        foreach ($array_tbl as $a) {

            $this->db->set('id_menu',(int)$a['ID']);
            $this->db->set('id_transaksi',$testes);
            $this->db->set('jumlah_pesanan',(int)$a['Qty']);
            $this->db->insert('detail_transaksi');


        }


    }



    function get_by_id($id)
    {
        $this->db->select('id_menu');
        return parent::get_by_id($id);
    }

    function tambah_data($data, $table){
        $this->db->insert($table,$data);
    }

    function edit_data($where, $table){
        return $this->db->get_where($table, $where);
    }

    function update_data($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    function hapus_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_data_tenant(){
      $this->db->select('*')->from('tenant');
      return $this->db->get()->result();
    }

    function get_data_category(){
        $this->db->select('*')->from('kategori');
        return $this->db->get()->result();
      }

    function getLastId($id_tenant){
        $this->db->select_max('id_menu')->from('menu');
        $this->db->like('id_menu' , $id_tenant,'after');
        return $this->db->get()->result();
    }

    function get_new_id($id_tenant)
    {
        $this->db->select_max('id_menu')
            ->from($this->table)
            ->like('id_menu', $id_tenant, 'after');
        
        $row = $this->db->get()->row();
        $last_id = 1;
        if ($row->id_menu) {
            $last_id = substr($row->id_menu, -4) + 1;
        }
        return $id_tenant . substr('0000' . $last_id, -4);
    }
}
