<?php
use Jenssegers\Date\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Trans_model extends MY_Model 
{
    protected $table = 'transaksi';
    protected $detTable = 'detail_transaksi';

    public function datatable($status = '', $id_tenant = '', $tanggal_start = '', $tanggal_end = '')
    {
        $this->datatables->select("id_transaksi,waktu_transaksi, IF(status_bayar = '0', 'Belum Bayar', 'Sudah Bayar') AS status_bayar, TotalPembayaran, no_meja, tenant.nama_tenant")
            ->from($this->table)
            ->join('tenant','tenant.id_tenant=transaksi.id_tenant');

        if (!empty($status)) {
            $this->datatables->where('status_bayar', $status);
        }
        if (!empty($id_tenant)) {
            $this->datatables->where("$this->table.id_tenant", $id_tenant);
        }
        if (empty($tanggal_start)) {
            $tanggal_start = date('Y-m-d');
        }
        if (empty($tanggal_end)) {
            $tanggal_end = date('Y-m-d');
        }
        $this->datatables->where("$this->table.waktu_transaksi >=", $tanggal_start . ' 00:00:00')
            ->where("$this->table.waktu_transaksi <=", $tanggal_end . ' 23:59:59');
        return $this->datatables->generate();
    }

    public function download($status = '', $id_tenant = '', $tanggal_start = '', $tanggal_end = '')
    {
        $this->db->select("id_transaksi, waktu_transaksi, IF(status_bayar = '0', 'Belum Bayar', 'Sudah Bayar') AS status_bayar, TotalPembayaran, no_meja, tenant.nama_tenant")
            ->from($this->table)
            ->join('tenant','tenant.id_tenant=transaksi.id_tenant');

        if (!empty($status)) {
            $this->db->where('status_bayar', $status);
        }
        if (!empty($id_tenant)) {
            $this->db->where("$this->table.id_tenant", $id_tenant);
        }
        if (empty($tanggal_start)) {
            $tanggal_start = date('Y-m-d');
        }
        if (empty($tanggal_end)) {
            $tanggal_end = date('Y-m-d');
        }
        $this->db->where("$this->table.waktu_transaksi >=", $tanggal_start . ' 00:00:00')
            ->where("$this->table.waktu_transaksi <=", $tanggal_end . ' 23:59:59');
        return $this->db->get()->result();
    }

    public function addNew($data)
    {
        $query = $this->insert($data); 
        return $query;
    }

    public function coba(){
        $this->datatables->add_column('foto_menu', '<img src="foto_menu" width=20>', 'foto_menu');
        $this->datatables->select('id_menu,nama_menu,desk_menu,status_menu,harga_menu');
        $this->datatables->add_column('action', anchor('','Edit',array('class'=>'btn btn-danger btn-sm')), 'id_menu');
        $this->datatables->from('menu');
        return $this->datatables->generate();
    }

    public function getData($data)
    {
        if ($data == NULL){

            $this->db->select("id_transaksi,waktu_transaksi, IF(status_bayar = '0', 'Belum Bayar', 'Sudah Bayar') AS status_bayar, TotalPembayaran, no_meja,nama_cust")->from($this->table);
            return $this->db->get()->result_array();
        }
        elseif ($data == '0'){

            $this->db->select("id_transaksi,waktu_transaksi, IF(status_bayar = '0', 'Belum Bayar', 'Sudah Bayar') AS status_bayar, total_pembayaran, no_meja,nama_cust")->from($this->table);
            $this->db->where('status_bayar',$data);
            return $this->db->get()->result_array();
        }
        elseif ($data == '1'){

            $this->db->select("id_transaksi,waktu_transaksi, IF(status_bayar = '0', 'Belum Bayar', 'Sudah Bayar') AS status_bayar, total_pembayaran, no_meja,nama_cust")->from($this->table);
            $this->db->where('status_bayar',$data);
            return $this->db->get()->result_array();
        }

        
    }
    public function getDataDetail($id_transaksi)
    {
        $this->db->select('a.jumlah_pesanan, b.nama_menu, b.harga_menu, c.nama_cust, c.waktu_transaksi, c.total_pembayaran, c.no_meja, c.status_bayar');
        $this->db->from('detail_transaksi a');
        $this->db->join('menu b', 'a.id_menu=b.id_menu');
        $this->db->join('transaksi c', 'a.id_transaksi=c.id_transaksi');
        $this->db->where('a.id_transaksi',$id_transaksi);
        


        return $this->db->get()->result_array(); 
        
    }


    public function update_status($id_menu) {
        $this->db->select('status_menu')->from('detail_transaksi')->where('id_menu',$id_menu);
     
        $status=$this->db->get()->result();
        $status=$status[0]->status_menu;
        if ($status == 1){
            $status=0;
            $this->db->query("UPDATE menu SET status_menu=0 WHERE id_menu=$id_menu");
        }
        elseif ($status == 0) {
            $status=1;
            $this->db->query("UPDATE menu SET status_menu=1 WHERE id_menu=$id_menu");
        }
        
        return $status;
    }

public function getDataMakanan()
    {
        $this->db->select('*')->from($this->table)->where('kategori',1);
        $this->db->get()->result_array(); 
        
    }
    public function getDataModal($id_menu)
    {
        $this->db->select('*')->from($this->table)->where('id_menu',$id_menu);
        return $this->db->get()->result(); 
        
    }

    public function dataOrder($array_tbl,$diskon,$totalPesanan,$pembayaran,$nomeja)
    {
        $this->db->select_max('id_transaksi')->from('transaksi');
        $testes=$this->db->get()->result_array();
        $testes=$testes[0]["id_transaksi"];
        if($testes==null){
            $testes=1;
        }
        else{
            $testes=$testes+1;
        }
        $this->db->set('id_transaksi',$testes);
        $this->db->set('status_bayar','0');
        $this->db->set('biaya_pesanan',$totalPesanan);
        $this->db->set('discount',$diskon);
        $this->db->set('total_pembayaran',$pembayaran);
        $this->db->set('no_meja',$nomeja);
        $this->db->insert('transaksi');

        foreach ($array_tbl as $a) {
         
            $this->db->set('id_menu',(int)$a['ID']);
            $this->db->set('id_transaksi',$testes);
            $this->db->set('jumlah_pesanan',(int)$a['Qty']);
            $this->db->insert('detail_transaksi');


        }

        
    }

    function get_by_id($id)
    {
        $this->db->select('id_menu');
        return parent::get_by_id($id);
    }

    function tambah_data($data, $table){
        $this->db->insert($table,$data);
    }

    function edit_data($where, $table){
        return $this->db->get_where($table, $where);
    }

    function update_data($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }
    
    function hapus_data($where,$table){
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_dt_tenant(){
        $this->db->select('tenant.id_tenant, tenant.nama_tenant')
        ->from('tenant')
        ->join('transaksi','tenant.id_tenant=transaksi.id_tenant')
        ->group_by('tenant.id_tenant');
        return $this->db->get()->result();
    }

    function filter_by_tenant()
    {
        if (!empty($this->session->userdata('id_tenant'))) {
            $this->db->where('transaksi.id_tenant', $this->session->userdata('id_tenant'));
        }
    }

    function grafik_transaksi($start, $end) 
    {
        $this->db->select('menu.nama_menu AS nama, SUM(detail_transaksi.jumlah_pesanan) AS jumlah_pesanan')
            ->from('detail_transaksi')
            ->join('menu', 'menu.id_menu = detail_transaksi.id_menu', 'left')
            ->join('transaksi', 'transaksi.kode_order = detail_transaksi.kode_order', 'left')
            ->where("transaksi.waktu_transaksi BETWEEN '$start 00:00:00' AND '$end 23:59:59'")
            ->group_by('menu.nama_menu');

        $this->filter_by_tenant();

        return $this->db->get()->result();
    }

    function grafik_transaksi_tenant($start, $end)
    {
        $this->db->select('tenant.nama_tenant AS nama, SUM(detail_transaksi.jumlah_pesanan) AS jumlah_pesanan')
            ->from('detail_transaksi')
            ->join('transaksi', 'transaksi.kode_order = detail_transaksi.kode_order', 'left')
            ->join('tenant', 'tenant.id_tenant = transaksi.id_tenant', 'left')
            ->where("transaksi.waktu_transaksi BETWEEN '$start 00:00:00' AND '$end 23:59:59'")
            ->group_by('tenant.nama_tenant');
        
        return $this->db->get()->result();
    }

    function jumlah_transaksi($start, $end)
    {
        $this->db->select('COUNT(*) AS jumlah_transaksi')
            ->from('transaksi')
            ->where("transaksi.waktu_transaksi BETWEEN '$start 00:00:00' AND '$end 23:59:59'");
        
        $this->filter_by_tenant();

        $row = $this->db->get()->row();
        if ($row) {
            return $row->jumlah_transaksi;
        } else 
            return 0;
    }

    function total_transaksi($start, $end)
    {
        $this->db->select('SUM(TotalPembayaran) AS total_transaksi')
            ->from('transaksi')
            ->where("transaksi.waktu_transaksi BETWEEN '$start 00:00:00' AND '$end 23:59:59'");

        $this->filter_by_tenant();

        $row = $this->db->get()->row();
        if ($row && ($row->total_transaksi > 0)) {
            return $row->total_transaksi;
        }
        return 0;
    }

    function grafik_2($tahun) 
    {
        $id_tenant = $this->session->userdata('id_tenant');
        $select = "SELECT ";
        for($i = 1; $i <= 12; $i++) {
            if ($i > 1) $select .= ", ";
            $bulan = substr('00' . $i, -2);
            if (!empty($id_tenant)) {
                $select .= "(SELECT COUNT(*) FROM $this->table WHERE DATE_FORMAT(waktu_transaksi, '%Y%m') = '$tahun$bulan' AND id_tenant = '$id_tenant') AS jumlah_$i, ";
                $select .= "(SELECT SUM(totalPembayaran) FROM $this->table WHERE DATE_FORMAT(waktu_transaksi, '%Y%m') = '$tahun$bulan' AND id_tenant = '$id_tenant') AS total_$i";
            } else {
                $select .= "(SELECT COUNT(*) FROM $this->table WHERE DATE_FORMAT(waktu_transaksi, '%Y%m') = '$tahun$bulan') AS jumlah_$i, ";
                $select .= "(SELECT SUM(totalPembayaran) FROM $this->table WHERE DATE_FORMAT(waktu_transaksi, '%Y%m') = '$tahun$bulan') AS total_$i";
            }
        }
        return $this->db->query($select)->row();
    }
	function get_detail_by_id_menu($id_menu){
		$this->db->where("$this->detTable.id_menu", $id_menu);
		return $this->db->get($this->detTable)->result();
	}
}