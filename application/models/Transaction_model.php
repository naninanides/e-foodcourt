<?php
use Jenssegers\Date\Date;

defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_model extends MY_Model 
{
    protected $table = 't_transactions';

    public function datatable($acc_id)
    {
        $this->datatables->select($this->table.'.*, t_accounts.acc_number')
			->from($this->table)
			->join('t_accounts', $this->table.'.acc_id = t_accounts.id')
            ->where("$this->table.acc_id", $acc_id);

        return $this->datatables->generate();
    }
}