<?php

    class dashboard_model extends CI_Model {

      //models
        public function get_total_transaksi($id_tenant)
        {
            $this->db->select_sum('jumlah_pesanan');
            $this->db->from('detail_transaksi');
            $this->db->join('menu','menu.id_menu =
              detail_transaksi.id_menu');
            if($id_tenant != '')
            {
                $this->db->where('menu.ID_TENANT', $id_tenant);
            }
            $query = $this->db->get();
            return $query->result_array();
        }

        public function get_total_penjualan($id_tenant)
        {
            $this->db->select('detail_transaksi.id_menu,nama_menu,jumlah_pesanan,harga_menu');
            $this->db->from('detail_transaksi');
            $this->db->join('menu','menu.id_menu =
              detail_transaksi.id_menu');
            if($id_tenant != '')
            {
                $this->db->where('menu.ID_TENANT', $id_tenant);
            }
            $query = $this->db->get();
            return $query->result_array();
        }

        public function get_chartSummary($id_tenant)
        {
            $this->db->select_sum('jumlah_pesanan');
            $this->db->from('detail_transaksi');
            $this->db->join('menu','menu.id_menu =
              detail_transaksi.id_menu');
            $this->db->join('transaksi','transaksi.id_transaksi =
              detail_transaksi.id');
            $this->db->where('menu.ID_TENANT', $id_tenant);
            $this->db->group_by('DAY(transaksi.created)');
            $query = $this->db->get();
            return $query->result_array();
        }

        public function get_chartMenu($id_tenant)
        {
            $this->db->select('nama_menu');
            $this->db->select_sum('jumlah_pesanan');
            $this->db->from('detail_transaksi');
            $this->db->join('menu','menu.id_menu =
              detail_transaksi.id_menu');
            if($id_tenant != '')
            {
                $this->db->where('menu.ID_TENANT', $id_tenant);
            }
            $this->db->group_by('detail_transaksi.id_menu');
            $query = $this->db->get();
            return $query->result_array();
        }
        public function get_tenant($id_tenant)
        {
            $this->db->select('id_tenant,nama_tenant,pemilik_tenant,kat_tenant
              ');
            $this->db->from('tenant');
            if($id_tenant != '')
            {
                $this->db->where('id_tenant', $id_tenant);
            }
            $query = $this->db->get();
            return $query->result_array();
        }
    }
?>
