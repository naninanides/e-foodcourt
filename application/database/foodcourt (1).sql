-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2017 at 02:38 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `foodcourt`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_resources`
--

CREATE TABLE `acl_resources` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('module','controller','action','other') NOT NULL DEFAULT 'other',
  `parent` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_resources`
--

INSERT INTO `acl_resources` (`id`, `name`, `type`, `parent`, `created`, `modified`) VALUES
(1, 'welcome', 'module', NULL, '2012-11-12 12:07:26', NULL),
(2, 'auth', 'module', NULL, '2012-11-12 04:00:23', NULL),
(3, 'auth/login', 'controller', 2, '2012-11-12 12:43:42', '2012-11-12 12:44:06'),
(4, 'auth/logout', 'controller', 2, '2012-11-12 12:43:56', NULL),
(5, 'auth/user', 'controller', 2, '2012-11-12 04:07:59', '2012-11-12 08:29:29'),
(6, 'acl', 'module', NULL, '2012-02-02 13:47:43', NULL),
(7, 'acl/resource', 'controller', 6, '2012-02-02 13:47:57', NULL),
(8, 'acl/resource/index', 'action', 7, '2012-02-02 13:48:21', NULL),
(9, 'acl/resource/add', 'action', 7, '2012-02-02 13:48:35', '2012-10-16 17:26:12'),
(10, 'acl/resource/edit', 'action', 7, '2012-02-02 13:48:50', '2012-07-09 18:44:38'),
(11, 'acl/resource/delete', 'action', 7, '2012-02-02 13:49:06', NULL),
(12, 'acl/role', 'controller', 6, '2012-07-12 17:54:16', NULL),
(13, 'acl/role/index', 'action', 12, '2012-07-12 17:55:29', NULL),
(14, 'acl/role/add', 'action', 12, '2012-07-12 17:56:00', NULL),
(15, 'acl/role/edit', 'action', 12, '2012-07-12 17:56:19', NULL),
(16, 'acl/role/delete', 'action', 12, '2012-07-12 17:56:55', NULL),
(17, 'acl/rule', 'controller', 6, '2012-07-12 17:53:04', NULL),
(18, 'acl/rule/edit', 'action', 17, '2012-07-12 17:53:25', NULL),
(19, 'utils', 'module', NULL, NULL, NULL),
(21, 'api', 'module', NULL, NULL, NULL),
(22, 'samples', 'module', NULL, NULL, NULL),
(25, 'administration', 'module', NULL, NULL, NULL),
(27, 'home', 'module', 0, NULL, NULL),
(28, 'tenant', 'module', NULL, NULL, NULL),
(29, 'dashboard', 'module', NULL, NULL, NULL),
(30, 'setting', 'module', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `acl_roles`
--

CREATE TABLE `acl_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_roles`
--

INSERT INTO `acl_roles` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Administrator', '2011-12-27 12:00:00', NULL),
(2, 'Guest', '2011-12-27 12:00:00', NULL),
(3, 'Staf', '2012-11-12 04:30:02', '2012-11-12 04:30:39'),
(4, 'Manager', '2012-11-12 04:30:24', NULL),
(5, 'Account', NULL, NULL),
(6, 'Tenant', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `acl_role_parents`
--

CREATE TABLE `acl_role_parents` (
  `role_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_role_parents`
--

INSERT INTO `acl_role_parents` (`role_id`, `parent`, `order`) VALUES
(3, 2, 0),
(4, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `acl_rules`
--

CREATE TABLE `acl_rules` (
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `access` enum('allow','deny') NOT NULL DEFAULT 'deny',
  `priviledge` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_rules`
--

INSERT INTO `acl_rules` (`role_id`, `resource_id`, `access`, `priviledge`) VALUES
(2, 1, 'allow', NULL),
(2, 3, 'allow', NULL),
(2, 4, 'allow', NULL),
(4, 2, 'allow', NULL),
(4, 5, 'allow', NULL),
(5, 3, 'allow', NULL),
(5, 4, 'allow', NULL),
(6, 1, 'allow', NULL),
(6, 2, 'allow', NULL),
(6, 3, 'deny', NULL),
(6, 4, 'deny', NULL),
(6, 25, 'allow', NULL),
(6, 28, 'allow', NULL),
(6, 29, 'allow', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_autologin`
--

CREATE TABLE `auth_autologin` (
  `user` int(11) NOT NULL,
  `series` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_autologin`
--

INSERT INTO `auth_autologin` (`user`, `series`, `key`, `created`) VALUES
(1704, '45020bb370d0e932f40385c025658e7ebfe926e66b29c91b238972ac31a668b9', 'cacf750f62a6a7bc6cd0ffd0d668c6412c9d9a00c159cb247c398bca40fe7bd2', '2016-05-10 15:16:40');

-- --------------------------------------------------------

--
-- Table structure for table `auth_users`
--

CREATE TABLE `auth_users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `registered` datetime NOT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_users`
--

INSERT INTO `auth_users` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `lang`, `registered`, `role_id`) VALUES
(1002, 'Diane', 'Murphy', 'dmurphy', 'dmurphy@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1056, 'Mary', 'Patterson', 'mpatterso', 'mpatterso@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1076, 'Jeff', 'Firrelli', 'jeff.firrelli', 'jeff.firrelli@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1088, 'William', 'Patterson', 'wpatterson', 'wpatterson@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1102, 'Gerard', 'Bondur', 'gbondur', 'gbondur@classicmodelcars.com', '$2a$08$/9GPAwtVkFug2y5yBIhmPOZWSev.Myt.ruNENXo9DT4VrqTwNBE2K', 'en', '2012-03-01 05:54:30', NULL),
(1143, 'Anthony', 'Bow', 'abow', 'abow@classicmodelcars.com', '$2a$08$w6grERmP9T3r7FOBAuxLjO0l9H05ZgFTgGUY26hA89/g/Wq.QLqye', NULL, '2012-03-01 05:54:30', NULL),
(1165, 'Leslie', 'Jennings', 'ljennings', 'ljennings@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1166, 'Leslie', 'Thompson', 'lthompson', 'lthompson@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1216, 'Steve', 'Patterson', 'spatterson', 'spatterson@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1337, 'Loui', 'Bondur', 'lbondur', 'lbondur@classicmodelcars.com', '$2a$08$tGx5NElKJIm2hkX3OwRYSOp/VZ/r.oaB2YHdK.HBCDM921rfUVAta', NULL, '2012-03-01 05:54:30', NULL),
(1370, 'Gerard', 'Hernandez', 'ghernande', 'ghernande@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1401, 'Pamela', 'Castillo', 'pcastillo', 'pcastillo@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1501, 'Larry', 'Bott', 'lbott', 'lbott@classicmodelcars.com', '$2a$08$Njus3nhJ9bX5YYGra6xRu.ldrTylOMebKHXW/Wfl0o2wMvtppY476', NULL, '2012-03-01 05:54:30', NULL),
(1504, 'Barry', 'Jones', 'bjones', 'bjones@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1611, 'Andy', 'Fixter', 'afixter', 'afixter@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1612, 'Peter', 'Marsh', 'pmarsh', 'pmarsh@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1621, 'Mami', 'Nishi', 'mnishi', 'mnishi@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1625, 'Yoshimi', 'Kato', 'ykato', 'ykato@classicmodelcars.com', '', 'en', '2012-03-01 05:54:30', 5),
(1702, 'Martin', 'Gerard', 'mgerard', 'mgerard@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL),
(1703, 'Ardi', 'Soebrata', 'ardissoebrata', 'ardissoebrata@gmail.com', '$2a$08$KZRME/RCMM.ikhJvS9IQtOD/qQcM/922akreUjQ7fgL6BanTAwsIm', 'en', '2012-03-09 12:57:48', 4),
(1704, 'Administrator', 'Tea', 'admin', 'admin@example.com', '$2a$08$dxSn4NG3GUxu3XGLr4niIuemUHBohdWdBobNsRi6WpBE.h8zHNmXO', 'id', '2012-03-15 19:23:59', 1),
(1706, 'Test', 'TestLast', 'test', 'test@test.com', 'test', 'en', '2012-11-09 10:58:34', 2),
(1707, 'John', 'Smith', 'john.smith', 'john.smith@names.com', '', NULL, '2017-04-29 00:00:00', 5),
(1708, 'Eky Wijaya', 'Susanto', 'Eky', 'eky@yahoo.com', '$2a$08$cNwjADulcqQ0fGaDSHUqTebQJKI3hVUA1W/1426PxVWZfV9tDdioO', 'id', '2017-07-27 09:45:58', 6),
(1709, 'Eky Wijaya', 'Susanto', 'ekyw', 'eky@tenant.com', '$2a$08$iVDFB9bUBc9ymzNGdJ21xuTCutjImNjxfigwnx.GiiToDcxAsHxWq', 'id', '2017-07-31 09:36:24', 6);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(10) NOT NULL,
  `branch_name` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `cmpny_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branch_id`, `branch_name`, `address`, `cmpny_id`) VALUES
(1, 'PT Jaya Abadi Tech', 'Jl. Kemerdekaan 17', 1001),
(2, 'PT JA Bank', 'Jl. Persatuan Bangsa', 1001);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_cust` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `nama_cust` varchar(50) DEFAULT NULL,
  `jk_cust` varchar(10) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id_menu` int(11) NOT NULL,
  `u_id_transaksi` varchar(50) NOT NULL,
  `jumlah_pesanan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id_menu`, `u_id_transaksi`, `jumlah_pesanan`) VALUES
(1010001, 'DLYH28E', 2),
(1010001, 'DOWT4ZG', 3),
(1010001, 'EN1SOMG', 2),
(1010002, 'DLYH28E', 1),
(1010002, 'DOWT4ZG', 2),
(1010002, 'EN1SOMG', 3),
(1010003, 'DLYH28E', 3),
(1010003, 'DOWT4ZG', 3),
(1012001, 'DLYH28E', 6),
(1012001, 'DOWT4ZG', 3),
(1012002, 'DOWT4ZG', 2);

-- --------------------------------------------------------

--
-- Table structure for table `kasir`
--

CREATE TABLE `kasir` (
  `id_kasir` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `nama_kasir` varchar(50) DEFAULT NULL,
  `jk_kasir` varchar(15) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `id_tenant` int(11) DEFAULT NULL,
  `nama_menu` varchar(50) DEFAULT NULL,
  `desk_menu` text,
  `status_menu` tinyint(1) DEFAULT NULL,
  `foto_menu` varchar(100) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `harga_menu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `u_id`, `id_tenant`, `nama_menu`, `desk_menu`, `status_menu`, `foto_menu`, `created_by`, `created`, `updated_by`, `updated`, `harga_menu`) VALUES
(1010001, '1709', 101, 'Nasi Ayam Bakar', 'Ayam bakar beserta nasi dan sambal lalap yang mantap !', 1, 'http://192.168.43.195:80/e-foodcourt/Images/Chinese/Chinese_6_600_C.jpg', 'Administrator', '2017-07-31 10:14:06', NULL, NULL, 18000),
(1010002, '1709', 101, 'Nasi Ayam Goreng', 'Nasi dengan ayam yang digoreng', 1, 'http://192.168.43.195:80/e-foodcourt/Images/Chinese/Chinese_1_600_C.jpg', 'Administrator', '2017-07-31 16:23:30', NULL, NULL, 15000),
(1010003, '1709', 101, 'Nasi Ayam Penyet', 'Ayam penyet beserta nasi dan sambal yang sedap !', 1, 'http://192.168.43.195:80/e-foodcourt/Images/Chinese/Chinese_5_600_C.jpg', 'Administrator', '2017-07-31 16:35:22', NULL, NULL, 20000),
(1012001, '1709', 101, 'Es Jeruk', 'Jus yang diberi peras', 0, 'http://192.168.43.195:80/e-foodcourt/Images/Chinese/Chinese_3_600_C.jpg', 'Administrator', '2017-07-31 16:37:02', NULL, NULL, 6500),
(1012002, '1709', 101, 'Es Teh Manis', 'Teh yang ditambah dengan gula dan es. Dapat menghilangkan dahaga dan menyegarkan pikiran kembali', 1, 'http://192.168.43.195:80/e-foodcourt/Images/Chinese/Chinese_4_600_C.jpg', 'Administrator', '2017-07-31 16:39:36', NULL, NULL, 3500);

-- --------------------------------------------------------

--
-- Table structure for table `tenant`
--

CREATE TABLE `tenant` (
  `id_tenant` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `nama_tenant` varchar(50) DEFAULT NULL,
  `pemilik_tenant` varchar(50) DEFAULT NULL,
  `kat_tenant` varchar(50) DEFAULT NULL,
  `foto_tenant` varchar(250) NOT NULL,
  `notel1_tenant` int(15) DEFAULT NULL,
  `notel2_tenant` int(15) DEFAULT NULL,
  `email_tenant` varchar(50) DEFAULT NULL,
  `sewa_tenant` date NOT NULL,
  `desk_tenant` text,
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tenant`
--

INSERT INTO `tenant` (`id_tenant`, `u_id`, `nama_tenant`, `pemilik_tenant`, `kat_tenant`, `foto_tenant`, `notel1_tenant`, `notel2_tenant`, `email_tenant`, `sewa_tenant`, `desk_tenant`, `created_by`, `created`, `updated_by`, `updated`) VALUES
(101, '1709', 'Kampung Ayam', 'Eky', 'Fast Food', 'http://192.168.43.195:80/e-foodcourt/Images/Chinese/Chinese_5_600_C.jpg', 8181818, 9292929, 'kampungayam@email.com', '2018-07-31', 'Kampungnya para ayam.', 'Admin', '2017-07-31 10:13:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `u_id_transaksi` varchar(50) NOT NULL,
  `nama_cust` varchar(20) NOT NULL,
  `notel_cust` varchar(12) NOT NULL,
  `id_kasir` int(11) DEFAULT NULL,
  `id_cust` int(11) DEFAULT NULL,
  `waktu_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_bayar` tinyint(1) DEFAULT NULL,
  `biaya_pesanan` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `total_pembayaran` int(11) NOT NULL,
  `no_meja` varchar(5) NOT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `u_id_transaksi`, `nama_cust`, `notel_cust`, `id_kasir`, `id_cust`, `waktu_transaksi`, `status_bayar`, `biaya_pesanan`, `discount`, `total_pembayaran`, `no_meja`, `created_by`, `created`, `updated_by`, `updated`) VALUES
(2, 'DLYH28E', 'James', '08527522312', NULL, NULL, '2017-09-08 02:25:06', 0, 150000, 15000, 135000, '12', NULL, NULL, NULL, NULL),
(5, 'DOWT4ZG', 'Joni', '08566666666', NULL, NULL, '2017-09-11 06:24:27', 0, 170500, 17050, 153450, '', NULL, NULL, NULL, NULL),
(1, 'EKXFYLG', 'iqbal', '08527231234', NULL, NULL, '2017-09-07 08:04:21', 0, 140000, 14000, 126000, '15', NULL, NULL, NULL, NULL),
(4, 'EN1SOMG', 'Joni', '08566666666', NULL, NULL, '2017-09-11 06:10:57', 0, 81000, 8100, 72900, '', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_accounts`
--

CREATE TABLE `t_accounts` (
  `id` int(11) NOT NULL,
  `acc_number` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `balance` decimal(11,0) NOT NULL DEFAULT '0',
  `last_transaction` datetime DEFAULT NULL,
  `last_topup` datetime DEFAULT NULL,
  `auth_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_accounts`
--

INSERT INTO `t_accounts` (`id`, `acc_number`, `name`, `balance`, `last_transaction`, `last_topup`, `auth_user_id`) VALUES
(1, '235268563', 'John Smith', '2607000', '2017-04-19 19:15:10', '2017-03-30 12:23:30', 1707);

-- --------------------------------------------------------

--
-- Table structure for table `t_transactions`
--

CREATE TABLE `t_transactions` (
  `id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `trans_date` datetime NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `debit` decimal(11,0) DEFAULT NULL,
  `credit` decimal(11,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_transactions`
--

INSERT INTO `t_transactions` (`id`, `acc_id`, `trans_date`, `vendor_id`, `description`, `debit`, `credit`) VALUES
(1, 1, '2016-04-01 11:02:23', NULL, 'Topup', '5000000', '0'),
(2, 1, '2016-05-01 00:08:28', NULL, 'UD Saefullah', '0', '344000'),
(3, 1, '2016-05-30 01:47:27', NULL, 'CV Mardhiyah Tbk', '0', '377000'),
(4, 1, '2016-06-02 08:16:21', NULL, 'CV Puspasari (Persero) Tbk', '0', '835000'),
(5, 1, '2016-06-10 03:22:36', NULL, 'PD Wahyudin Hutapea', '0', '390000'),
(6, 1, '2016-06-11 06:15:44', NULL, 'PD Maheswara Laksita', '0', '1089000'),
(7, 1, '2016-06-17 21:11:12', NULL, 'CV Puspita Yuliarti Tbk', '0', '113000'),
(8, 1, '2016-06-29 21:08:02', NULL, 'CV Hidayat Tbk', '0', '617000'),
(9, 1, '2016-07-08 18:03:06', NULL, 'UD Usada Sudiati Tbk', '0', '1016000'),
(10, 1, '2016-07-10 17:17:22', NULL, 'Topup', '5000000', '0'),
(11, 1, '2016-07-10 17:27:22', NULL, 'Perum Budiman Mandala', '0', '793000'),
(12, 1, '2016-07-13 23:15:24', NULL, 'CV Widiastuti Widiastuti (Persero) Tbk', '0', '776000'),
(13, 1, '2016-07-14 21:01:38', NULL, 'PD Samosir (Persero) Tbk', '0', '459000'),
(14, 1, '2016-07-20 22:38:15', NULL, 'UD Nasyiah Tbk', '0', '848000'),
(15, 1, '2016-07-24 02:51:42', NULL, 'PD Zulkarnain Suartini (Persero) Tbk', '0', '417000'),
(16, 1, '2016-07-31 06:44:01', NULL, 'PD Prasetya Nurdiyanti', '0', '1324000'),
(17, 1, '2016-08-08 08:46:42', NULL, 'Topup', '5000000', '0'),
(18, 1, '2016-08-08 08:56:42', NULL, 'PD Permata Maulana Tbk', '0', '797000'),
(19, 1, '2016-08-09 02:28:10', NULL, 'CV Simbolon', '0', '372000'),
(20, 1, '2016-08-16 21:24:22', NULL, 'PT Firgantoro Tarihoran', '0', '1306000'),
(21, 1, '2016-09-11 09:46:33', NULL, 'Perum Najmudin Halimah', '0', '423000'),
(22, 1, '2016-09-30 19:34:58', NULL, 'PD Laksmiwati Adriansyah (Persero) Tbk', '0', '1228000'),
(23, 1, '2016-10-01 18:42:42', NULL, 'PT Handayani Maryati', '0', '163000'),
(24, 1, '2016-10-02 03:52:49', NULL, 'Perum Maryadi (Persero) Tbk', '0', '987000'),
(25, 1, '2016-10-06 16:00:51', NULL, 'Topup', '5000000', '0'),
(26, 1, '2016-10-06 16:10:51', NULL, 'UD Habibi Santoso Tbk', '0', '1272000'),
(27, 1, '2016-10-11 21:03:23', NULL, 'UD Prasetyo Prakasa Tbk', '0', '713000'),
(28, 1, '2016-10-17 15:03:15', NULL, 'Perum Nuraini Kusmawati (Persero) Tbk', '0', '731000'),
(29, 1, '2016-10-26 17:10:02', NULL, 'PT Anggraini Nugroho (Persero) Tbk', '0', '1220000'),
(30, 1, '2016-11-21 07:21:03', NULL, 'CV Pranowo Mardhiyah (Persero) Tbk', '0', '600000'),
(31, 1, '2016-11-21 22:26:43', NULL, 'PD Nababan Riyanti (Persero) Tbk', '0', '408000'),
(32, 1, '2016-11-26 21:18:31', NULL, 'Topup', '5000000', '0'),
(33, 1, '2016-11-26 21:28:31', NULL, 'CV Jailani', '0', '552000'),
(34, 1, '2016-12-05 17:28:07', NULL, 'PD Gunarto (Persero) Tbk', '0', '126000'),
(35, 1, '2016-12-06 09:16:52', NULL, 'Perum Pertiwi Tbk', '0', '1155000'),
(36, 1, '2016-12-25 17:32:15', NULL, 'CV Dongoran Prayoga Tbk', '0', '908000'),
(37, 1, '2017-01-03 03:34:30', NULL, 'PT Rajata Prastuti', '0', '233000'),
(38, 1, '2017-01-12 05:40:10', NULL, 'Perum Usamah Hassanah Tbk', '0', '285000'),
(39, 1, '2017-01-19 07:47:10', NULL, 'CV Pranowo (Persero) Tbk', '0', '1168000'),
(40, 1, '2017-01-23 11:24:33', NULL, 'PD Irawan Maryati', '0', '853000'),
(41, 1, '2017-01-24 16:39:04', NULL, 'Topup', '5000000', '0'),
(42, 1, '2017-01-24 16:49:04', NULL, 'Perum Wibisono Kusmawati', '0', '1445000'),
(43, 1, '2017-01-28 14:41:03', NULL, 'PD Wahyudin', '0', '853000'),
(44, 1, '2017-02-01 23:29:33', NULL, 'CV Suartini', '0', '727000'),
(45, 1, '2017-02-03 12:11:19', NULL, 'Perum Nurdiyanti', '0', '1039000'),
(46, 1, '2017-02-13 16:37:10', NULL, 'UD Puspasari Novitasari', '0', '709000'),
(47, 1, '2017-02-13 20:39:26', NULL, 'Topup', '5000000', '0'),
(48, 1, '2017-02-13 20:49:26', NULL, 'Perum Mandasari Saputra (Persero) Tbk', '0', '822000'),
(49, 1, '2017-03-02 04:45:03', NULL, 'Perum Hidayat', '0', '660000'),
(50, 1, '2017-03-15 13:34:06', NULL, 'Perum Sitorus Sinaga (Persero) Tbk', '0', '241000'),
(51, 1, '2017-03-17 08:57:48', NULL, 'PT Uyainah Agustina Tbk', '0', '1035000'),
(52, 1, '2017-03-17 12:34:27', NULL, 'PT Simbolon Putra Tbk', '0', '521000'),
(53, 1, '2017-03-20 20:24:40', NULL, 'PD Prabowo Pertiwi', '0', '1072000'),
(54, 1, '2017-03-24 15:50:01', NULL, 'PD Rahayu (Persero) Tbk', '0', '728000'),
(55, 1, '2017-03-30 12:23:30', NULL, 'Topup', '5000000', '0'),
(56, 1, '2017-03-30 12:33:30', NULL, 'PD Wasita (Persero) Tbk', '0', '763000'),
(57, 1, '2017-04-13 13:27:00', NULL, 'CV Prastuti Prayoga (Persero) Tbk', '0', '1066000'),
(58, 1, '2017-04-19 19:15:10', NULL, 'UD Winarsih Nababan (Persero) Tbk', '0', '814000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_resources`
--
ALTER TABLE `acl_resources`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `acl_roles`
--
ALTER TABLE `acl_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `acl_role_parents`
--
ALTER TABLE `acl_role_parents`
  ADD PRIMARY KEY (`role_id`,`parent`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `acl_rules`
--
ALTER TABLE `acl_rules`
  ADD PRIMARY KEY (`role_id`,`resource_id`),
  ADD KEY `resource_id` (`resource_id`);

--
-- Indexes for table `auth_autologin`
--
ALTER TABLE `auth_autologin`
  ADD PRIMARY KEY (`user`,`series`);

--
-- Indexes for table `auth_users`
--
ALTER TABLE `auth_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_cust`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id_menu`,`u_id_transaksi`) USING BTREE,
  ADD KEY `FK_RELATIONSHIP_4` (`u_id_transaksi`);

--
-- Indexes for table `kasir`
--
ALTER TABLE `kasir`
  ADD PRIMARY KEY (`id_kasir`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `FK_RELATIONSHIP_5` (`id_tenant`);

--
-- Indexes for table `tenant`
--
ALTER TABLE `tenant`
  ADD PRIMARY KEY (`id_tenant`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`u_id_transaksi`),
  ADD KEY `FK_REFERENCE_4` (`id_kasir`),
  ADD KEY `FK_REFERENCE_5` (`id_cust`);

--
-- Indexes for table `t_accounts`
--
ALTER TABLE `t_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_user_id` (`auth_user_id`);

--
-- Indexes for table `t_transactions`
--
ALTER TABLE `t_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trans_date` (`trans_date`),
  ADD KEY `acc_id` (`acc_id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl_resources`
--
ALTER TABLE `acl_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `acl_roles`
--
ALTER TABLE `acl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `auth_users`
--
ALTER TABLE `auth_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1710;
--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_accounts`
--
ALTER TABLE `t_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_transactions`
--
ALTER TABLE `t_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `acl_role_parents`
--
ALTER TABLE `acl_role_parents`
  ADD CONSTRAINT `acl_role_parents_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_role_parents_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `acl_rules`
--
ALTER TABLE `acl_rules`
  ADD CONSTRAINT `acl_rules_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_rules_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `acl_resources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_users`
--
ALTER TABLE `auth_users`
  ADD CONSTRAINT `auth_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`),
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`u_id_transaksi`) REFERENCES `transaksi` (`u_id_transaksi`);

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`id_tenant`) REFERENCES `tenant` (`id_tenant`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `FK_REFERENCE_4` FOREIGN KEY (`id_kasir`) REFERENCES `kasir` (`id_kasir`),
  ADD CONSTRAINT `FK_REFERENCE_5` FOREIGN KEY (`id_cust`) REFERENCES `customer` (`id_cust`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
