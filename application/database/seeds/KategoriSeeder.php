<?php

class KategoriSeeder extends Seeder 
{
	private $table = 'kategori';

	public function run()
	{
        $this->db->truncate($this->table);
        $this->load->model('administration/kategori_model');

        $kategoris = array('Makanan', 'Minuman', 'Lain-Lain');
        foreach($kategoris as $kategori) {
            $this->kategori_model->insert(['nama' => $kategori]);
        }
	}
}
