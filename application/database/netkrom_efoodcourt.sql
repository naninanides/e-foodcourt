-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2018 at 05:21 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `netkrom_efoodcourt`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_resources`
--

CREATE TABLE `acl_resources` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('module','controller','action','other') NOT NULL DEFAULT 'other',
  `parent` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_resources`
--

INSERT INTO `acl_resources` (`id`, `name`, `type`, `parent`, `created`, `modified`) VALUES
(1, 'welcome', 'module', NULL, '2012-11-12 12:07:26', NULL),
(2, 'auth', 'module', NULL, '2012-11-12 04:00:23', NULL),
(3, 'auth/login', 'controller', 2, '2012-11-12 12:43:42', '2012-11-12 12:44:06'),
(4, 'auth/logout', 'controller', 2, '2012-11-12 12:43:56', NULL),
(5, 'auth/user', 'controller', 2, '2012-11-12 04:07:59', '2012-11-12 08:29:29'),
(6, 'acl', 'module', NULL, '2012-02-02 13:47:43', NULL),
(7, 'acl/resource', 'controller', 6, '2012-02-02 13:47:57', NULL),
(8, 'acl/resource/index', 'action', 7, '2012-02-02 13:48:21', NULL),
(9, 'acl/resource/add', 'action', 7, '2012-02-02 13:48:35', '2012-10-16 17:26:12'),
(10, 'acl/resource/edit', 'action', 7, '2012-02-02 13:48:50', '2012-07-09 18:44:38'),
(11, 'acl/resource/delete', 'action', 7, '2012-02-02 13:49:06', NULL),
(12, 'acl/role', 'controller', 6, '2012-07-12 17:54:16', NULL),
(13, 'acl/role/index', 'action', 12, '2012-07-12 17:55:29', NULL),
(14, 'acl/role/add', 'action', 12, '2012-07-12 17:56:00', NULL),
(15, 'acl/role/edit', 'action', 12, '2012-07-12 17:56:19', NULL),
(16, 'acl/role/delete', 'action', 12, '2012-07-12 17:56:55', NULL),
(17, 'acl/rule', 'controller', 6, '2012-07-12 17:53:04', NULL),
(18, 'acl/rule/edit', 'action', 17, '2012-07-12 17:53:25', NULL),
(19, 'utils', 'module', NULL, NULL, NULL),
(21, 'api', 'module', NULL, NULL, NULL),
(22, 'samples', 'module', NULL, NULL, NULL),
(25, 'administration', 'module', NULL, NULL, NULL),
(27, 'home', 'module', 0, NULL, NULL),
(29, 'dashboard', 'module', NULL, NULL, NULL),
(30, 'setting', 'module', NULL, NULL, NULL),
(32, 'setting/Settingtenant', 'module', 30, NULL, NULL),
(33, 'setting/Settingmenu_tnt', 'module', 30, NULL, NULL),
(34, 'tenant', 'module', NULL, NULL, NULL),
(35, 'tenant/Order', 'controller', 34, NULL, NULL),
(36, 'tenant/Hasilorder', 'controller', 34, NULL, NULL),
(37, 'tenant/Hasilorder_tnt', 'controller', 34, NULL, NULL),
(38, 'tenant/Laporantenant', 'controller', 34, NULL, NULL),
(39, 'setting/Settingmenu', 'controller', 30, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `acl_roles`
--

CREATE TABLE `acl_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=26 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_roles`
--

INSERT INTO `acl_roles` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Administrator', '2011-12-27 12:00:00', NULL),
(5, 'Account', NULL, NULL),
(6, 'Tenant', NULL, NULL),
(7, 'Kasir', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `acl_role_parents`
--

CREATE TABLE `acl_role_parents` (
  `role_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `acl_rules`
--

CREATE TABLE `acl_rules` (
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `access` enum('allow','deny') NOT NULL DEFAULT 'deny',
  `priviledge` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_rules`
--

INSERT INTO `acl_rules` (`role_id`, `resource_id`, `access`, `priviledge`) VALUES
(1, 32, 'allow', NULL),
(1, 35, 'deny', NULL),
(5, 3, 'allow', NULL),
(5, 4, 'allow', NULL),
(6, 1, 'allow', NULL),
(6, 2, 'allow', NULL),
(6, 3, 'deny', NULL),
(6, 4, 'deny', NULL),
(6, 21, 'allow', NULL),
(6, 25, 'allow', NULL),
(6, 29, 'allow', NULL),
(6, 35, 'deny', NULL),
(6, 36, 'allow', NULL),
(6, 39, 'allow', NULL),
(7, 2, 'allow', NULL),
(7, 3, 'allow', NULL),
(7, 4, 'allow', NULL),
(7, 5, 'allow', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `api_session`
--

CREATE TABLE `api_session` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `session_data` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `api_session`
--

INSERT INTO `api_session` (`id`, `user_id`, `access_token`, `expires`, `session_data`) VALUES
(164, 1712, '35f0a48ccf41a3331dc60ff8912f39bd', '2018-04-23 09:25:04', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"35f0a48ccf41a3331dc60ff8912f39bd\",\"_expires\":\"2018-04-23 09:25:04\",\"_issued\":\"2018-04-22 09:25:04\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(165, 1712, 'ac7448ff3334cb975fc99cf2c30bc123', '2018-04-23 09:37:32', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"ac7448ff3334cb975fc99cf2c30bc123\",\"_expires\":\"2018-04-23 09:37:32\",\"_issued\":\"2018-04-22 09:37:32\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(166, 1712, '330144cf8ed4e76587555f9fb81aaab5', '2018-04-23 09:39:59', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"330144cf8ed4e76587555f9fb81aaab5\",\"_expires\":\"2018-04-23 09:39:59\",\"_issued\":\"2018-04-22 09:39:59\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(167, 1712, '29b6ac4f84dfdac371e7631cf096c949', '2018-04-23 09:40:46', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"29b6ac4f84dfdac371e7631cf096c949\",\"_expires\":\"2018-04-23 09:40:46\",\"_issued\":\"2018-04-22 09:40:46\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(168, 1712, 'ccb1307abdfcc0be32fe9dee2a76fdfc', '2018-04-23 10:06:58', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"ccb1307abdfcc0be32fe9dee2a76fdfc\",\"_expires\":\"2018-04-23 10:06:58\",\"_issued\":\"2018-04-22 10:06:58\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(169, 1712, '329729e4534ba60a8e408c1c4a132a0c', '2018-04-23 11:17:18', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"329729e4534ba60a8e408c1c4a132a0c\",\"_expires\":\"2018-04-23 11:17:18\",\"_issued\":\"2018-04-22 11:17:18\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(170, 1712, '4be27aed2fa08ca61d676afd06a8d58f', '2018-04-23 14:03:10', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"4be27aed2fa08ca61d676afd06a8d58f\",\"_expires\":\"2018-04-23 14:03:10\",\"_issued\":\"2018-04-22 14:03:10\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(171, 1712, '7b9ef83ad822ece83dbe50d68c06f73e', '2018-04-23 14:04:33', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"7b9ef83ad822ece83dbe50d68c06f73e\",\"_expires\":\"2018-04-23 14:04:33\",\"_issued\":\"2018-04-22 14:04:33\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(172, 1712, 'dc49df3d9c9a6930a7c38ba4878a433f', '2018-04-23 14:09:29', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"dc49df3d9c9a6930a7c38ba4878a433f\",\"_expires\":\"2018-04-23 14:09:29\",\"_issued\":\"2018-04-22 14:09:29\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(173, 1712, '0f262748ace033b9689439e70bac3d93', '2018-04-23 14:10:11', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"0f262748ace033b9689439e70bac3d93\",\"_expires\":\"2018-04-23 14:10:11\",\"_issued\":\"2018-04-22 14:10:11\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(174, 1712, '4cefd79d01b5947deb425c49f7ba7f77', '2018-04-23 16:21:34', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"4cefd79d01b5947deb425c49f7ba7f77\",\"_expires\":\"2018-04-23 16:21:34\",\"_issued\":\"2018-04-22 16:21:34\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(175, 1712, '3f356d7b99230d3236a353f3fe317aa9', '2018-04-24 09:06:10', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"3f356d7b99230d3236a353f3fe317aa9\",\"_expires\":\"2018-04-24 09:06:10\",\"_issued\":\"2018-04-23 09:06:10\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(176, 1712, 'fc1cf75597fa4ab0c337a9faebff9d98', '2018-04-24 09:08:00', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"fc1cf75597fa4ab0c337a9faebff9d98\",\"_expires\":\"2018-04-24 09:08:00\",\"_issued\":\"2018-04-23 09:08:00\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(177, 1712, 'e74fddb1059fd0ea86ee34dcea2fd104', '2018-04-24 12:47:03', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"e74fddb1059fd0ea86ee34dcea2fd104\",\"_expires\":\"2018-04-24 12:47:03\",\"_issued\":\"2018-04-23 12:47:03\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(178, 1712, '37972fb324ed34d94e576cd07da0bb4a', '2018-04-24 12:48:23', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"37972fb324ed34d94e576cd07da0bb4a\",\"_expires\":\"2018-04-24 12:48:23\",\"_issued\":\"2018-04-23 12:48:23\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(179, 1712, 'c59b185a99b8e7fd50dfd8f77bc32a8b', '2018-04-24 12:50:51', '{\"user_id\":\"1712\",\"username\":\"tenant1\",\"first_name\":\"tenant1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"c59b185a99b8e7fd50dfd8f77bc32a8b\",\"_expires\":\"2018-04-24 12:50:51\",\"_issued\":\"2018-04-23 12:50:51\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(180, 1713, '2ac397e662429a99e36adcd08fbe8964', '2018-04-24 13:19:47', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"2ac397e662429a99e36adcd08fbe8964\",\"_expires\":\"2018-04-24 13:19:47\",\"_issued\":\"2018-04-23 13:19:47\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(181, 1713, '623aae32adf263f2a41fd4fcf1cf1cc1', '2018-04-24 13:22:12', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"623aae32adf263f2a41fd4fcf1cf1cc1\",\"_expires\":\"2018-04-24 13:22:12\",\"_issued\":\"2018-04-23 13:22:12\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(182, 1714, 'b9a1de83b4758dbd3f1830dd96bd3066', '2018-04-24 13:23:39', '{\"user_id\":\"1714\",\"username\":\"maranggi1\",\"first_name\":\"Sate\",\"last_name\":\"Maranggi Purwakarta\",\"role_id\":\"6\",\"access_token\":\"b9a1de83b4758dbd3f1830dd96bd3066\",\"_expires\":\"2018-04-24 13:23:39\",\"_issued\":\"2018-04-23 13:23:39\",\"expires_in\":86400,\"Id_tenant\":\"106\"}'),
(183, 1504, 'e89b399b4b7790f9a99dfef10e90509c', '2018-04-24 13:27:12', '{\"user_id\":\"1504\",\"username\":\"pawonsunda1\",\"first_name\":\"Pawon Sunda\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"e89b399b4b7790f9a99dfef10e90509c\",\"_expires\":\"2018-04-24 13:27:12\",\"_issued\":\"2018-04-23 13:27:12\",\"expires_in\":86400,\"Id_tenant\":\"102\"}'),
(184, 1720, '934791574842069c677cc664c8f2b5fd', '2018-04-24 14:19:32', '{\"user_id\":\"1720\",\"username\":\"kopi1\",\"first_name\":\"Kedai Kopi\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"934791574842069c677cc664c8f2b5fd\",\"_expires\":\"2018-04-24 14:19:32\",\"_issued\":\"2018-04-23 14:19:32\",\"expires_in\":86400,\"Id_tenant\":\"109\"}'),
(185, 1718, 'ea1d75884a546bf302899a67cf8c1256', '2018-04-24 14:21:40', '{\"user_id\":\"1718\",\"username\":\"soto1\",\"first_name\":\"Soto Gebrak\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"ea1d75884a546bf302899a67cf8c1256\",\"_expires\":\"2018-04-24 14:21:40\",\"_issued\":\"2018-04-23 14:21:40\",\"expires_in\":86400,\"Id_tenant\":\"107\"}'),
(186, 1719, 'e4700f6fd5b482a73616590ed75a310f', '2018-04-24 14:22:10', '{\"user_id\":\"1719\",\"username\":\"happyjuice1\",\"first_name\":\"Happy Juice\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"e4700f6fd5b482a73616590ed75a310f\",\"_expires\":\"2018-04-24 14:22:10\",\"_issued\":\"2018-04-23 14:22:10\",\"expires_in\":86400,\"Id_tenant\":\"108\"}'),
(187, 1713, '6eb25e0381914b847198e70e081a5bef', '2018-04-24 15:27:48', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"6eb25e0381914b847198e70e081a5bef\",\"_expires\":\"2018-04-24 15:27:48\",\"_issued\":\"2018-04-23 15:27:48\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(188, 1713, '26d05631ec06565a85d03ebbb20629aa', '2018-04-24 16:50:32', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"26d05631ec06565a85d03ebbb20629aa\",\"_expires\":\"2018-04-24 16:50:32\",\"_issued\":\"2018-04-23 16:50:32\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(189, 1713, '036da3e60b13b15a51ad5bd9cbc3ae70', '2018-04-24 20:34:10', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"036da3e60b13b15a51ad5bd9cbc3ae70\",\"_expires\":\"2018-04-24 20:34:10\",\"_issued\":\"2018-04-23 20:34:10\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(190, 1713, 'b2254897b2e45f3fd5560cfea9dac542', '2018-04-24 21:02:56', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"b2254897b2e45f3fd5560cfea9dac542\",\"_expires\":\"2018-04-24 21:02:56\",\"_issued\":\"2018-04-23 21:02:56\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(191, 1713, '20cf4f1d3188f2dcbddbd84cf99223eb', '2018-04-24 21:12:31', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"20cf4f1d3188f2dcbddbd84cf99223eb\",\"_expires\":\"2018-04-24 21:12:31\",\"_issued\":\"2018-04-23 21:12:31\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(192, 1713, '17f9509881f7c987366b3eebf46e1532', '2018-04-24 22:26:40', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"17f9509881f7c987366b3eebf46e1532\",\"_expires\":\"2018-04-24 22:26:40\",\"_issued\":\"2018-04-23 22:26:40\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(193, 1718, '619a8b6f18a6c1cfcca24b96419175ca', '2018-04-24 22:41:52', '{\"user_id\":\"1718\",\"username\":\"soto1\",\"first_name\":\"Soto Gebrak\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"619a8b6f18a6c1cfcca24b96419175ca\",\"_expires\":\"2018-04-24 22:41:52\",\"_issued\":\"2018-04-23 22:41:52\",\"expires_in\":86400,\"Id_tenant\":\"107\"}'),
(194, 1504, '540fc93eaf935561c9e448996aefe5f3', '2018-04-24 22:42:59', '{\"user_id\":\"1504\",\"username\":\"pawonsunda1\",\"first_name\":\"Pawon Sunda\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"540fc93eaf935561c9e448996aefe5f3\",\"_expires\":\"2018-04-24 22:42:59\",\"_issued\":\"2018-04-23 22:42:59\",\"expires_in\":86400,\"Id_tenant\":\"102\"}'),
(195, 1718, 'f8c2141726b81ab17bc6402dae15ede1', '2018-04-24 22:45:14', '{\"user_id\":\"1718\",\"username\":\"soto1\",\"first_name\":\"Soto Gebrak\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"f8c2141726b81ab17bc6402dae15ede1\",\"_expires\":\"2018-04-24 22:45:14\",\"_issued\":\"2018-04-23 22:45:14\",\"expires_in\":86400,\"Id_tenant\":\"107\"}'),
(196, 1718, 'e4f6dd6f8622d8ba429f41d953b5bf3d', '2018-04-24 22:46:29', '{\"user_id\":\"1718\",\"username\":\"soto1\",\"first_name\":\"Soto Gebrak\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"e4f6dd6f8622d8ba429f41d953b5bf3d\",\"_expires\":\"2018-04-24 22:46:29\",\"_issued\":\"2018-04-23 22:46:29\",\"expires_in\":86400,\"Id_tenant\":\"107\"}'),
(197, 1713, 'cf1c793c5b7e087fc08a82398c1da5a7', '2018-04-24 22:47:58', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"cf1c793c5b7e087fc08a82398c1da5a7\",\"_expires\":\"2018-04-24 22:47:58\",\"_issued\":\"2018-04-23 22:47:58\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(198, 1713, 'bf20a5e0d0f9290fc63b4084e9acfcfc', '2018-04-24 22:52:11', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"bf20a5e0d0f9290fc63b4084e9acfcfc\",\"_expires\":\"2018-04-24 22:52:11\",\"_issued\":\"2018-04-23 22:52:11\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(199, 1718, '957bfe4f646a618a3eef0098b11b1d0f', '2018-04-24 22:53:01', '{\"user_id\":\"1718\",\"username\":\"soto1\",\"first_name\":\"Soto Gebrak\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"957bfe4f646a618a3eef0098b11b1d0f\",\"_expires\":\"2018-04-24 22:53:01\",\"_issued\":\"2018-04-23 22:53:01\",\"expires_in\":86400,\"Id_tenant\":\"107\"}'),
(200, 1713, '6f12cb36e9a050d688c0b5abe230feac', '2018-04-24 22:55:02', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"6f12cb36e9a050d688c0b5abe230feac\",\"_expires\":\"2018-04-24 22:55:02\",\"_issued\":\"2018-04-23 22:55:02\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(201, 1714, 'd80e5aeb189728eafaa8b2fce8a4c238', '2018-04-24 23:01:24', '{\"user_id\":\"1714\",\"username\":\"maranggi1\",\"first_name\":\"Sate\",\"last_name\":\"Maranggi Purwakarta\",\"role_id\":\"6\",\"access_token\":\"d80e5aeb189728eafaa8b2fce8a4c238\",\"_expires\":\"2018-04-24 23:01:24\",\"_issued\":\"2018-04-23 23:01:24\",\"expires_in\":86400,\"Id_tenant\":\"106\"}'),
(202, 1714, '2c2c0aa6c292f9b02aad0e085176ea98', '2018-04-24 23:02:37', '{\"user_id\":\"1714\",\"username\":\"maranggi1\",\"first_name\":\"Sate\",\"last_name\":\"Maranggi Purwakarta\",\"role_id\":\"6\",\"access_token\":\"2c2c0aa6c292f9b02aad0e085176ea98\",\"_expires\":\"2018-04-24 23:02:37\",\"_issued\":\"2018-04-23 23:02:37\",\"expires_in\":86400,\"Id_tenant\":\"106\"}'),
(203, 1712, 'f97f8232e828bdc057f161d73489d240', '2018-04-24 23:04:26', '{\"user_id\":\"1712\",\"username\":\"kampungayam1\",\"first_name\":\"Kampung Ayam 1\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"f97f8232e828bdc057f161d73489d240\",\"_expires\":\"2018-04-24 23:04:26\",\"_issued\":\"2018-04-23 23:04:26\",\"expires_in\":86400,\"Id_tenant\":\"101\"}'),
(204, 1718, '81abe37f0ad31722c6625652bb312291', '2018-04-24 23:16:13', '{\"user_id\":\"1718\",\"username\":\"soto1\",\"first_name\":\"Soto Gebrak\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"81abe37f0ad31722c6625652bb312291\",\"_expires\":\"2018-04-24 23:16:13\",\"_issued\":\"2018-04-23 23:16:13\",\"expires_in\":86400,\"Id_tenant\":\"107\"}'),
(205, 1719, '68a9519e28d96e16fe54633fd01c7650', '2018-04-25 00:05:33', '{\"user_id\":\"1719\",\"username\":\"happyjuice1\",\"first_name\":\"Happy Juice\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"68a9519e28d96e16fe54633fd01c7650\",\"_expires\":\"2018-04-25 00:05:33\",\"_issued\":\"2018-04-24 00:05:33\",\"expires_in\":86400,\"Id_tenant\":\"108\"}'),
(206, 1714, '378ee5a1b6e8d2161d542b0c43eb70d3', '2018-04-25 00:06:06', '{\"user_id\":\"1714\",\"username\":\"maranggi1\",\"first_name\":\"Sate\",\"last_name\":\"Maranggi Purwakarta\",\"role_id\":\"6\",\"access_token\":\"378ee5a1b6e8d2161d542b0c43eb70d3\",\"_expires\":\"2018-04-25 00:06:06\",\"_issued\":\"2018-04-24 00:06:06\",\"expires_in\":86400,\"Id_tenant\":\"106\"}'),
(207, 1713, '17ded4035821b189ec4dce5acd534f3c', '2018-04-25 11:04:00', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"17ded4035821b189ec4dce5acd534f3c\",\"_expires\":\"2018-04-25 11:04:00\",\"_issued\":\"2018-04-24 11:04:00\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(208, 1723, '92a659f5f2ead3b0fb3b7c64a405b3f6', '2018-04-25 11:29:24', '{\"user_id\":\"1723\",\"username\":\"yudi1\",\"first_name\":\"Yudi\",\"last_name\":\"MT\",\"role_id\":\"6\",\"access_token\":\"92a659f5f2ead3b0fb3b7c64a405b3f6\",\"_expires\":\"2018-04-25 11:29:24\",\"_issued\":\"2018-04-24 11:29:24\",\"expires_in\":86400,\"Id_tenant\":\"111\"}'),
(209, 1719, 'd228c670ece7772fcf80a05f91d3c827', '2018-04-25 11:34:54', '{\"user_id\":\"1719\",\"username\":\"happyjuice1\",\"first_name\":\"Happy Juice\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"d228c670ece7772fcf80a05f91d3c827\",\"_expires\":\"2018-04-25 11:34:54\",\"_issued\":\"2018-04-24 11:34:54\",\"expires_in\":86400,\"Id_tenant\":\"108\"}'),
(210, 1713, 'e7b36e0d7ed5fefaac308848bd756ba6', '2018-05-03 10:04:45', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"e7b36e0d7ed5fefaac308848bd756ba6\",\"_expires\":\"2018-05-03 10:04:45\",\"_issued\":\"2018-05-02 10:04:45\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(211, 1713, '0b42f24e641c424e396546727201a580', '2018-05-03 14:15:10', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"0b42f24e641c424e396546727201a580\",\"_expires\":\"2018-05-03 14:15:10\",\"_issued\":\"2018-05-02 14:15:10\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(212, 1713, '1d0cfb6cf7196addf04b80ac27bf3cbe', '2018-05-05 16:07:40', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"1d0cfb6cf7196addf04b80ac27bf3cbe\",\"_expires\":\"2018-05-05 16:07:40\",\"_issued\":\"2018-05-04 16:07:40\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(213, 1713, 'a5f36c6e7bf6dbcae52acaa4070f1665', '2018-05-06 14:47:24', '{\"user_id\":\"1713\",\"username\":\"thaitea1\",\"first_name\":\"Thai Tea\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"a5f36c6e7bf6dbcae52acaa4070f1665\",\"_expires\":\"2018-05-06 14:47:24\",\"_issued\":\"2018-05-05 14:47:24\",\"expires_in\":86400,\"Id_tenant\":\"104\"}'),
(214, 1718, 'e48bf5521b713b8ca1cf9ca14c1ecbe3', '2018-05-06 15:12:10', '{\"user_id\":\"1718\",\"username\":\"soto1\",\"first_name\":\"Soto Gebrak\",\"last_name\":\"\",\"role_id\":\"6\",\"access_token\":\"e48bf5521b713b8ca1cf9ca14c1ecbe3\",\"_expires\":\"2018-05-06 15:12:10\",\"_issued\":\"2018-05-05 15:12:10\",\"expires_in\":86400,\"Id_tenant\":\"107\"}');

-- --------------------------------------------------------

--
-- Table structure for table `auth_autologin`
--

CREATE TABLE `auth_autologin` (
  `user` int(11) NOT NULL,
  `series` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_autologin`
--

INSERT INTO `auth_autologin` (`user`, `series`, `key`, `created`) VALUES
(1704, '45020bb370d0e932f40385c025658e7ebfe926e66b29c91b238972ac31a668b9', 'cacf750f62a6a7bc6cd0ffd0d668c6412c9d9a00c159cb247c398bca40fe7bd2', '2016-05-10 15:16:40');

-- --------------------------------------------------------

--
-- Table structure for table `auth_users`
--

CREATE TABLE `auth_users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `registered` datetime NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `id_tenant` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_users`
--

INSERT INTO `auth_users` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `lang`, `registered`, `role_id`, `id_tenant`) VALUES
(1143, 'Anthony', 'Bow', 'abow', 'abow@classicmodelcars.com', '$2a$08$w6grERmP9T3r7FOBAuxLjO0l9H05ZgFTgGUY26hA89/g/Wq.QLqye', NULL, '2012-03-01 05:54:30', NULL, 111),
(1504, 'Pawon Sunda', '', 'pawonsunda1', 'bjones@classicmodelcars.com', '$2a$08$2gCXu4o9Jr9ImjaS1VdAFOWAJjIynGHA9.VfhkozQvlkgIEhWSebC', 'id', '2012-03-01 05:54:30', 6, 102),
(1611, 'Andy', 'Fixter', 'afixter', 'afixter@classicmodelcars.com', '', NULL, '2012-03-01 05:54:30', NULL, NULL),
(1703, 'Ardi', 'Soebrata', 'ardissoebrata', 'ardissoebrata@gmail.com', '$2a$08$KZRME/RCMM.ikhJvS9IQtOD/qQcM/922akreUjQ7fgL6BanTAwsIm', 'en', '2012-03-09 12:57:48', NULL, NULL),
(1704, 'Administrator', 'Tea', 'admin', 'admin@example.com', '$2a$08$dxSn4NG3GUxu3XGLr4niIuemUHBohdWdBobNsRi6WpBE.h8zHNmXO', 'id', '2012-03-15 19:23:59', 1, NULL),
(1708, 'Eky Wijaya', 'Susanto', 'Eky', 'eky@yahoo.com', '$2a$08$cNwjADulcqQ0fGaDSHUqTebQJKI3hVUA1W/1426PxVWZfV9tDdioO', 'id', '2017-07-27 09:45:58', 6, NULL),
(1709, 'Eky Wijaya', 'Susanto', 'ekyw', 'eky@tenant.com', '$2a$08$iVDFB9bUBc9ymzNGdJ21xuTCutjImNjxfigwnx.GiiToDcxAsHxWq', 'id', '2017-07-31 09:36:24', 6, NULL),
(1710, 'admin2', '', 'admin2', 'admin2@gmail.com', '$2a$08$EY.tNRC7CrEzYvjwPzbEDu/yVjRcOYUrZPiDtpolHWUJGv3Vwr/jq', 'en', '2018-02-02 15:52:14', 1, 109),
(1711, 'Angga', 'Daniar', 'angga', 'angga@gmail.com', '$2a$08$7V8Qv2qtBWzKKCbTqB7HuuY458QE05AwZT0dyBsipnQNOFM.W97Jq', 'id', '2018-02-20 13:59:10', 7, NULL),
(1712, 'Kampung Ayam 1', '', 'kampungayam1', 'tenant1@mail.com', '$2a$08$LY1zMjT9E16L99PfG1BZleqQ0/xpK1.M0T5VELWccD/g8G17VWHp2', 'en', '2018-04-16 10:30:36', 6, 101),
(1713, 'Thai Tea', '', 'thaitea1', 'tenant2@mail.com', '$2a$08$jAaY8RWCHE8Kv/WZQHKRzeXFeMyCUfeveEk0sA5co40D/9Su7YMIy', 'id', '2018-04-22 16:20:37', 6, 104),
(1714, 'Sate', 'Maranggi Purwakarta', 'maranggi1', 'rizky@gmail.com', '$2a$08$tJGG0S0l0zW6G6n92ryVIOmt.YHIgJShVmCRoqEFsnnDRt6hkCzXO', 'id', '2018-04-23 11:34:51', 6, 106),
(1715, 'cxcx', 'zz', 'superadmin', 'idauy.mt@gmail.com', '$2a$08$KHVxyj8ts1wt4OVfYfHGLeitGsQY4H0fM81vgjzGkVPUYZFL6CEv6', 'en', '2018-04-23 11:48:10', 6, 105),
(1716, 'czxcafdwqVEWTEWBETWEAVWEGVFA', 'Nursyiam', 'adminSADADASDASDADA', 'admSSin@administrator.com', '$2a$08$o6VKT0DZAaY5QiYmd6qkHuvead/jaVOSUKj.K6xy5Uie3rrWrl0J6', 'en', '2018-04-23 11:48:58', 6, 104),
(1717, 'BRUI', 'SSS', 'BRUI', 'rinzky@gmail.com', '$2a$08$J/3Vu7/DhIdmVLTr2LmGVexcdGpirRJDAqi5JoAysKDq3wLY98pYi', 'en', '2018-04-23 11:54:08', NULL, 0),
(1718, 'Soto Gebrak', '', 'soto1', 'soto@mail.com', '$2a$08$/FYS1BMfHryChTNBd8Gx3eeWWZ4lVLn7a2kWF0HWNNDg15UaB/9hW', 'id', '2018-04-23 12:19:52', 6, 107),
(1719, 'Happy Juice', '', 'happyjuice1', 'happyjuice@mail.com', '$2a$08$dICIf41IGLJDJUoy1DVYk.6nWrk2c4Jgfvveyc5bZKfTRb/dYMrvi', 'id', '2018-04-23 14:05:05', 6, 108),
(1720, 'Kedai Kopi', '', 'kopi1', 'kopi@mail.com', '$2a$08$75hhZBrliqZ2CWTrB89vj.wzGYXlto38q1FzuiBaMlGBc6K7.xD02', 'id', '2018-04-23 14:13:33', 6, 109),
(1721, 'Ershon', '', 'ershon1', 'ershon@mail.com', '$2a$08$pHyi7kjPmcwwbWaKrL5mGOv6CGYt8FMB2LyEWk1MsIFcQEDD4xoCO', 'id', '2018-04-23 15:25:31', 6, 110),
(1722, 'admin kasir', '', 'admin1', 'admin1@mail.com', 'admin1', 'en', '2018-04-23 19:15:28', 7, 0),
(1723, 'Yudi', 'MT', 'yudi1', 'yudi@mai.com', '$2a$08$ud4FdxFjXrGCGytgW7Gfzeh4bB39pnkdYFT20HKCiLyy2qyyBVZaG', 'en', '2018-04-24 11:19:51', 6, 111);

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(10) NOT NULL,
  `branch_name` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `cmpny_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branch_id`, `branch_name`, `address`, `cmpny_id`) VALUES
(1, 'PT Jaya Abadi Tech', 'Jl. Kemerdekaan 17', 1001),
(2, 'PT JA Bank', 'Jl. Persatuan Bangsa', 1001);

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` int(11) NOT NULL,
  `card_no` varchar(255) DEFAULT NULL,
  `card_uid` varchar(255) DEFAULT NULL,
  `holder_name` varchar(255) DEFAULT NULL,
  `qrcode` int(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `card_no`, `card_uid`, `holder_name`, `qrcode`) VALUES
(1, '0000871113050001', '00708D8021B41D6B', 'Theogratia Dinovan', 123456),
(2, '0000860313050001', '00D0128121B41D6B', 'Kartu e-Foodcourt 1', 98765);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_cust` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `nama_cust` varchar(50) DEFAULT NULL,
  `jk_cust` varchar(10) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id` int(11) NOT NULL,
  `kode_order` varchar(50) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `jumlah_pesanan` int(11) DEFAULT NULL,
  `u_id_transaksi` varchar(50) DEFAULT '00708D8021B41D6B',
  `harga` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT '0',
  `total_harga_menu` int(20) DEFAULT NULL,
  `kode_tenant` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status_bayar` int(1) NOT NULL DEFAULT '0',
  `status_antar` int(1) NOT NULL DEFAULT '0',
  `no_agp` varchar(30) DEFAULT NULL,
  `no_kursi` int(10) DEFAULT NULL,
  `nama_cust` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id`, `kode_order`, `id_menu`, `jumlah_pesanan`, `u_id_transaksi`, `harga`, `discount`, `total_harga_menu`, `kode_tenant`, `create_at`, `status_bayar`, `status_antar`, `no_agp`, `no_kursi`, `nama_cust`) VALUES
(434584, '2018042210101', 1010002, 1, '2018042210101', 15000, 0, 15000, 101, '2018-04-22 02:25:53', 1, 1, '', 123, 'adam'),
(434585, '2018042210101', 1010003, 1, '2018042210101', 20000, 0, 20000, 101, '2018-04-22 02:25:53', 1, 1, '', 123, 'adam'),
(434586, '2018042210102', 1010001, 1, '2018042210102', 18000, 0, 18000, 101, '2018-04-22 02:27:34', 1, 1, '', 56, 'adam'),
(434587, '2018042210103', 1010001, 1, '2018042210103', 18000, 0, 18000, 101, '2018-04-22 03:09:39', 0, 0, '', 27, 'agp0001'),
(434588, '2018042210103', 1012005, 1, '2018042210103', 6500, 0, 6500, 101, '2018-04-22 03:09:39', 0, 0, '', 27, 'agp0001'),
(434589, '2018042210104', 1010001, 2, '2018042210104', 36000, 0, 36000, 101, '2018-04-22 04:16:22', 0, 0, '', 8388, 'djdkddjj'),
(434590, '2018042210104', 1010002, 2, '2018042210104', 30000, 0, 30000, 101, '2018-04-22 04:16:22', 0, 0, '', 8388, 'djdkddjj'),
(434591, '2018042210105', 1010001, 2, '2018042210105', 36000, 0, 36000, 101, '2018-04-22 07:15:15', 0, 0, '', 244, 'http://en.m.wikipedia.org'),
(434592, '2018042210106', 1010001, 2, '2018042210106', 36000, 0, 36000, 101, '2018-04-22 07:19:08', 0, 0, '', 567, 'http://www.qrstuff.com'),
(434593, '2018042210106', 1010002, 2, '2018042210106', 30000, 0, 30000, 101, '2018-04-22 07:19:08', 0, 0, '', 567, 'http://www.qrstuff.com'),
(434594, '2018042210106', 1010003, 3, '2018042210106', 60000, 0, 60000, 101, '2018-04-22 07:19:08', 1, 0, '', 567, 'http://www.qrstuff.com'),
(434602, '2018042310401', 1040001, 1, '2018042310401', 8000, 0, 8000, 104, '2018-04-23 14:12:44', 1, 0, '', 17, '123456'),
(434626, '2018042310402', 1040001, 1, '00708D8021B41D6B', 8000, 0, 8000, 104, '2018-04-23 15:58:31', 1, 0, '', 6, '123456'),
(434630, '2018042310701', 1060004, 1, '00708D8021B41D6B', 18000, 0, 18000, 107, '2018-04-23 16:42:49', 0, 1, '', 56, '123456'),
(434631, '2018042410801', 1060025, 1, '00708D8021B41D6B', 10000, 0, 10000, 108, '2018-04-23 17:05:49', 0, 0, '', 12, '123456'),
(434632, '2018042410601', 1060003, 1, '00708D8021B41D6B', 20000, 0, 20000, 106, '2018-04-23 17:06:17', 0, 1, '', 18, '123456'),
(434633, '2018042411101', 1060039, 2, '00708D8021B41D6B', 36000, 0, 36000, 111, '2018-04-24 04:33:39', 0, 0, '', 15, '123456'),
(434634, '2018042410802', 1060028, 1, '00708D8021B41D6B', 10000, 0, 10000, 108, '2018-04-24 04:35:51', 0, 0, '', 3, '123456'),
(434636, '2018050210401', 1060001, 1, '2018050210401', 15000, 0, 15000, 104, '2018-05-02 03:13:55', 0, 0, '', 7, '123456'),
(434637, '2018050210402', 1060008, 1, '00708D8021B41D6B', 15000, 0, 15000, 104, '2018-05-02 07:15:31', 0, 0, '', 3, '123456'),
(434638, '2018050410401', 1060038, 1, '2018050410401', 10000, 0, 10000, 104, '2018-05-04 09:08:39', 0, 0, '', 18, '123456'),
(434639, '2018050510401', 1040001, 1, '2018050510401', 8000, 0, 8000, 104, '2018-05-05 07:47:50', 0, 0, '', 8, '123456'),
(434640, '2018050510701', 1060021, 1, '2018050510701', 20000, 0, 20000, 107, '2018-05-05 08:12:28', 0, 0, '', 18, '123456'),
(434641, '2018050610401', 1060043, 1, '2018050610401', 18000, 0, 19000, 104, '2018-05-06 04:36:36', 0, 0, '', 8, '123456');

--
-- Triggers `detail_transaksi`
--
DELIMITER $$
CREATE TRIGGER `after_delete_detail` AFTER DELETE ON `detail_transaksi` FOR EACH ROW BEGIN
UPDATE transaksi SET transaksi.TotalPembayaran = (transaksi.TotalPembayaran-(SELECT SUM(old.total_harga_menu))) WHERE transaksi.u_id_transaksi = old.u_id_transaksi AND LEFT(transaksi.waktu_transaksi,10) = LEFT(old.create_at,10) AND transaksi.id_tenant = old.kode_tenant;
INSERT history_hapus_pesanan values(curdate(), old.kode_order,old.kode_tenant,old.id_menu,old.jumlah_pesanan,
old.total_harga_menu);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_total_transaksi` AFTER INSERT ON `detail_transaksi` FOR EACH ROW BEGIN
	SET @jmlh = (SELECT COUNT(*) FROM transaksi WHERE u_id_transaksi = new.u_id_transaksi AND LEFT(waktu_transaksi,10) = LEFT(new.create_at,10) AND id_tenant = new.kode_tenant);

	IF @jmlh = 0 THEN
		INSERT INTO transaksi (kode_order, no_meja, nama_cust, u_id_transaksi, id_tenant, TotalPembayaran) VALUES (new.kode_order, new.no_kursi, new.nama_cust, new.u_id_transaksi, new.kode_tenant, (SELECT SUM(new.total_harga_menu)));
	ELSE 
		UPDATE transaksi SET transaksi.TotalPembayaran = (transaksi.TotalPembayaran + (SELECT SUM(new.total_harga_menu))) WHERE transaksi.u_id_transaksi = new.u_id_transaksi AND LEFT(transaksi.waktu_transaksi,10) = LEFT(new.create_at,10) AND transaksi.id_tenant = new.kode_tenant;
	END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `after_update_detail_transaksi` AFTER UPDATE ON `detail_transaksi` FOR EACH ROW BEGIN
                SET @menu = (SELECT menu.nama_menu FROM menu WHERE id_menu = old.id_menu LIMIT 1);

                IF old.status_bayar != '1' AND new.status_bayar = '1' THEN
                                INSERT INTO notifications VALUES (NULL, old.kode_tenant, old.id, CONCAT(@menu, ', No. Order : ', old.kode_order,' Total : ', FORMAT(old.total_harga_menu,0)));
                END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `harga_tenant` BEFORE INSERT ON `detail_transaksi` FOR EACH ROW BEGIN
DECLARE harga int;
DECLARE kode_tenant int;


SET harga = (SELECT harga_menu FROM menu where id_menu=new.id_menu);
SET new.harga = new.jumlah_pesanan*harga;
SET new.total_harga_menu = (new.harga - (new.harga*new.discount));

set kode_tenant = (SELECT id_tenant from menu where id_menu = new.id_menu);
SET new.kode_tenant = kode_tenant;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi_ol`
--

CREATE TABLE `detail_transaksi_ol` (
  `id` int(11) NOT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `jumlah_pesanan` int(11) DEFAULT NULL,
  `u_id_transaksi` varchar(50) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `kode_tenant` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_transaksi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_transaksi_ol`
--

INSERT INTO `detail_transaksi_ol` (`id`, `id_menu`, `jumlah_pesanan`, `u_id_transaksi`, `harga`, `kode_tenant`, `create_at`, `id_transaksi`) VALUES
(1, 1, 1, '1', 1, 1, '2018-02-15 09:11:21', NULL),
(2, 2, 2, '222222', 2, 2, '2018-02-15 09:13:29', NULL),
(175, 1010001, 1, 'EKXFYLG', 18000, 101, '2018-02-14 10:14:23', NULL),
(176, 1010002, 1, 'EKXFYLG', 15000, 101, '2018-02-14 10:14:23', NULL),
(177, 1010001, 1, 'DLYH28E', 18000, 101, '2018-02-15 03:11:13', NULL),
(178, 1010001, 1, 'GM4IYNE', 18000, 101, '2018-02-15 04:22:45', NULL),
(179, 1010002, 1, 'EN1SOMG', 15000, 101, '2018-02-15 06:24:03', NULL),
(180, 1010002, 1, 'DOWT4ZG', 15000, 101, '2018-02-15 07:44:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history_hapus_pesanan`
--

CREATE TABLE `history_hapus_pesanan` (
  `tgl` datetime NOT NULL,
  `kode_order` varchar(45) DEFAULT NULL,
  `id_tenant` varchar(45) DEFAULT NULL,
  `id_menu` varchar(45) DEFAULT NULL,
  `jml` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `history_hapus_pesanan`
--

INSERT INTO `history_hapus_pesanan` (`tgl`, `kode_order`, `id_tenant`, `id_menu`, `jml`, `sub_total`) VALUES
('2018-04-23 00:00:00', '2018042310401', '104', '1040001', 1, 8000),
('2018-04-23 00:00:00', '2018042310402', '104', '1060001', 1, 15000),
('2018-04-23 00:00:00', '2018042310401', '104', '1060001', 1, 15000),
('2018-04-23 00:00:00', '2018042310401', '104', '1060001', 1, 15000),
('2018-04-23 00:00:00', '2018042310402', '104', '1060001', 1, 15000),
('2018-04-23 00:00:00', '2018042310402', '104', '1060001', 1, 15000),
('2018-04-23 00:00:00', '2018042310403', '104', '1040001', 1, 8000),
('2018-05-02 00:00:00', '2018050210401', '104', '1040001', 1, 8000);

-- --------------------------------------------------------

--
-- Table structure for table `kasir`
--

CREATE TABLE `kasir` (
  `id_kasir` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL COMMENT 'untuk apa?',
  `nama_kasir` varchar(50) DEFAULT NULL,
  `jk_kasir` varchar(15) DEFAULT NULL COMMENT 'untuk apa?',
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE `meja` (
  `id` int(10) NOT NULL,
  `no_meja` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `id_tenant` int(11) DEFAULT NULL,
  `nama_menu` varchar(50) DEFAULT NULL,
  `desk_menu` text,
  `kategori` int(11) NOT NULL,
  `status_menu` tinyint(1) DEFAULT NULL,
  `foto_menu` varchar(100) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `harga_menu` int(11) DEFAULT NULL,
  `discount` int(12) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `u_id`, `id_tenant`, `nama_menu`, `desk_menu`, `kategori`, `status_menu`, `foto_menu`, `created_by`, `created`, `updated_by`, `updated`, `harga_menu`, `discount`) VALUES
(1010001, '1709', 101, 'Nasi Ayam Bakar', 'Nasi dengan ayam yang dibakar', 0, 1, 'images/nasiayambakar.jpg', 'Administrator', '2017-07-31 10:14:06', NULL, NULL, 18000, NULL),
(1010002, '1709', 101, 'Nasi Ayam Goreng', 'Nasi dengan ayam yang digoreng', 0, 1, 'images/nasiayamgoreng.jpg', 'Administrator', '2017-07-31 16:23:30', NULL, NULL, 15000, NULL),
(1010003, '1709', 101, 'Nasi Ayam Penyet', 'Nasi dengan ayam yang dipenyet', 0, 1, 'images/nasiayampenyet.jpeg', 'Administrator', '2017-07-31 16:35:22', NULL, NULL, 20000, NULL),
(1012005, '1709', 101, 'Es Jeruk', 'Jus yang diberi peras', 1, 1, 'images/jusjeruk.jpg', 'Administrator', '2017-07-31 16:37:02', NULL, NULL, 6500, NULL),
(1012006, '1709', 101, 'Es Teh Manis', 'Teh yang ditambah dengan gula dan es. Dapat menghilangkan dahaga dan menyegarkan pikiran kembali', 1, 1, 'images/es.jpg', 'Administrator', '2017-07-31 16:39:36', NULL, NULL, 3500, NULL),
(1020001, '1710', 102, 'nasi liwet', 'nasi liwet enak', 1, 1, '	\r\nimages/nasiayamgoreng.jpg', 'admin', NULL, NULL, NULL, 5000, 0),
(1040001, '1713', 104, 'Thai Tea Mango', 'Teh khas thailand rasa original', 1, 1, 'images/1524450811thai tea mango.jpg', NULL, NULL, '1713', '2018-04-23 09:33:31', 8000, 0),
(1060001, '1713', 104, 'Green Tea', '', 1, 1, 'images/1524450343Minggu-26.11-Minuman-Tea-Nyot-Nyot.jpg', '1713', '2018-04-23 09:25:43', '1713', '2018-04-23 13:33:27', 15000, 0),
(1060003, '1714', 106, 'Sate Kambing Maranggi', '', 0, 1, 'images/1524460494sate maranggi menu.jpg', '1714', '2018-04-23 12:14:54', NULL, NULL, 20000, 0),
(1060004, '1718', 107, 'Soto Campur', '', 0, 1, 'images/1524460876soto.jpg', '1718', '2018-04-23 12:21:16', NULL, NULL, 18000, 0),
(1060005, '1712', 101, 'Sate Tikus Nikmat', 'Diolah dengan bahan alami dan bermutu serta disajikan dengan rempah khas eropa, menjadikan sate tikus lain dari sate lainnya', 0, 1, 'images/1524462622sate tikus.jpg', '1712', '2018-04-23 12:50:22', '1712', '2018-04-23 12:52:17', 100000, 0),
(1060006, '1714', 106, 'Sate Ayam Maranggi', '', 0, 1, 'images/1524463962113458_satemaranggicvr.jpg', '1714', '2018-04-23 13:12:42', NULL, NULL, 20000, 0),
(1060007, '1714', 106, 'Sate Kelinci Maranggi', '', 0, 1, 'images/1524464019sate-kambing-bumbu-pedas.jpg', '1714', '2018-04-23 13:13:39', NULL, NULL, 25000, 0),
(1060008, '1713', 104, 'Thai Tea Taro', '', 1, 1, 'images/15244652648641e140b789e9529752e3253c9ec8ed.jpg', '1713', '2018-04-23 13:32:59', '1713', '2018-04-23 13:34:24', 15000, 0),
(1060009, '1713', 104, 'Thai Tea Chocolate', '', 1, 1, 'images/1524465417Phoe-Tea-Thai-Tea-Milo-Coffee.jpg', '1713', '2018-04-23 13:36:57', NULL, NULL, 15000, 0),
(1060010, '1713', 104, 'Thai Tea White Milk', '', 1, 1, 'images/1524465489download.jpg', '1713', '2018-04-23 13:38:09', NULL, NULL, 15000, 0),
(1060011, '1714', 106, 'Iga Bakar', '', 0, 1, 'images/1524466227rib-steak.jpg', '1714', '2018-04-23 13:50:27', NULL, NULL, 35000, 0),
(1060012, '1714', 106, 'Soup Iga', '', 0, 1, 'images/1524466317download.jpg', '1714', '2018-04-23 13:51:57', NULL, NULL, 35000, 0),
(1060013, '1714', 106, 'Nasi Putih', '', 0, 1, 'images/1524466394mitos-nasi.jpg', '1714', '2018-04-23 13:53:14', NULL, NULL, 5000, 0),
(1060014, '1504', 102, 'Pepes Ayam', '', 0, 1, 'images/1524466669pepes ayam.jpg', '1504', '2018-04-23 13:57:49', NULL, NULL, 15000, 0),
(1060015, '1504', 102, 'Pepes Jamur', '', 0, 1, 'images/1524466687pepes jamur.jpg', '1504', '2018-04-23 13:58:07', NULL, NULL, 3000, 0),
(1060016, '1504', 102, 'Pepes Ikan Asin', '', 0, 1, 'images/1524466707pepes peda.jpg', '1504', '2018-04-23 13:58:27', NULL, NULL, 7000, 0),
(1060017, '1504', 102, 'Sambel Cibiuk', '', 0, 1, 'images/1524466722sambel cibiuk.jpg', '1504', '2018-04-23 13:58:42', NULL, NULL, 5000, 0),
(1060018, '1504', 102, 'Sambel Terasi Dadakan', '', 0, 1, 'images/1524466738sambel terasi.jpg', '1504', '2018-04-23 13:58:58', NULL, NULL, 5000, 0),
(1060019, '1504', 102, 'Tahu Goreng', '', 0, 1, 'images/1524466751tahu goreng.jpg', '1504', '2018-04-23 13:59:11', NULL, NULL, 2000, 0),
(1060020, '1504', 102, 'Tempe Goreng', '', 0, 1, 'images/1524466764tempe goreng.jpg', '1504', '2018-04-23 13:59:24', NULL, NULL, 2000, 0),
(1060021, '1718', 107, 'Soto Bandung', '', 0, 1, 'images/1524466902soto bandung.JPG', '1718', '2018-04-23 14:01:42', NULL, NULL, 20000, 0),
(1060022, '1718', 107, 'Soto Betawi', '', 0, 1, 'images/1524466918soto betawi.jpg', '1718', '2018-04-23 14:01:58', NULL, NULL, 20000, 0),
(1060023, '1718', 107, 'Soto Daging', '', 0, 1, 'images/1524466935soto daging.jpg', '1718', '2018-04-23 14:02:15', NULL, NULL, 25000, 0),
(1060024, '1718', 107, 'Soto Madura', '', 0, 1, 'images/1524466950soto madura.jpg', '1718', '2018-04-23 14:02:30', NULL, NULL, 18000, 0),
(1060025, '1719', 108, 'Jus Alpukat', '', 1, 1, 'images/1524467388jus alpukat.jpg', '1719', '2018-04-23 14:09:48', NULL, NULL, 10000, 0),
(1060026, '1719', 108, 'Jus Pisang', '', 1, 1, 'images/1524467405jus cau.jpg', '1719', '2018-04-23 14:10:05', NULL, NULL, 10000, 0),
(1060027, '1719', 108, 'jus Jambu Biji', '', 1, 1, 'images/1524467420jus jambu.jpg', '1719', '2018-04-23 14:10:20', NULL, NULL, 10000, 0),
(1060028, '1719', 108, 'Jus Jeruk', '', 1, 1, 'images/1524467432jus jeruk.jpg', '1719', '2018-04-23 14:10:32', NULL, NULL, 10000, 0),
(1060029, '1719', 108, 'Jus Melon', '', 1, 1, 'images/1524467449jus melon.jpg', '1719', '2018-04-23 14:10:49', NULL, NULL, 10000, 0),
(1060030, '1719', 108, 'Jus Mangga', '', 1, 1, 'images/1524467459jus mangga.jpg', '1719', '2018-04-23 14:10:59', NULL, NULL, 10000, 0),
(1060031, '1719', 108, 'Jus Buah Naga', '', 1, 1, 'images/1524467471jus naga.jpg', '1719', '2018-04-23 14:11:11', NULL, NULL, 10000, 0),
(1060032, '1719', 108, 'Jus Strawberry', '', 1, 1, 'images/1524467487jus strawberry.jpg', '1719', '2018-04-23 14:11:27', NULL, NULL, 10000, 0),
(1060033, '1720', 109, 'Hot Black Cofee (americano)', '', 1, 1, 'images/1524467859hot black cofee.jpg', '1720', '2018-04-23 14:17:39', NULL, NULL, 18000, 0),
(1060034, '1720', 109, 'Hot Mocachino', '', 1, 1, 'images/1524467884hot mocachino.jpg', '1720', '2018-04-23 14:18:04', NULL, NULL, 17000, 0),
(1060035, '1720', 109, 'Iced Black Cofee', '', 1, 1, 'images/1524467907iced black cofee.jpg', '1720', '2018-04-23 14:18:27', NULL, NULL, 18000, 0),
(1060036, '1720', 109, 'Iced Mocachino', '', 1, 1, 'images/1524467924iced mocachino.jpg', '1720', '2018-04-23 14:18:44', NULL, NULL, 20000, 0),
(1060037, '1720', 109, 'White Cofee', '', 1, 1, 'images/1524467945white cofee.jpg', '1720', '2018-04-23 14:19:05', NULL, NULL, 20000, 0),
(1060038, '1713', 104, 'kopi', '', 1, 1, 'images/1524472493hot black cofee.jpg', '1713', '2018-04-23 15:34:53', NULL, NULL, 10000, 0),
(1060039, '1723', 111, 'Bakso Semar Pedas', '', 0, 1, 'images/1524543848soto madura.jpg', '1723', '2018-04-24 11:24:08', NULL, NULL, 18000, 0),
(1060040, '1723', 111, 'Jus Melon', '', 1, 1, 'images/1524543972jus melon.jpg', '1723', '2018-04-24 11:26:12', NULL, NULL, 15000, 0),
(1060043, '1713', 104, 'thai tea mix', '', 1, 1, 'images/1525425273PA243829-800x608.jpg', '1713', '2018-05-04 16:14:33', NULL, NULL, 18000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu_a`
--

CREATE TABLE `menu_a` (
  `id_menu` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `id_tenant` int(11) DEFAULT NULL,
  `nama_menu` varchar(50) DEFAULT NULL,
  `desk_menu` text,
  `kategori` int(11) NOT NULL,
  `status_menu` tinyint(1) DEFAULT NULL,
  `foto_menu` varchar(100) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `harga_menu` int(11) DEFAULT NULL,
  `discount` int(12) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `menu_a`
--

INSERT INTO `menu_a` (`id_menu`, `u_id`, `id_tenant`, `nama_menu`, `desk_menu`, `kategori`, `status_menu`, `foto_menu`, `created_by`, `created`, `updated_by`, `updated`, `harga_menu`, `discount`) VALUES
(0, '1714', 106, 'daging', '', 0, 1, 'images/1524460137nusa penida.png', '1714', '2018-04-23 12:08:57', NULL, NULL, 1000, 0),
(1010001, '1709', 101, 'Nasi Ayam Bakar', 'Nasi dengan ayam yang dibakar', 0, 1, 'images/nasiayambakar.jpg', 'Administrator', '2017-07-31 10:14:06', NULL, NULL, 18000, NULL),
(1010002, '1709', 101, 'Nasi Ayam Goreng', 'Nasi dengan ayam yang digoreng', 0, 1, 'images/nasiayamgoreng.jpg', 'Administrator', '2017-07-31 16:23:30', NULL, NULL, 15000, NULL),
(1010003, '1709', 101, 'Nasi Ayam Penyet', 'Nasi dengan ayam yang dipenyet', 0, 1, 'images/nasiayampenyet.jpeg', 'Administrator', '2017-07-31 16:35:22', NULL, NULL, 20000, NULL),
(1010004, '1709', 101, 'Sate Ayam', 'sate ayam mantap', 0, 1, '', 'Administrator', '2018-02-07 14:37:17', NULL, NULL, 16000, NULL),
(1012005, '1709', 101, 'Es Jeruk', 'Jus yang diberi peras', 1, 1, 'images/jusjeruk.jpg', 'Administrator', '2017-07-31 16:37:02', NULL, NULL, 6500, NULL),
(1012006, '1709', 101, 'Es Teh Manis', 'Teh yang ditambah dengan gula dan es. Dapat menghilangkan dahaga dan menyegarkan pikiran kembali', 1, 1, 'images/es.jpg', 'Administrator', '2017-07-31 16:39:36', NULL, NULL, 3500, NULL),
(1020001, '1710', 102, 'nasi liwet', 'nasi liwet enak', 1, 1, '	\r\nimages/nasiayamgoreng.jpg', 'admin', NULL, NULL, NULL, 5000, 0),
(1040001, '1713', 104, 'Thai Tea Mango', 'Teh khas thailand rasa original', 1, 1, 'images/1524450811thai tea mango.jpg', NULL, NULL, '1713', '2018-04-23 09:33:31', 8000, 0),
(1060001, '1713', 104, 'Original Thai Tea', '', 1, 1, 'images/1524450343Minggu-26.11-Minuman-Tea-Nyot-Nyot.jpg', '1713', '2018-04-23 09:25:43', NULL, NULL, 15000, 0),
(1060002, '1713', 104, 'Sate Kambing Maranggi', '', 1, 1, 'images/1524458817sate maranggi menu.jpg', '1713', '2018-04-23 11:46:57', NULL, NULL, 23000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu_old`
--

CREATE TABLE `menu_old` (
  `id_menu` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `id_tenant` int(11) DEFAULT NULL,
  `nama_menu` varchar(50) DEFAULT NULL,
  `desk_menu` text,
  `kategori` int(11) NOT NULL,
  `status_menu` tinyint(1) DEFAULT NULL,
  `foto_menu` varchar(100) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `harga_menu` int(11) DEFAULT NULL,
  `discount` int(12) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_old`
--

INSERT INTO `menu_old` (`id_menu`, `u_id`, `id_tenant`, `nama_menu`, `desk_menu`, `kategori`, `status_menu`, `foto_menu`, `created_by`, `created`, `updated_by`, `updated`, `harga_menu`, `discount`) VALUES
(0, '1714', 106, 'daging', '', 0, 1, 'images/1524460137nusa penida.png', '1714', '2018-04-23 12:08:57', NULL, NULL, 1000, 0),
(1010001, '1709', 101, 'Nasi Ayam Bakar', 'Nasi dengan ayam yang dibakar', 0, 1, 'images/nasiayambakar.jpg', 'Administrator', '2017-07-31 10:14:06', NULL, NULL, 18000, NULL),
(1010002, '1709', 101, 'Nasi Ayam Goreng', 'Nasi dengan ayam yang digoreng', 0, 1, 'images/nasiayamgoreng.jpg', 'Administrator', '2017-07-31 16:23:30', NULL, NULL, 15000, NULL),
(1010003, '1709', 101, 'Nasi Ayam Penyet', 'Nasi dengan ayam yang dipenyet', 0, 1, 'images/nasiayampenyet.jpeg', 'Administrator', '2017-07-31 16:35:22', NULL, NULL, 20000, NULL),
(1010004, '1709', 101, 'Sate Ayam', 'sate ayam mantap', 0, 1, '', 'Administrator', '2018-02-07 14:37:17', NULL, NULL, 16000, NULL),
(1012005, '1709', 101, 'Es Jeruk', 'Jus yang diberi peras', 1, 1, 'images/jusjeruk.jpg', 'Administrator', '2017-07-31 16:37:02', NULL, NULL, 6500, NULL),
(1012006, '1709', 101, 'Es Teh Manis', 'Teh yang ditambah dengan gula dan es. Dapat menghilangkan dahaga dan menyegarkan pikiran kembali', 1, 1, 'images/es.jpg', 'Administrator', '2017-07-31 16:39:36', NULL, NULL, 3500, NULL),
(1020001, '1710', 102, 'nasi liwet', 'nasi liwet enak', 1, 1, '	\r\nimages/nasiayamgoreng.jpg', 'admin', NULL, NULL, NULL, 5000, 0),
(1040001, '1713', 104, 'Thai Tea Mango', 'Teh khas thailand rasa original', 1, 1, 'images/1524450811thai tea mango.jpg', NULL, NULL, '1713', '2018-04-23 09:33:31', 8000, 0),
(1060001, '1713', 104, 'Original Thai Tea', '', 1, 1, 'images/1524450343Minggu-26.11-Minuman-Tea-Nyot-Nyot.jpg', '1713', '2018-04-23 09:25:43', NULL, NULL, 15000, 0),
(1060002, '1713', 104, 'Sate Kambing Maranggi', '', 1, 1, 'images/1524458817sate maranggi menu.jpg', '1713', '2018-04-23 11:46:57', NULL, NULL, 23000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(20180422113124);

-- --------------------------------------------------------

--
-- Table structure for table `namafc`
--

CREATE TABLE `namafc` (
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `telp` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `namafc`
--

INSERT INTO `namafc` (`nama`, `alamat`, `telp`) VALUES
('Foodcourt Mall Artha Gading', 'Jl. Artha Gading Sel. No.1, RT.18/RW.8, Klp. Gading Bar., Klp. Gading, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14240', '021-455322');

-- --------------------------------------------------------

--
-- Table structure for table `nominal_tu`
--

CREATE TABLE `nominal_tu` (
  `id` int(11) NOT NULL,
  `nominal` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nominal_tu`
--

INSERT INTO `nominal_tu` (`id`, `nominal`) VALUES
(1, '20000'),
(2, '40000'),
(3, '500000');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `id_tenant` int(11) DEFAULT NULL,
  `id_detail_transaksi` int(11) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `id_tenant`, `id_detail_transaksi`, `message`) VALUES
(9, 101, 434594, 'Nasi Ayam Penyet, No. Order : 2018042210106 Total : 60,000');

-- --------------------------------------------------------

--
-- Table structure for table `tenant`
--

CREATE TABLE `tenant` (
  `id_tenant` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `nama_tenant` varchar(50) DEFAULT NULL,
  `pemilik_tenant` varchar(50) DEFAULT NULL,
  `kat_tenant` varchar(50) DEFAULT NULL,
  `foto_tenant` varchar(250) NOT NULL,
  `notel1_tenant` int(15) DEFAULT NULL,
  `notel2_tenant` int(15) DEFAULT NULL,
  `email_tenant` varchar(50) DEFAULT NULL,
  `sewa_tenant` date NOT NULL,
  `desk_tenant` text,
  `status_tenant` int(11) DEFAULT '1',
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tenant`
--

INSERT INTO `tenant` (`id_tenant`, `u_id`, `nama_tenant`, `pemilik_tenant`, `kat_tenant`, `foto_tenant`, `notel1_tenant`, `notel2_tenant`, `email_tenant`, `sewa_tenant`, `desk_tenant`, `status_tenant`, `created_by`, `created`, `updated_by`, `updated`) VALUES
(101, '1712', 'Kampung Ayam', '', 'Fast Food', '', 8181818, 9292929, 'kampungayam@email.com', '2018-07-31', 'Kampungnya para ayam.', 1, 'Admin', '2017-07-31 10:13:50', 'Administrator', '2018-04-22 09:17:31'),
(102, '1504', 'Pawon Sunda', '', 'Fast Food', '', 8181818, 9292929, 'kampungayam@email.com', '2018-07-31', 'Ayam Penyet Cita Rasa Tinggi', 1, 'Admin', '2017-07-31 10:13:50', 'Administrator', '2018-04-23 13:27:09'),
(104, '1713', 'Thai Tea', '', 'Drink', '', 2147483647, 0, 'thai.tea@gmail.com', '2018-03-01', '', 1, 'Administrator', '2018-03-01 11:42:13', 'Administrator', '2018-04-23 13:18:36'),
(106, '1714', 'Sate Maranggi ', 'Maranggi Purwakarta', 'food', '', 2147483647, 0, 'maranggi@mail.com', '0000-00-00', 'fgdgsds', 1, 'Administrator', '2018-04-23 11:33:53', 'Administrator', '2018-04-23 13:23:17'),
(107, '1718', 'Soto Gebrak', '', 'Food', '', 0, 0, 'soto@mail.com', '0000-00-00', '', 1, 'Administrator', '2018-04-23 12:19:19', 'Administrator', '2018-04-23 13:28:43'),
(108, '1719', 'Happy Juice', '', 'Drink', '', 0, 0, 'jus@mail.com', '0000-00-00', '', 1, 'Administrator', '2018-04-23 14:04:21', 'Administrator', '2018-04-23 14:05:14'),
(109, '1720', 'Kedai Kopi', '', '', '', 0, 0, 'kopi@mail.com', '0000-00-00', '', 1, 'Administrator', '2018-04-23 14:13:00', 'Administrator', '2018-04-23 14:13:43'),
(110, '1143', 'Rujak Bebek', 'Bow', 'Food', '', 0, 0, 'rujak@mail.com', '0000-00-00', '', 1, 'Administrator', '2018-04-23 15:21:23', NULL, NULL),
(111, '1723', 'Bakso Semar', 'MT', 'Makanan', '', 21999888, 0, 'baksosemar@mailcom', '0000-00-00', '', 1, 'Administrator', '2018-04-24 11:18:09', 'Administrator', '2018-04-24 11:20:43');

-- --------------------------------------------------------

--
-- Table structure for table `tenant_r`
--

CREATE TABLE `tenant_r` (
  `id_tenant` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `nama_tenant` varchar(50) DEFAULT NULL,
  `pemilik_tenant` varchar(50) DEFAULT NULL,
  `kat_tenant` varchar(50) DEFAULT NULL,
  `foto_tenant` varchar(250) NOT NULL,
  `notel1_tenant` int(15) DEFAULT NULL,
  `notel2_tenant` int(15) DEFAULT NULL,
  `email_tenant` varchar(50) DEFAULT NULL,
  `sewa_tenant` date NOT NULL,
  `desk_tenant` text,
  `status_tenant` int(11) DEFAULT '1',
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tenant_r`
--

INSERT INTO `tenant_r` (`id_tenant`, `u_id`, `nama_tenant`, `pemilik_tenant`, `kat_tenant`, `foto_tenant`, `notel1_tenant`, `notel2_tenant`, `email_tenant`, `sewa_tenant`, `desk_tenant`, `status_tenant`, `created_by`, `created`, `updated_by`, `updated`) VALUES
(101, '1712', 'Kampung Ayam', '', 'Fast Food', '', 8181818, 9292929, 'kampungayam@email.com', '2018-07-31', 'Kampungnya para ayam.', 1, 'Admin', '2017-07-31 10:13:50', 'Administrator', '2018-04-22 09:17:31'),
(102, '1710', 'Pawon Sunda', 'Eky', 'Fast Food', 'http://128.199.82.69/e-foodcourt/Images/eki.jpg', 8181818, 9292929, 'kampungayam@email.com', '2018-07-31', 'Ayam Penyet Cita Rasa Tinggi', 1, 'Admin', '2017-07-31 10:13:50', NULL, NULL),
(104, '1707', 'Thai Tea', 'Smith', 'Drink', 'testestes', 2147483647, 0, 'thai.tea@gmail.com', '2018-03-01', '', 1, 'Administrator', '2018-03-01 11:42:13', NULL, NULL),
(106, '1710', 'Sate Maranggi ', '', 'food', '', 2147483647, 0, 'maranggi@mail.com', '0000-00-00', 'fgdgsds', 1, 'Administrator', '2018-04-23 11:33:53', 'Administrator', '2018-04-23 12:04:06');

-- --------------------------------------------------------

--
-- Table structure for table `topup`
--

CREATE TABLE `topup` (
  `tgl_tu` datetime NOT NULL,
  `u_id` varchar(45) NOT NULL,
  `harga` int(11) NOT NULL,
  `kdUser` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `topup`
--

INSERT INTO `topup` (`tgl_tu`, `u_id`, `harga`, `kdUser`) VALUES
('2018-04-22 00:08:46', '00708D8021B41D6B', 20000, 0),
('2018-04-22 00:09:06', '00708D8021B41D6B', 20000, 0),
('2018-04-22 00:09:24', '00708D8021B41D6B', 20000, 0),
('2018-04-23 01:58:53', '00708D8021B41D6B', 40000, 0),
('2018-04-23 02:05:07', '00708D8021B41D6B', 40000, 0),
('2018-04-23 02:15:53', '00708D8021B41D6B', 40000, 0),
('2018-04-23 02:18:04', '00708D8021B41D6B', 20000, 0),
('2018-04-23 16:24:17', '00708D8021B41D6B', 40000, 0),
('2018-04-23 16:25:12', '00708D8021B41D6B', 500000, 0),
('2018-04-23 16:36:48', '00708D8021B41D6B', 20000, 0),
('2018-04-23 16:43:05', '00708D8021B41D6B', 40000, 0),
('2018-04-23 17:56:49', '00708D8021B41D6B', 500000, 0),
('2018-04-23 21:15:28', '00708D8021B41D6B', 20000, 0),
('2018-04-24 00:09:52', '00708D8021B41D6B', 20000, 0),
('2018-04-24 00:25:31', '00D0128121B41D6B', 20000, 0),
('2018-04-24 11:40:31', '00708D8021B41D6B', 40000, 0),
('2018-04-24 11:41:18', '00708D8021B41D6B', 500000, 0),
('2018-05-05 15:05:44', '00708D8021B41D6B', 20000, 1722),
('2018-05-05 15:41:16', '00708D8021B41D6B', 20000, 1722),
('2018-05-06 11:54:46', '00708D8021B41D6B', 20000, 1722),
('2018-05-06 11:58:13', '00708D8021B41D6B', 20000, 1722),
('2018-05-06 11:59:18', '00708D8021B41D6B', 20000, 1722),
('2018-05-06 12:18:27', '00708D8021B41D6B', 20000, 1722),
('2018-05-06 12:19:33', '00708D8021B41D6B', 20000, 1722),
('2018-05-06 12:21:47', '00708D8021B41D6B', 20000, 1722),
('2018-05-06 12:32:07', '00708D8021B41D6B', 20000, 1722),
('2018-05-06 12:33:47', '00708D8021B41D6B', 20000, 1722),
('2018-05-06 12:35:07', '00708D8021B41D6B', 20000, 1722),
('2018-05-06 12:56:51', '00708D8021B41D6B', 20000, 1722);

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `kode_order` varchar(50) DEFAULT NULL,
  `u_id_transaksi` varchar(50) DEFAULT NULL,
  `id_tenant` int(11) NOT NULL,
  `nama_cust` varchar(50) DEFAULT NULL COMMENT 'Ini adalah kode Barcode',
  `notel_cust` varchar(12) DEFAULT NULL,
  `email_cust` varchar(100) DEFAULT NULL,
  `id_kasir` int(11) DEFAULT NULL,
  `id_cust` int(11) DEFAULT NULL,
  `waktu_transaksi` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_bayar` tinyint(1) DEFAULT '0',
  `biaya_pesanan` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `TotalPembayaran` int(11) DEFAULT '0',
  `no_meja` varchar(5) DEFAULT NULL,
  `no_agp` varchar(30) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `harga_total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `kode_order`, `u_id_transaksi`, `id_tenant`, `nama_cust`, `notel_cust`, `email_cust`, `id_kasir`, `id_cust`, `waktu_transaksi`, `status_bayar`, `biaya_pesanan`, `discount`, `TotalPembayaran`, `no_meja`, `no_agp`, `created_by`, `created`, `updated_by`, `updated`, `harga_total`) VALUES
(104, '2018042210101', '2018042210101', 101, 'adam', NULL, NULL, NULL, NULL, '2018-04-22 02:25:53', 0, NULL, NULL, 35000, '123', NULL, NULL, NULL, NULL, NULL, NULL),
(105, '2018042210102', '2018042210102', 101, 'adam', NULL, NULL, NULL, NULL, '2018-04-22 02:27:34', 0, NULL, NULL, 18000, '56', NULL, NULL, NULL, NULL, NULL, NULL),
(106, '2018042210103', '2018042210103', 101, 'agp0001', NULL, NULL, NULL, NULL, '2018-04-22 03:09:39', 0, NULL, NULL, 24500, '27', NULL, NULL, NULL, NULL, NULL, NULL),
(107, '2018042210104', '2018042210104', 101, 'djdkddjj', NULL, NULL, NULL, NULL, '2018-04-22 04:16:22', 0, NULL, NULL, 66000, '8388', NULL, NULL, NULL, NULL, NULL, NULL),
(108, '2018042210105', '2018042210105', 101, 'http://en.m.wikipedia.org', NULL, NULL, NULL, NULL, '2018-04-22 07:15:15', 0, NULL, NULL, 36000, '244', NULL, NULL, NULL, NULL, NULL, NULL),
(109, '2018042210106', '2018042210106', 101, 'http://www.qrstuff.com', NULL, NULL, NULL, NULL, '2018-04-22 07:19:08', 0, NULL, NULL, 126000, '567', NULL, NULL, NULL, NULL, NULL, NULL),
(117, '2018042310401', '2018042310401', 104, '123456', NULL, NULL, NULL, NULL, '2018-04-23 15:49:44', 1, NULL, NULL, 8000, '17', NULL, NULL, NULL, NULL, NULL, NULL),
(121, '2018042310402', '00708D8021B41D6B', 104, '123456', NULL, NULL, NULL, NULL, '2018-04-23 16:55:04', 1, NULL, NULL, 8000, '6', NULL, NULL, NULL, NULL, NULL, NULL),
(122, '2018042310701', '00708D8021B41D6B', 107, '123456', NULL, NULL, NULL, NULL, '2018-04-23 16:55:04', 1, NULL, NULL, 18000, '56', NULL, NULL, NULL, NULL, NULL, NULL),
(123, '2018042410801', '00708D8021B41D6B', 108, '123456', NULL, NULL, NULL, NULL, '2018-04-23 17:07:37', 1, NULL, NULL, 10000, '12', NULL, NULL, NULL, NULL, NULL, NULL),
(124, '2018042410601', '00708D8021B41D6B', 106, '123456', NULL, NULL, NULL, NULL, '2018-04-23 17:07:37', 1, NULL, NULL, 20000, '18', NULL, NULL, NULL, NULL, NULL, NULL),
(125, '2018042411101', '00708D8021B41D6B', 111, '123456', NULL, NULL, NULL, NULL, '2018-04-24 04:43:41', 1, NULL, NULL, 36000, '15', NULL, NULL, NULL, NULL, NULL, NULL),
(126, '2018042410802', '00708D8021B41D6B', 108, '123456', NULL, NULL, NULL, NULL, '2018-04-24 04:43:41', 1, NULL, NULL, 10000, '3', NULL, NULL, NULL, NULL, NULL, NULL),
(128, '2018050210401', '2018050210401', 104, '123456', NULL, NULL, NULL, NULL, '2018-05-02 03:13:55', 0, NULL, NULL, 15000, '7', NULL, NULL, NULL, NULL, NULL, NULL),
(129, '2018050210402', '00708D8021B41D6B', 104, '123456', NULL, NULL, NULL, NULL, '2018-05-02 07:44:46', 0, NULL, NULL, 15000, '3', NULL, NULL, NULL, NULL, NULL, NULL),
(130, '2018050410401', '2018050410401', 104, '123456', NULL, NULL, NULL, NULL, '2018-05-04 09:08:39', 0, NULL, NULL, 10000, '18', NULL, NULL, NULL, NULL, NULL, NULL),
(131, '2018050510401', '2018050510401', 104, '123456', NULL, NULL, NULL, NULL, '2018-05-05 08:33:44', 0, NULL, NULL, 8000, '8', NULL, NULL, NULL, NULL, NULL, NULL),
(132, '2018050510701', '2018050510701', 107, '123456', NULL, NULL, NULL, NULL, '2018-05-05 08:33:47', 0, NULL, NULL, 20000, '18', NULL, NULL, NULL, NULL, NULL, NULL),
(133, '2018050610401', '2018050610401', 104, '123456', NULL, NULL, NULL, NULL, '2018-05-06 04:57:39', 1, NULL, NULL, 18000, '8', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_accounts`
--

CREATE TABLE `t_accounts` (
  `id` int(11) NOT NULL,
  `acc_number` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `balance` decimal(11,0) NOT NULL DEFAULT '0',
  `last_transaction` datetime DEFAULT NULL,
  `last_topup` datetime DEFAULT NULL,
  `auth_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_transactions`
--

CREATE TABLE `t_transactions` (
  `id` int(11) NOT NULL,
  `acc_id` int(11) NOT NULL,
  `trans_date` datetime NOT NULL,
  `vendor_id` int(11) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `debit` decimal(11,0) DEFAULT NULL,
  `credit` decimal(11,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_detail`
-- (See below for the actual view)
--
CREATE TABLE `v_detail` (
`qrcode` varchar(50)
,`kode_order` varchar(50)
,`id_menu` int(11)
,`nama_menu` varchar(50)
,`nama_tenant` varchar(50)
,`kode_tenant` int(11)
,`TotalPembayaran` int(11)
,`total_harga_menu` int(20)
,`u_id_transaksi` varchar(50)
,`create_at` timestamp
,`harga` int(11)
,`discount` int(11)
,`jumlah_pesanan` int(11)
,`holder_name` varchar(255)
,`card_no` varchar(255)
,`card_uid` varchar(255)
,`status_bayar` tinyint(1)
,`no_meja` varchar(5)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_topup`
-- (See below for the actual view)
--
CREATE TABLE `v_topup` (
`tgl_tu` datetime
,`u_id` varchar(45)
,`holder_name` varchar(255)
,`harga` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `v_detail`
--
DROP TABLE IF EXISTS `v_detail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_detail`  AS  select `detail_transaksi`.`nama_cust` AS `qrcode`,`detail_transaksi`.`kode_order` AS `kode_order`,`detail_transaksi`.`id_menu` AS `id_menu`,`menu`.`nama_menu` AS `nama_menu`,`tenant`.`nama_tenant` AS `nama_tenant`,`detail_transaksi`.`kode_tenant` AS `kode_tenant`,`transaksi`.`TotalPembayaran` AS `TotalPembayaran`,`detail_transaksi`.`total_harga_menu` AS `total_harga_menu`,`detail_transaksi`.`u_id_transaksi` AS `u_id_transaksi`,`detail_transaksi`.`create_at` AS `create_at`,`detail_transaksi`.`harga` AS `harga`,`detail_transaksi`.`discount` AS `discount`,`detail_transaksi`.`jumlah_pesanan` AS `jumlah_pesanan`,`cards`.`holder_name` AS `holder_name`,`cards`.`card_no` AS `card_no`,`cards`.`card_uid` AS `card_uid`,`transaksi`.`status_bayar` AS `status_bayar`,`transaksi`.`no_meja` AS `no_meja` from ((((`detail_transaksi` join `transaksi` on((`transaksi`.`kode_order` = `detail_transaksi`.`kode_order`))) join `menu` on((`detail_transaksi`.`id_menu` = `menu`.`id_menu`))) join `tenant` on((`menu`.`id_tenant` = `tenant`.`id_tenant`))) join `cards` on((`transaksi`.`nama_cust` = `cards`.`qrcode`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_topup`
--
DROP TABLE IF EXISTS `v_topup`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_topup`  AS  select `topup`.`tgl_tu` AS `tgl_tu`,`topup`.`u_id` AS `u_id`,`cards`.`holder_name` AS `holder_name`,`topup`.`harga` AS `harga` from (`topup` join `cards` on((`topup`.`u_id` = convert(`cards`.`card_uid` using utf8)))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_resources`
--
ALTER TABLE `acl_resources`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `acl_roles`
--
ALTER TABLE `acl_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `acl_role_parents`
--
ALTER TABLE `acl_role_parents`
  ADD PRIMARY KEY (`role_id`,`parent`),
  ADD KEY `parent` (`parent`);

--
-- Indexes for table `acl_rules`
--
ALTER TABLE `acl_rules`
  ADD PRIMARY KEY (`role_id`,`resource_id`),
  ADD KEY `resource_id` (`resource_id`);

--
-- Indexes for table `api_session`
--
ALTER TABLE `api_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_token` (`access_token`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_autologin`
--
ALTER TABLE `auth_autologin`
  ADD PRIMARY KEY (`user`,`series`);

--
-- Indexes for table `auth_users`
--
ALTER TABLE `auth_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity_idx` (`last_activity`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_cust`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_menu` (`id_menu`) USING BTREE;

--
-- Indexes for table `detail_transaksi_ol`
--
ALTER TABLE `detail_transaksi_ol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_menu` (`id_menu`) USING BTREE;

--
-- Indexes for table `kasir`
--
ALTER TABLE `kasir`
  ADD PRIMARY KEY (`id_kasir`);

--
-- Indexes for table `meja`
--
ALTER TABLE `meja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `FK_RELATIONSHIP_5` (`id_tenant`);

--
-- Indexes for table `menu_a`
--
ALTER TABLE `menu_a`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `FK_RELATIONSHIP_5` (`id_tenant`);

--
-- Indexes for table `menu_old`
--
ALTER TABLE `menu_old`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `FK_RELATIONSHIP_5` (`id_tenant`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tenant` (`id_tenant`),
  ADD KEY `id_detail_transaksi` (`id_detail_transaksi`);

--
-- Indexes for table `tenant`
--
ALTER TABLE `tenant`
  ADD PRIMARY KEY (`id_tenant`);

--
-- Indexes for table `tenant_r`
--
ALTER TABLE `tenant_r`
  ADD PRIMARY KEY (`id_tenant`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `FK_REFERENCE_4` (`id_kasir`),
  ADD KEY `FK_REFERENCE_5` (`id_cust`);

--
-- Indexes for table `t_accounts`
--
ALTER TABLE `t_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auth_user_id` (`auth_user_id`);

--
-- Indexes for table `t_transactions`
--
ALTER TABLE `t_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trans_date` (`trans_date`),
  ADD KEY `acc_id` (`acc_id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acl_resources`
--
ALTER TABLE `acl_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `acl_roles`
--
ALTER TABLE `acl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `api_session`
--
ALTER TABLE `api_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;
--
-- AUTO_INCREMENT for table `auth_users`
--
ALTER TABLE `auth_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1724;
--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=434642;
--
-- AUTO_INCREMENT for table `detail_transaksi_ol`
--
ALTER TABLE `detail_transaksi_ol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;
--
-- AUTO_INCREMENT for table `meja`
--
ALTER TABLE `meja`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1060044;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tenant`
--
ALTER TABLE `tenant`
  MODIFY `id_tenant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=134;
--
-- AUTO_INCREMENT for table `t_accounts`
--
ALTER TABLE `t_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_transactions`
--
ALTER TABLE `t_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `acl_role_parents`
--
ALTER TABLE `acl_role_parents`
  ADD CONSTRAINT `acl_role_parents_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_role_parents_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `acl_rules`
--
ALTER TABLE `acl_rules`
  ADD CONSTRAINT `acl_rules_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_rules_ibfk_2` FOREIGN KEY (`resource_id`) REFERENCES `acl_resources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_users`
--
ALTER TABLE `auth_users`
  ADD CONSTRAINT `auth_users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `acl_roles` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`);

--
-- Constraints for table `menu_a`
--
ALTER TABLE `menu_a`
  ADD CONSTRAINT `FK_RELATIONSHIP_5` FOREIGN KEY (`id_tenant`) REFERENCES `tenant_r` (`id_tenant`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`id_tenant`) REFERENCES `tenant_r` (`id_tenant`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`id_detail_transaksi`) REFERENCES `detail_transaksi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `FK_REFERENCE_4` FOREIGN KEY (`id_kasir`) REFERENCES `kasir` (`id_kasir`),
  ADD CONSTRAINT `FK_REFERENCE_5` FOREIGN KEY (`id_cust`) REFERENCES `customer` (`id_cust`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
